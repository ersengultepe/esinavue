/* Add here all your JS customizations */
function elementSum(selector){
    let total = 0;
    $(selector).each( function(index, value){
        total += parseInt(value.innerHTML)
    } );
    return total;
}

function arraySum(arr){
    let total = 0;
    $.each(arr, function(index, value){
        total += value
    } );
    return total;
}

function createArray(selector) {
    let array = [];
    $(selector).each( function(index, value){
        array.push(value)
    } );
    return array;
}