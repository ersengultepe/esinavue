@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
@stop

@section('content')
<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">{{ Session::get('breadcrumb') }}</h2>
            <small>Toplam {{ Session::get('breadcrumb') }}: {{ $yorumlar->total() }}</small>
            @if(request()->segment(3) === 'onaylananlar')
                <span class="sayfa" id="sp_onaylananlar"></span>
            @endif
            @if(request()->segment(3) === 'onay-bekleyenler')
                <span class="sayfa" id="sp_onay_bekleyenler"></span>
            @endif
            @if(request()->segment(3) === 'silinenler')
                <span class="sayfa" id="sp_silinenler"></span>
            @endif
        </header>

        <div class="panel-body">
            {{ $yorumlar->links() }}
            <div class="table-responsive">
                <table class="table table-hover mb-none">
                    <thead>
                    <tr>
                        <th>Sıra</th>
                        <th>ID</th>
                        <th>Yorum</th>
                        <th>Ekleyen</th>
                        <th>Eklenme Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($yorumlar as $yorum)
                        <tr id="tr_{{ $yorum->id }}">
                            <td>{{ $yorumlar->currentPage() === 1 ? $loop->iteration :  ( ( $yorumlar->perPage()*($yorumlar->currentPage()-1)) +  $loop->iteration ) }}</td>
                            <td>{{ $yorum->id }}</td>
                            <td rel="tooltip" data-placement="top" data-original-title="{{ optional($yorum->adres)->adi }}">
                                <a href="{{ url('/sinav/'.optional($yorum->adres)->link) }}" target="_blank" style="text-decoration-line: none;color: #1a7576;font-size: larger">
                                    {!! $yorum->yorum !!}
                                </a>
                            </td>

                            <td>@if(optional($yorum->user)->id > 0 )
                                    <img class="img-thumbnail" src="{{ asset($yorum->user->foto) }}" style="max-height: 60px;max-width: 60px">
                                    {{ $yorum->user->kullanici }}
                                @else
                                    {{ $yorum->kullanici }}
                                @endif
                            </td>
                            <td>{{ $yorum->created_at }}</td>
                            <td class="actions-hover actions-fade" width="200">
                                <select class="form-control input-sm mb-md yorum-onay" id="{{ $yorum->id }}">
                                    <option value="1" @if($yorum->onay === 1) selected @endif >Onayla</option>
                                    <option value="0" @if($yorum->onay === 0) selected @endif >Beklet</option>
                                    <option value="2" @if($yorum->onay === 2) selected @endif >Sil</option>
                                </select>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            {{ $yorumlar->links() }}
        </div>
    </section>
</div>

@stop

@section('js')
    <script>
        $('.yorum-onay').change(function () {
            let onay = $(this).val();
            let yorum_id = this.id;

            $.ajax({
                type: "POST",
                url: document.location.origin + '/ajax/yorum/onay-update',
                data: {
                    'yorum_id': yorum_id,
                    'onay': onay
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });

               if($(".sayfa").attr('id') === 'sp_onaylananlar')
               {
                   $("#tr_"+yorum_id).fadeOut(2300);
               }
               if($(".sayfa").attr('id') === 'sp_onay_bekleyenler')
               {
                   $("#tr_"+yorum_id).fadeOut(2300);
               }
               if($(".sayfa").attr('id') === 'sp_silinenler')
               {
                   $("#tr_"+yorum_id).fadeOut(2300);
               }

            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Yorum onayı güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });
    </script>
@stop