<?php $__env->startSection('title'); ?>
    <title>Şifre Sıfırlama | <?php echo e(config('app.name')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Şifre Sıfırlama Sayfası">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo e(url('/')); ?>">Ana Sayfa</a></li>
                        <li class="active"><a href="#">Şifre Sıfırlama</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Şifre Sıfırlama</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?php echo e(__('Şifre Sıfırlama')); ?></div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <form method="POST" action="<?php echo e(route('password.email')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><?php echo e(__('E-Mail Adresiniz')); ?></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required>

                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-dark">
                                    <?php echo e(__('Şifre Sıfırlama Bağlantısı Gönder')); ?>

                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>