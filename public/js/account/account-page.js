let acp = new Vue({
    el: '#account',
    name: 'AccountPage',
    data:{
        kendiHakkinda: false,
        kendiHakkindaOzet: true,
        gosterGizle: 'Hepsini Göster',
        sehirler: {},
        ilceler: {},
        meslekler:{},
        takimlar:{},
        selectedSehir: 0,
        selectedIlce: 0,
        selectedTahsil: '',
        selectedMeslek: '',
        selectedTakim: '',
        userProfilePageValues:{},
        mottolar:{
            1 : 'İyi kararlar zamanla oluşur.',
            2 : 'Tanrı zar atmaz.',
            3 : 'Hayatta hiçbir şey nedensiz değildir, iyi ya da kötü şeylerin mutlaka sebebi vardır.',
            4 : 'Mutluluk verirsen mutluluk alırsın.',
            5 : 'Su akar yolunu bulur.',
            6 : 'En büyük erdem dürüstlüktür.',
            7 : 'Senden başka kimse seni yıkamaz. Senden başka kimse seni kurtaramaz.',
            8 : 'Hayat,ciddiye alınması gereken çok önemli bir şeydir.',
            9 : 'Kadere Razı Ol.',
            10: 'Tekerleri döndürmeye devam et.',
            11: 'Sonuna kadar git.',
            12: 'Gideceğin yeri bilmiyorsan, vardığın yerin önemi yoktur.'
        }
    },
    created(){
        this.getUserProfilePageValues();
    },
    methods:{
        kendiHakkindaClick(){
            if(this.kendiHakkindaOzet){
                this.kendiHakkindaOzet = false;
                this.kendiHakkinda = true;
                this.gosterGizle = 'GİZLE';
            }else{
                this.kendiHakkindaOzet = true;
                this.kendiHakkinda = false;
                this.gosterGizle = 'HEPSİNİ GÖSTER';
            }
        },
        menuActive(path){
            if('hesap-yonetimi' === window.location.pathname.split('/')[1]){
                return this.isActive = true;
            }else{
                return this.isActive = false;
            }
        },
        getSehirler(){
            axios.get('https://'+window.location.host+'/get/sehirler').then(response =>{
                this.sehirler = response.data;
            });
        },
        getIlceler(sehir_id){
            axios.get('https://'+window.location.host+'/get/ilceler/'+sehir_id).then(response =>{
                this.ilceler = response.data;
            });
        },
        getMeslekler(){
            axios.get('https://'+window.location.host+'/get/meslekler').then(response => {
                this.meslekler = response.data;
            })
        },
        getTakimlar(){
            axios.get('https://'+window.location.host+'/get/takimlar').then(response => {
                this.takimlar = response.data;
            })
        },
        getUserProfilePageValues(){
            axios.get(window.location.href.replace('/hesap-yonetimi/', '/hesap-yonetimi/')).then(response =>{
                this.userProfilePageValues = response.data;
                this.selectedSehir = this.userProfilePageValues.sehir;
                this.ilceler = this.getIlceler(this.userProfilePageValues.sehir);
                this.selectedIlce = this.userProfilePageValues.ilce;
                this.selectedTahsil = this.userProfilePageValues.tahsil;
                this.selectedMeslek = this.userProfilePageValues.meslek;
                this.selectedTakim = this.userProfilePageValues.takim;
                this.getSehirler();
                this.getMeslekler();
                this.getTakimlar();
            })
        },
        getMotto(){
            return this.mottolar[Math.floor(Math.random() * 12)+1];
        }
    }
});