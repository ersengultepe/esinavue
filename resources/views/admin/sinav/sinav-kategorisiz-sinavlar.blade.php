@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Kategorisiz Sınavlar</h2>
            </header>
           @if($sinavlar->count() > 0)
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Sınav Adı</th>
                                <th>Eklenme Tarihi</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($sinavlar as $sinav)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="{{ route('getSinavSorulari', ['id'=>$sinav->id]) }}">
                                            {{ str_limit($sinav->adi) }}
                                        </a>
                                    </td>
                                    <td>{{ $sinav->created_at }}</td>
                                    <td class="actions-hover actions-fade">
                                        <a href=""><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $sinavlar->links() }}
           @else
                Listelenecek sınav bulunmamaktadır.
           @endif

        </section>
    </div>
@stop