@extends('profile.template')

@section('content')
    <div class="row" id="profile">

        <div class="col-md-4 col-lg-3">

            <section class="panel">
                <div class="panel-body">
                    <div class="thumb-info mb-md">
                        <img src="{{ userPhoto($user) }}" class="rounded img-responsive" alt="{{ $user->kullanici }}" style="width: 100%">
                        <div class="thumb-info-title">
                            <span class="thumb-info-inner">{{  ucWordsTr($user->kullanici) }}</span>
                            <span class="thumb-info-type">{{ $user->meslek }}</span>
                        </div>
                    </div>

                    <hr class="dotted short">

                    <h6 class="text-muted">Hakkında</h6>
                    <p>{{ $user->kendi_hakkinda }}</p>

                    <hr class="dotted short">

                    <div class="social-icons-list">
                        <a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                        <a rel="tooltip" data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                        <a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
                    </div>

                </div>
            </section>

        </div>
        <div class="col-md-8 col-lg-6">
            <div class="tabs">
                <ul class="nav nav-tabs tabs-primary">
                    <li class="active">
                        <a href="#popular" data-toggle="tab">Genel Bakış</a>
                    </li>
                    <li>
                        <a href="#recent" data-toggle="tab">Hesap Bilgileri</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="popular" class="tab-pane active">

                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                    </div>
                    <div id="recent" class="tab-pane">
                        <p>Recent</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-3">

            <h4 class="mb-md">Sınav İstatistikleri</h4>
            <ul class="simple-card-list mb-xlg">
                @if($info->toplamPuan === '-')
                <li class="danger">
                    <h3>Hiç Sınav Çözmedi</h3>
                </li>
                    @else
                    <li class="primary">
                        <h3>{{ $info->basariSirasi }}</h3>
                        <p>Başarı Sırası</p>
                    </li>
                    <li class="primary">
                        <h3>{{ $info->toplamPuan }}</h3>
                        <p>Toplam Puan</p>
                    </li>
                    <li class="primary">
                        <h3>{{ $info->ortalamaPuan }}</h3>
                        <p>Son 10 Sınav Ortalaması</p>
                    </li>
                    <li class="primary">
                        <h3>{{ $info->cozulensinav }}</h3>
                        <p>Çözülen Sınav Sayısı</p>
                    </li>
                @endif

            </ul>

        </div>

    </div>
@stop

@section('js_sup')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
@endsection

@section('js')
    <script>
        Highcharts.chart('container', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: '{{ ucWordsTr($user->kullanici).' Son 10 Sınavındaki Başarısı' }}'
            },
            subtitle: {
                text: 'Kaynak : esinavsalonu.com'
            },
            xAxis: [{
                categories: [
                    <?php  foreach ($sinavAdlari as $ad) {
                    echo '"' . $ad . '",';
                }
                    ?>
                ],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} puan',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Ortalama Puan',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Sınav Puanı',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} ',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 60,
                floating: true,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'Sınav Puanı',
                type: 'column',
                yAxis: 1,
                data: {{ collect($sinavlar)->take(10)->pluck('puan') }},
                tooltip: {
                    valueSuffix: ' puan'
                }

            }, {
                name: 'Ortalama',
                type: 'spline',
                yAxis: 1,
                data: {{ collect($sinavlar)->take(10)->pluck('ort') }},
                tooltip: {
                    valueSuffix: ' puan'
                }
            }]
        });
    </script>
    @endsection