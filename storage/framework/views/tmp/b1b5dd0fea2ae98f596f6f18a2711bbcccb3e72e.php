<div id="edit" class="tab-pane">
    <form class="form-horizontal" method="get" >
        <h4 class="mb-xlg">Kişisel Bilgileriniz</h4>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileFirstName">Adınız</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileFirstName" value="<?php echo e(Auth::user()->ad); ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileLastName">Soyadınız</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileLastName" value="<?php echo e(Auth::user()->soyad); ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileAddress">Doğum Tarihiniz</label>
                <div class="col-md-8">
                    <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                        <input id="date" data-plugin-masked-input="" data-input-mask="99/99/9999"
                               placeholder="__/__/____" class="form-control" value="<?php echo e(Auth::user()->dogum_tarihi_ff); ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="cinsiyet">Cinsiyetiniz</label>
                <div class="col-md-8">
                    <select class="form-control mb-md">
                        <option value="2" <?php if(Auth::user()->cinsiyet == 2): ?> selected <?php endif; ?> >Kadın</option>
                        <option value="1" <?php if(Auth::user()->cinsiyet == 1): ?> selected <?php endif; ?> >Erkek</option>
                        <option value="0" <?php if(Auth::user()->cinsiyet == 0 ): ?> selected <?php endif; ?> >Belirtmek İstemiyorum</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="email">E-postanız</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="email" placeholder="<?php echo e(Auth::user()->email); ?>"
                           disabled="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="email">E-Bülten Aboneliği</label>
                <div class="col-md-8">
                    <div class="switch switch-sm switch-info">
                        <input type="checkbox" name="switch" data-plugin-ios-switch checked="checked"/>
                    </div>
                </div>
            </div>
        </fieldset>
        <hr class="dotted tall">
        <h4 class="mb-xlg">Senin Hakkında </h4>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label" for="">Şehir</label>
                <div class="col-md-8">
                <select class="form-control populate select2-container-active" v-model="selectedSehir" @click="getIlceler(selectedSehir)">
                    <optgroup label="Şehirler">
                        <option :value="item.id" v-for="item in sehirler" >{{ item.sehir }}</option>
                    </optgroup>
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="ilce">İlçe</label>
                <div class="col-md-8">
                    <select class="form-control populate " v-model="selectedIlce">
                        <optgroup label="İlçeler">
                            <option :value="item.id" v-for="item in ilceler" >{{ item.ilce }}</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="tahsil">Tahsil</label>
                <div class="col-md-8 select2-result-label">
                    <select class="form-control populate select2-container-active" v-model="selectedTahsil">
                        <optgroup label="Öğrenim Durumunuz">
                            <option value="ilk_orta">İlkokul / Ortaokul</option>
                            <option value="lise">Lise</option>
                            <option value="universite">Üniversite</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="meslek">Meslek</label>
                <div class="col-md-8">
                    <select class="form-control populate" v-model="selectedMeslek">
                        <optgroup label="Mesleğiniz">
                            <option :value="item.id" v-for="item in meslekler">{{ item.meslek }}</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="meslek">Tuttuğunuz Takım</label>
                <div class="col-md-8">
                    <select class="form-control populate" v-model="selectedTakim">
                        <optgroup label="Takımınız">
                            <option :value="item.id" v-for="item in takimlar">
                                {{ item.takim }}
                            </option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileCompany">Website</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-globe"></i>
                        </span>
                        <input id="website" data-plugin-masked-input="" data-input-mask="www.orneksiteadi.com"
                               :value="userProfilePageValues.website" placeholder="www.orneksiteadi.com" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileCompany">Motton Nedir?</label>
                <div class="col-md-8">
                    <input class="form-control" data-plugin-maxlength="" maxlength="140"
                           :value="userProfilePageValues.motto" :placeholder="getMotto()">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileBio">Kendini Anlatsana Biraz</label>
                <div class="col-md-8">
                    <textarea class="form-control" rows="3" id="profileBio"
                              :value="userProfilePageValues.kendi_hakkinda"
                              placeholder="Duygusal mısın? Eğlenceli mi? Yoksa Lider Ruhlu mu? Gerçekte Nasıl Birisin?"></textarea>
                </div>
            </div>

        </fieldset>

        <hr class="dotted tall">
        <h4 class="mb-xlg">Şifre Değiştir</h4>
        <fieldset class="mb-xl">
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileNewPassword">Mevcut Şifre <i data-placement="top" data-toggle="tooltip" title="Sayısal ve özel karakterler kullanmayınız. (Örn;%?*\123@€{, gibi)" class="fa fa-info-circle"></i></label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileNewPassword">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileNewPassword">Yeni Şifre</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileNewPassword">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileNewPasswordRepeat">Tekrar Yeni Şifre</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileNewPasswordRepeat">
                </div>
            </div>
        </fieldset>

        <div class="panel-footer">
            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Güncelle</button>
                    <button type="reset" class="btn btn-default">Temizle</button>
                </div>
            </div>
        </div>

    </form>

</div>
