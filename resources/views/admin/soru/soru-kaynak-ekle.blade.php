@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
@stop

@section('js')
    <!-- Specific Page Vendor -->
    <script src="{{ asset('/pa/HTML/assets/vendor/fuelux/js/spinner.js') }}"></script>
@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title">Soru Kaynağı Giriş Sayfası</h2>

                    @if(Session::has('sinav_kaynak_error'))
                    <br><div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <strong>Hata !</strong> Yeni Bir Sınav Kaynağı Eklerken Hata İle Karşılaşıldı. Yeniden deneyiniz.
                        </div>
                        @endif

                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="post" action="{{ route('setSoruKaynakEkle') }}">
                        @csrf
                        <br>
                        <!-- start:  -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Sınav Kaynağı</label>
                            <div class="col-md-6">
                                <input type="text" name="ad" class="form-control" id="inputDefault">
                            </div>
                        </div>
                        <!-- end:  -->

                        <!-- start:  -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Sınav Tarihi</label>
                            <div class="col-md-6">
                                <div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</span>
                                    <input type="text" name="tarih" data-plugin-datepicker="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- end:  -->

                        <!-- start: Telif -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="telif">Telif Var mı?</label>
                            <div class="col-md-6">
                                <select name="telif" class="select2_single form-control" tabindex="1" data-toggle="tooltip" data-trigger="hover" data-original-title="Sınav İçin Telif Sorunu Var mı?" id="telif">
                                    <option value='1' >Evet</option>
                                    <option value='0' >Hayır</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: Telif -->

                        <!-- start:  -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault22">Sınav Düzenleyen Kurum</label>
                            <div class="col-md-6">
                                <input type="text" name="duzenleyen_kurum" class="form-control" id="inputDefault22">
                            </div>
                        </div>
                        <!-- end:  -->

                        <!-- start:  -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault3">Sınav Düzenlenen Kurum</label>
                            <div class="col-md-6">
                                <input type="text" name="duzenlenen_kurum" class="form-control" id="inputDefault3">
                            </div>
                        </div>
                        <!-- end:  -->

                        <!-- start:  -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Soru Sayısı</label>
                            <div class="col-md-6">
                                <div data-plugin-spinner="" data-plugin-options="{ &quot;value&quot;:0, &quot;min&quot;: 0, &quot;max&quot;: 500 }">
                                    <div class="input-group" style="width:150px;">
                                        <input type="text" name="soru_sayisi" class="spinner-input form-control" maxlength="3" >
                                        <div class="spinner-buttons input-group-btn">
                                            <button type="button" class="btn btn-default spinner-up">
                                                <i class="fa fa-angle-up"></i>
                                            </button>
                                            <button type="button" class="btn btn-default spinner-down">
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <p>
                                     <code>En Fazla</code> 500'e kadar
                                </p>
                            </div>
                        </div>
                        <!-- end:  -->

                        <br>
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-success">Kaydet</button>

                    </form>
                </div>
            </section>
        </div>
    </div>
    @stop