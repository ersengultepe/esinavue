let edtp = new Vue({
   el:'#edit-profile',
   name:'AccountEdit',
   data:{
       sehirler:[]       
   },
    created(){
    this.getSehirler();
    },
    afterLoad(){
        this.getSehirler();
    },
    methods:{
        getSehirler(){
            axios.get('https://'+window.location.host+'/get/sehirler').then(response =>{
                this.sehirler = response.data[0];
            })
        }
    }
});