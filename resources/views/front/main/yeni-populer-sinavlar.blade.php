<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
				<div class="col-md-12">

                <!-- 970x250 Header Reklam -->
				<ins class="adsbygoogle"
					 style="display:inline-block;width:970px;height:250px"
					 data-ad-client="ca-pub-2211050103220921"
					 data-ad-slot="9398451499"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>

				</div>	
			
                <div class="col-sm-6">
                    <div class="heading heading-border heading-bottom-border">
                        <h3><strong>En Çok Çözülen </strong>Sınavlar</h3>
                    </div>

                    @foreach($enCokCozulenSinavlar as $sinav)
                        <div class="feature-box">
                            <div class="feature-box-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $sinav->adi }}">
                                <h4 class="heading-primary mb-0">
                                    <a  href="{{ route('sinav', [$sinav->link]) }}" style="text-decoration: none">
                                        {{ str_limit(' '.$sinav->adi,43) }}
                                    </a>
                                </h4>

                                <br>
                            </div>
                        </div>
                        @endforeach

                </div>
                <div class="col-sm-6">

                    <div class="heading heading-border heading-bottom-border">
                        <h3><strong>Yeni</strong> Eklenen Sınavlar</h3>
                    </div>

                    @foreach($yeniEklenenSinavlar as $sinav)
                        <div class="feature-box">
                            <div class="feature-box-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $sinav->adi }}">
                                <h4 class="heading-primary mb-0">
                                    <a  href="{{ route('sinav', [$sinav->link]) }}" style="text-decoration: none">
                                        {{ str_limit($sinav->adi,43) }}
                                    </a>
                                </h4>

                                <br>
                            </div>
                        </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>