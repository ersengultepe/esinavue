<!doctype html>
<html class="fixed">
<head>
    <!-- Basic -->
    <meta charset="UTF-8">

    <title>Kokpit | EsinavSalonu.Com</title>
    <meta name="keywords" content="esinavsalonu" />
    <meta name="description" content="esinavsalonu yönetim merkezi">
    <meta name="author" content="esinavsalonu.com">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/magnific-popup/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/pnotify/pnotify.custom.css') }}" />
@yield('css')
<!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/theme.css') }}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/skins/default.css') }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/theme-custom.css') }}">

    <!-- Head Libs -->
    <script src="{{ asset('pa/HTML/assets/vendor/modernizr/modernizr.js') }}"></script>
    <!-- Toaster -->
    {{--    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">--}}

    <script>
        (function(w,d,s,g,js,fs){
            g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
            js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
            js.src='https://apis.google.com/js/platform.js';
            fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
        }(window,document,'script'));
    </script>
</head>
<body>
<section class="body" >

    <!-- start: header -->
    <header class="header">
        <!-- start: Logo -->
    @include('admin.logo')
    <!-- end: Logo -->

        <!-- start: search & user box -->
    @includeWhen(Auth::check(), 'admin.header')
    <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div id="account-page" class="inner-wrapper" >

        @include('admin.sidebar')

        <section role="main" class="content-body">

            <!-- start: breadcrumbs -->
        @include('admin.breadcrumbs')
        <!-- end: breadcrumbs -->

            <!-- start: page -->
        @yield('content')

        <!-- start: soru kategori ekle ajax modal -->
            <div class="panel-body">
                <a class="modal-with-form btn btn-default" href="#modalForm">Yeni Soru Kategorisi Ekle</a>

                <!-- Modal Form -->
                <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                    <section class="panel">
                        <header class="panel-heading">
                            <h2 class="panel-title">Soru Kategorisi Ekleme Formu</h2>
                        </header>
                        <div class="panel-body">
                            <form id="demo-form" class="form-horizontal mb-lg" novalidate="novalidate">

                                <div class="form-group mt-lg">
                                    <label class="col-sm-3 control-label">Soru Kategori Adı</label>
                                    <div class="col-sm-9">
                                        <input type="text" id="soru_kategori_adi_ajax" name="kategori_adi" class="form-control" placeholder="Buraya soru kategorisini yaz..." required/>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <footer class="panel-footer">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-primary modal-confirm soruKatAjaxKaydet">Kaydet</button>
                                    <button class="btn btn-default modal-dismiss">İptal</button>
                                </div>
                            </div>
                        </footer>
                    </section>
                </div>

            </div>
            <!-- end: soru kategori ekle ajax modal -->


            <!-- end: page -->
        </section>
    </div>
    <!-- start: sidebar-right -->
@include('admin.sidebar-right')
<!-- end: sidebar-right -->

</section>


<!-- Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
<!-- Specific Page Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/pnotify/pnotify.custom.js') }}"></script>
@yield('js')

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('pa/HTML/assets/javascripts/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('pa/HTML/assets/javascripts/theme.custom.js') }}"></script>
<!-- Theme Initialization Files -->
<!-- Examples -->
{{--<script src="{{ asset('pa/HTML/assets/javascripts/dashboard/examples.dashboard.js') }}"></script>--}}

<script src="{{ asset('pa/HTML/assets/javascripts/theme.init.js') }}"></script>
<!-- Examples -->
<script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.notifications.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/javascripts/forms/examples.advanced.form.js') }}"></script>

<script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.modals.js') }}"></script>

<script type="text/javascript" src="{{ asset('public/ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replaceClass = 'ckeditor';
    CKEDITOR.config.filebrowserBrowseUrl = '/fileman/index.html';
    CKEDITOR.config.filebrowserImageBrowseUrl = '/fileman/index.html' + '?type=image';
    CKEDITOR.config.removeDialogTabs = 'link:upload;image:upload';
</script>

<script>
    $(".soruKatAjaxKaydet").click( function () {

        let soru_kategori_adi_ajax = $('#soru_kategori_adi_ajax').val();

        $.ajax({
            type: "POST",
            url: document.location.origin+'/ajax/soru-kategori/insert',
            data:{
                'kategori_adi': soru_kategori_adi_ajax,
            }
        }).done(function (data) {
            $('#soru_kategori_adi_ajax').val('');

            $.magnificPopup.close();

            // let select2Data = {
            //     id: data.id,
            //     text: data.kategori_adi
            // };
            console.log(soru_kategori_adi_ajax+' FFFFFFFFFFFF');

            var newOption = new Option(soru_kategori_adi_ajax, data.id, false, false);
            $(".soru-kategori-update").append(newOption);

            new PNotify({
                title: data.title,
                text: data.text,
                type: 'success',
                shadow: true
            });
        }).fail(function () {
            new PNotify({
                title: 'Başarısız',
                text: 'Soru Kategorisi Eklenemedi... :(',
                type: 'error',
                shadow: true
            })
        });
    })


</script>

</body>
</html>