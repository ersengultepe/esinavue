<?php $__env->startSection('title'); ?>
    <title><?php echo e(config('app.name').' | '.config('app.slogan')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
    <meta property="fb:app_id"          content="1234567890" />
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="http://newsblog.org/news/136756249803614" />
    <meta property="og:title"           content="Introducing our New Site" />
    <meta property="og:image"           content="https://scontent-sea1-1.xx.fbcdn.net/hphotos-xap1/t39.2178-6/851565_496755187057665_544240989_n.jpg" />
    <meta property="og:description"    content="http://samples.ogp.me/390580850990722" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '242614719760687',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-shop.css')); ?>">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/settings.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/layers.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/navigation.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active"><a href="<?php echo e(route('ekran')); ?>">Sınav Ekranı</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-lg-1">
                        <img class="img-fluid rounded mb-4" src="<?php echo e(asset($sinavTip->logo)); ?>"
                             alt="<?php echo e($sinavTip->sinav_tip); ?>">

                    </div>
                    <div class="col-lg-11">
                        <h1><?php echo e($sinavTip->sinav_tip); ?> </h1>
                    </div>
                    
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">

                <?php if($sinavKategorileri->where('sinav_format',3)->isEmpty() !== true): ?>
                    <div class="col-lg-12">
                        <h2>GEÇMİŞ SINAVLAR & DENEME SINAVLARI</h2>
                        <?php $__currentLoopData = $sinavKategorileri->where('sinav_format',3); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategori): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo $__env->make('front.sinav.sinav-ekrani-kategoriler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <br>
                        <hr class="tall">
                    </div>
                <?php endif; ?>

                <?php if($sinavKategorileri->where('sinav_format', 1)->isEmpty() !== true ): ?>
                    <div class="<?php if($sinavKategorileri->where('sinav_format', 3)->isEmpty()): ?> col-lg-12 <?php else: ?> col-lg-6 <?php endif; ?>">
                        <h2>ORTAK SINAVLAR</h2>
                        <?php $__currentLoopData = $sinavKategorileri->where('sinav_format', 1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategori): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo $__env->make('front.sinav.sinav-ekrani-kategoriler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        <br>
                        <hr class="tall">
                    </div>
                <?php endif; ?>


                <?php if($sinavKategorileri->where('sinav_format', 2)->isEmpty() !== true): ?>

                    <div class="<?php if($sinavKategorileri->where('sinav_format', 3)->isEmpty()): ?> col-lg-12 <?php else: ?> col-lg-6 <?php endif; ?>">
                        <h2>ALAN SINAVLARI</h2>

                        <?php $__currentLoopData = $sinavKategorileri->where('sinav_format', 2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kategori): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo $__env->make('front.sinav.sinav-ekrani-kategoriler', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <br>
                        <hr class="tall">
                    </div>
                    <?php endif; ?>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <!-- Vendor -->
    <script src="<?php echo e(asset('/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/popper/umd/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/common/common.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/isotope/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/vide/vide.min.js')); ?>"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo e(asset('/js/theme.js')); ?>"></script>

    <!-- Current Page Vendor and Views -->
    <script src="<?php echo e(asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')); ?>"></script>

    <!-- Theme Custom -->
    <script src="<?php echo e(asset('/js/custom.js')); ?>"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo e(asset('/js/theme.init.js')); ?>"></script>

    <script>
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: window.location.href,
                image: ''
            }, function(response){});
        }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>