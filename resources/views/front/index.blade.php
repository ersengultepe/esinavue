@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }} </title>
    @stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds" />
    <meta name="description" content="Online Deneme Sınavları">
    @stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/navigation.css') }}">


    @stop

@section('content')
    @include('front.part.header')

    <div class="body" >

        <div role="main" class="main" id="front">

        @include('front.part.slider')
            {{ config('name') }}
        @include('front.part.home-intro')

            @include('front.main.sinavlar')

            @include('front.main.yeni-populer-sinavlar')

            @include('front.main.istatistik')

            @include('front.main.puan-durumu')

            @include('front.main.haftanin-enleri')

        </div>

    </div>

    @stop

@section('js')
    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('/vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('/vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/vendor/vide/vide.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('/js/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('/js/theme.init.js') }}"></script>



    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-12345678-1', 'auto');
        ga('send', 'pageview');
    </script>
     -->
    @stop
