@extends('front.template')

@section('title')
    <title>Onay Kodu Gönder | {{ config('app.name') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Onay Kodu Gönderme Sayfası">
@stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
@stop


@section('content')
    @include('front.part.header')

    <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col">
                    <ul class="breadcrumb">
                        <li><a href="{{ url('/') }}">Ana Sayfa</a></li>
                        <li class="active"><a href="#">Onay Kodu Gönder </a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Onay Kodu Gönder </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Onay Kodu Gönder') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('confirm.send') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Adresiniz') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" maxlength="80" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-dark">
                                        {{ __('Onay Kodu Gönder') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
