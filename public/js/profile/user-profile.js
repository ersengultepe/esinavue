let up = new Vue({
   el: '#profile',
   name: 'UserProfile',
   data:{
       kendiHakkinda: false,
       kendiHakkindaOzet: true,
       gosterGizle: 'Hepsini Göster',
       pVal:{}
   },
    created(){
      this.getUserProfilePageValues();
    },
   methods:{
       kendiHakkindaClick(){
           if(this.kendiHakkindaOzet){
               this.kendiHakkindaOzet = false;
               this.kendiHakkinda = true;
               this.gosterGizle = 'GİZLE';
           }else{
               this.kendiHakkindaOzet = true;
               this.kendiHakkinda = false;
               this.gosterGizle = 'HEPSİNİ GÖSTER';
           }
       },
       getUserProfilePageValues(){
            axios.get(window.location.href.replace('uye', 'get/uye')).then(response =>{
                this.pVal = response.data;
            })
       },
       userPhoto(par){
           if(par.includes('graph.facebook.com')){
               return par;
           }
           return par;
       }
   }
});