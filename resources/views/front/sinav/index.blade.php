@extends('front.template')

@section('title')
<title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
<meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds, {!! $keywords !!}"/>
<meta name="description" content="{!! $sinav->adi !!}">
<meta property="og:title" content="{!! $sinav->adi !!}"/>
<meta property="og:type" content="article" />
<meta property="og:description" content="{{ $keywords }}" />
<meta property="og:site_name" content="esinavsalonu.com" />
<meta property="og:locale" content="tr_TR" />
<meta property="article:author" content="https://www.facebook.com/esinavsalonu/" />
<meta property="article:section" content="Türkiye" />
<meta property="og:url" content="{{ url($sinav->link) }}" />
<meta property="og:image" content="{{ asset('img/esinavsalonu-social.jpg') }}" />
<meta property="og:image:alt" content="esinavsalonu.com" />
<meta property="fb:pages" content="1469945773313460" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@esinavsalonu">
<meta name="twitter:title" content="{!! $sinav->adi !!}">
<meta name="twitter:description" content="{!! $keywords !!}">
<meta name="twitter:creator" content="@esinavsalonu">
<meta name="twitter:image:src" content="{{ asset('img/esinavsalonu-social.jpg') }}">
<meta name="twitter:image:alt" content="{!! $sinav->adi !!}" />
<meta name="twitter:domain" content="http://esinavsalonu.com/">
    @csrf
@stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">
    <style>
        .sidenav {
            width: auto;
            position: fixed;
            z-index: 1;
            top: 30%;
            left: 10px;
            background: #28a745;
            overflow-x: hidden;
            padding: 8px;
        }
    </style>
@stop

@section('content')
    <a id="bastaraf"></a>
    <div v-if="preloadx=== true" id="preloadx">
        <Pencil style="margin-top:30%;margin-left:50%;margin-right:50%;z-index: 9999;position: fixed;"></Pencil>
    </div>
    @include('front.part.header')

    <div role="main" class="main">

        @include('front.sinav.header')

        <div class="container">
            <div class="row">
                <span id="user-id" style="display: none">{{ Auth::id() }}</span>
                <span id="user-kullanici" style="display: none">@auth {{ Auth::user()->kullanici }} @endauth</span>
                <span id="route-url-parameter" style="display: none">{{ Route::current()->parameters()['url'] }}</span>

                @desktop
                <div class="sidenav" style="background: initial;">
                    <div class="heading heading-tertiary heading-border heading-bottom-border">
                        <h3 class="heading-tertiary">Puan :<strong> @{{ toplamPuan }} </strong></h3>
                    </div>
                    <div class="heading heading-secondary heading-border heading-bottom-border">
                        <h3 class="heading-secondary" id="sure"><strong>
                                <time v-model="sure">00:00:00</time>
                            </strong></h3>
                    </div>
                    <h4 class="heading-light background-color-primary" style="background-color: #2baab1 !important">
                        Doğru : @{{ dogrular.length }}</h4>
                    <h4 class="heading-light background-color-primary"
                        style="background-color: rgb(227, 97, 89) !important">Yanlış : @{{ yanlislar.length }}</h4>
                    <h4 class="heading-light background-color-primary"
                        style="background-color: rgb(0, 136, 204) !important">Boş : @{{ (soru_sayisi -
                        Object.keys(secilenSiklar).length) }}</h4>
                </div>
                @enddesktop

                @include('front.sinav.sinav-alani')

                @include('front.sinav.sidebar')
            </div>
        </div>
    </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('js/front/front.js') }}"></script>
    <script src="{{ asset('js/sinav_sayfasi/sinav_values.js') }}"></script>
    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('/js/theme.js') }}"></script>
    <!-- Theme Custom -->
    <script src="{{ asset('/js/custom.js') }}"></script>
    <!-- Theme Initialization Files -->
    <script src="{{ asset('/js/theme.init.js') }}"></script>
    <script>
        let sure = document.getElementById('sure'),
            start = document.getElementById('start'),
            stop = document.getElementById('stop'),
            clear = document.getElementById('clear'),
            seconds = 0, minutes = 0, hours = 0,
            t;
        function add() {
            seconds++;
            if (seconds >= 60) {
                seconds = 0;
                minutes++;
                if (minutes >= 60) {
                    minutes = 0;
                    hours++;
                }
            }
            sure.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" +
                (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" +
                (seconds > 9 ? seconds : "0" + seconds);
            timer();
        }

        function timer() {
            t = setTimeout(add, 1000);
        }

        timer();
    </script>
@stop