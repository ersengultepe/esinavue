<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinavDetay extends Model
{
    protected $table    = 'sinav_detays';

    protected $fillable = [
        'sinav_id',
        'soru_id',
        'ekleyen',
        'ip',
    ];

}
