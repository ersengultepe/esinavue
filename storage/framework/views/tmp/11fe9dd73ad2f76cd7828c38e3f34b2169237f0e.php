<header class="page-header">
    <h2><?php echo e(userTitle(Auth::user()->kullanici)); ?></h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="<?php echo e(url('/')); ?>">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Kullanıcı Profili</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>