@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">
                    <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                        <a href="{{ route('getSinavKategorileri') }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                            Geri Dön </a>
                    </button>
                    Kayıtlı Tüm Sınav Kategorilerimiz
                    <small>Toplam {{ $sinavlar->total() }} Sınav</small>
                </h2>

            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sınav Kategori Adı</th>
                            <th>Ekleyen</th>
                            <th>Eklenme Tarihi</th>
                            <th>Güncellenme Tarihi</th>
                            <th>İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($sinavlar as $sinav)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('getSinavSorulari', ['id'=>$sinav->id]) }}">
                                        {{ str_limit($sinav->adi) }}
                                    </a>
                                </td>
                                <td>{{ optional($sinav->user)->ad }}</td>
                                <td>{{ $sinav->created_at }}</td>
                                <td>{{ $sinav->updated_at }}</td>
                                <td class="actions-hover actions-fade">
                                    <a href=""><i class="fa fa-pencil"></i></a>
                                    <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            {{ $sinavlar->links() }}
        </section>
    </div>
@stop