@extends('admin.template')

@section('css')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />

@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title sinaw" id="{{ $sinavKategori->id }} ">
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                            <a href="{{ url()->previous() }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                                Geri Dön </a>
                        </button>
                        {{ breadCrumb() }} <br>
                        <small>{{ $sinavKategori->kategori_adi }}</small>
                    </h2>

                </header>

                <div class="panel-body">

                    <form id="myForm" class="form-horizontal form-bordered" action="#">

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Toplam Soru Sayısı</label>
                            <div class="col-md-6">
                                <div class="widget-summary widget-summary-sm">
                                    <div class="widget-summary-col widget-summary-col-icon">
                                        <div class="summary-icon bg-primary">
                                            <i class="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div class="widget-summary-col">
                                        <div class="summary">

                                            <div class="info">
                                                <strong class="amount">{{ array_sum($soruAdetleri) }}</strong>
                                                <h4 class="title">Soru Kategorileri eklendikçe eşzamanlı değişecektir</h4>
                                            </div>
                                        </div>
                                        <div class="summary-footer">
                                            <a class="text-muted text-uppercase">(view all)</a>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Sınav Kategorisi</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control sinav-kategori-adi" id="inputDefault" value="{{ $sinavKategori->kategori_adi }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Soru Formatı</label>
                            <div class="col-md-6">
                                <select class="form-control populate sinav-format">
                                        <option value="1" @if($sinavKategori->sinav_format == 1) selected @endif>Ortak Sınav Kategorisi</option>
                                        <option value="2" @if($sinavKategori->sinav_format == 2) selected @endif>Alan Sınavı Kategorisi</option>
                                        <option value="3" @if($sinavKategori->sinav_format == 3) selected @endif>Geçmiş Sınav & Deneme Sınavı</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Soru Kategorileri</label>
                            <div class="col-md-6">
                                <select data-plugin-selectTwo class="form-control populate sinav-kategorisi-soru-kategori-insert" >
                                    @foreach($soruKategorileri as $soruKat)
                                        <option value="{{ $soruKat->id }}" id="opt_{{ $soruKat->id }}">{{ $soruKat->kategori_adi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- start: page -->
                    <div class="panel-body">

                        <table class="table table-bordered table-striped mb-none " id="datatable-editable">
                            <thead>
                            <tr >
                                <th width="80">ID</th>
                                <th>Soru Kategorisi</th>
                                <th>Soru Adedi</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($selectedSoruKategorileri as $selected)
                                <tr class="gradeX" id="{{ $selected->id }}">
                                    <td class="soru-kategori-ids" id="skids_{{ $selected->id }}">{{ $selected->id }}</td>
                                    <td>{{ $selected->kategori_adi }}</td>
                                    <td class="soru-adet-col" >{{ $soruKategoriAndSoruAdedi[$selected->id] }}</td>

                                    <td class="actions">
                                        <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                                        <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                                        <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                    @endforeach

                            </tbody>
                        </table>

                    </div>
                </section>

            <div id="dialog" class="modal-block modal-full-color modal-block-danger mfp-hide">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Emin misin?</h2>
                    </header>
                    <div class="panel-body">
                        <div class="modal-wrapper">
                            <div class="modal-text">
                                <h4><strong>{{ $sinavKategori->kategori_adi }} </strong>sınavının soru kategorisini silmek istiyor musunuz?</h4>
                            </div>
                        </div>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button id="dialogConfirm" class="btn btn-danger">Sil</button>
                                <button id="dialogCancel" class="btn btn-default">İptal</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>


        </div>
    </div>
@stop

@section('js')
    <!-- Specific Page Vendor -->
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/tables/sinav.kategori.datatables.editable.js') }}"></script>
    <script src="{{ asset('/pa/HTML/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>

    <script>
        $('#datatable-editable').on('click', function () {
            let dinamik_soru_sayisi = $('#datatable-editable').DataTable().column( 2 ).data().sum();
            $('.amount').html(dinamik_soru_sayisi);

            let soru_kategori_ids = createArray($('#datatable-editable').DataTable().column( 0 ).data());
            let soru_adetleri = createArray($('#datatable-editable').DataTable().column( 2 ).data());
            let sinav_kategori_adi = $('.sinav-kategori-adi').val();
            let sinav_kategori_id = $('.sinaw').attr('id');
            let sinav_format = $('.sinav-format').val();

            $.ajax({
                type: "POST",
                url: document.location.origin + '/ajax/sinav-kategori/pattern/update',
                data: {
                    'soru_kategori_ids': soru_kategori_ids,
                    'soru_adetleri': soru_adetleri,
                    'sinav_kategori_adi': sinav_kategori_adi,
                    'sinav_kategori_id': sinav_kategori_id,
                    'sinav_format': sinav_format
                }
            }).done(function (data) {

            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'sınav kategorisi güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });

        });

    </script>
@stop
