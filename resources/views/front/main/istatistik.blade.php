<section class="section section-primary">
    <div class="container">
        <div class="row">

            <div class="col-lg-6">

                <h2 class="mb-1">E-sınav Salonu <strong>Rakamları</strong></h2>

                <div class="content-grid content-grid-dashed mt-4 mb-4">
                    <div class="row content-grid-row">
                        <div class="content-grid-item col-lg-6 text-center py-4">
                            <div class="counters">
                                <div class="counter text-color-light">
                                    <strong data-to="{{ $cozulenSoruSayisi->sayi }}" data-append="+">{{ $cozulenSoruSayisi->sayi }}</strong>
                                    <label>Çözülen Soru Sayısı</label>
                                </div>
                            </div>
                        </div>
                        <div class="content-grid-item col-lg-6 text-center py-4">
                            <div class="counters">
                                <div class="counter text-color-light">
                                    <strong data-to="{{ $soruSayisi->sayi }}">{{ $soruSayisi->sayi }}</strong>
                                    <label>Soru Sayısı</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row content-grid-row">
                        <div class="content-grid-item col-lg-6 text-center py-4">
                            <div class="counters">
                                <div class="counter text-color-light">
                                    <strong data-to="{{ $sinavSayisi->sayi }}">{{ $sinavSayisi->sayi }}</strong>
                                    <label>Sınav Sayısı</label>
                                </div>
                            </div>
                        </div>
                        <div class="content-grid-item col-lg-6 text-center py-4">
                            <div class="counters">
                                <div class="counter text-color-light">
                                    <strong data-to="{{ $uyeSayisi->sayi }}">{{ $uyeSayisi->sayi }}</strong>
                                    <label>Üye Sayısı</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>