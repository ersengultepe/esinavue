<header id="header" class="header-no-min-height"
        data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0'}">
    <div class="header-body">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <a href="{{ url('/') }}">
                            <img alt="esinavsalonu.com" width="150" height="48"
                                 src="{{ asset('img/assets/logo.png') }}">
                        </a>
                        <span class="alternative-font "><em>EsinavSalonu.Com</em></span>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-stripe">
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="dropdown-item @if(Route::currentRouteName() === 'anasayfa') active @endif"
                                               href="{{ url('/') }}">
                                                Anasayfa
                                            </a>
                                        </li>
                                        <li>
                                            @include('front.menu.yks')
                                        </li>
                                        <li>
                                            <a class="dropdown-item @if(Route::currentRouteName() === 'ekran') active @endif"
                                               href="{{ route('ekran') }}">
                                                Sınav Ekranı
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item @if(Route::currentRouteName() === 'sss') active @endif"
                                               href="{{ route('sss') }}">
                                                Sık Sorulan Sorular
                                            </a>
                                        </li>
                                        @guest()
                                            <li>
                                                <a class="dropdown-item" href="{{ route('login') }}">
                                                    <img style="height: 50px;margin-left: 8px;margin-top: -10px"
                                                         data-toggle="tooltip" data-placement="bottom" title=""
                                                         data-original-title="Giriş Yap"
                                                         src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8cGF0aCBzdHlsZT0iZmlsbDojNTQ2RTdBOyIgZD0iTTQxNS4zNDQsMjc2Ljg4NWMtNDEuMjM3LDAtNzQuNjY3LDMzLjQyOS03NC42NjcsNzQuNjY3djIxLjMzM2MwLDUuODkxLDQuNzc2LDEwLjY2NywxMC42NjcsMTAuNjY3ICBjNS44OTEsMCwxMC42NjctNC43NzYsMTAuNjY3LTEwLjY2N3YtMjEuMzMzYzAtMjkuNDU1LDIzLjg3OC01My4zMzMsNTMuMzMzLTUzLjMzM3M1My4zMzMsMjMuODc4LDUzLjMzMyw1My4zMzN2MjEuMzMzICBjMCw1Ljg5MSw0Ljc3NiwxMC42NjcsMTAuNjY3LDEwLjY2N2M1Ljg5MSwwLDEwLjY2Ny00Ljc3NiwxMC42NjctMTAuNjY3di0yMS4zMzNDNDkwLjAxMSwzMTAuMzE1LDQ1Ni41ODEsMjc2Ljg4NSw0MTUuMzQ0LDI3Ni44ODV6ICAiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGQzEwNzsiIGQ9Ik0zMzAuMDExLDM2Mi4yMTloMTcwLjY2N2M1Ljg5MSwwLDEwLjY2Nyw0Ljc3NiwxMC42NjcsMTAuNjY3djEyOCAgYzAsNS44OTEtNC43NzYsMTAuNjY3LTEwLjY2NywxMC42NjdIMzMwLjAxMWMtNS44OTEsMC0xMC42NjctNC43NzYtMTAuNjY3LTEwLjY2N3YtMTI4ICBDMzE5LjM0NCwzNjYuOTk0LDMyNC4xMTksMzYyLjIxOSwzMzAuMDExLDM2Mi4yMTl6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiM0NTVBNjQ7IiBkPSJNMjk4LjAxMSw1MDAuODg1di0xMjhjMC4wNzEtMTMuODU3LDkuMDUzLTI2LjA5MywyMi4yNTEtMzAuMzE1YzAuODMxLTcuNjY0LDIuNTg2LTE1LjIsNS4yMjctMjIuNDQzICBjLTEuODI2LTUuNTgtMy4yNTItMTEuMjg0LTQuMjY3LTE3LjA2N2MxNy40ODgtMTQuNTA1LDI4Ljc0NC0zNS4xNjgsMzEuNDQ1LTU3LjcyOGMtMC4zNzktMy44NDcsMC41MjktNy43MTEsMi41ODEtMTAuOTg3ICBjMy4zMjYtMC44MzMsNi4wNDktMy4yMTUsNy4zMTctNi40YzYuMTUzLTE0Ljg2OCwxMC4wMDktMzAuNTg2LDExLjQzNS00Ni42MTNjLTAuMDAxLTAuODctMC4xMDgtMS43MzctMC4zMi0yLjU4MSAgYy0xLjUzLTYuMjI2LTUuMTktMTEuNzIxLTEwLjM0Ny0xNS41MzF2LTU2LjU1NUMzNjEuMDAxLDQ4Ljc1LDMxNC41ODMsMi4zMzIsMjU2LjY2NywwQzE5OC43NSwyLjMzMiwxNTIuMzMyLDQ4Ljc1LDE1MCwxMDYuNjY3ICB2NTYuNTU1Yy01LjE1NiwzLjgxLTguODE3LDkuMzA1LTEwLjM0NywxNS41MzFjLTAuMjEyLDAuODQ0LTAuMzE5LDEuNzExLTAuMzIsMi41ODFjMS40MjQsMTYuMDM1LDUuMjgsMzEuNzYsMTEuNDM1LDQ2LjYzNSAgYzAuOTI0LDMuMDE1LDMuMzQ3LDUuMzM0LDYuNCw2LjEyM2MxLjE3MywwLjU5NywzLjQxMywzLjY5MSwzLjQxMywxMS4yNDNjMi43MTMsMjIuNTg4LDE0LjAwMSw0My4yNywzMS41MzEsNTcuNzcxICBjLTIuMTMzLDEzLjk3My0xMi42NTEsNTAuODE2LTM0LjkwMSw2MC4xMTdMNjIuNjQsMzk0LjY2N2MtMjQuMjkyLDguMDM0LTQyLjc0NywyNy45OTUtNDguODUzLDUyLjg0M2wtMTIuOCw1MS4yICBjLTEuNDQ5LDUuNzEsMi4wMDUsMTEuNTE0LDcuNzE1LDEyLjk2M2MwLjg2LDAuMjE4LDEuNzQ0LDAuMzI4LDIuNjMyLDAuMzI4aDI4OC43NjggIEMyOTguNzQzLDUwOC40NTEsMjk4LjAzNSw1MDQuNjg2LDI5OC4wMTEsNTAwLjg4NXoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZBRkFGQTsiIGQ9Ik00MzYuNjc3LDQyNi4yMTljMC0xMS43ODItOS41NTEtMjEuMzMzLTIxLjMzMy0yMS4zMzNjLTExLjc4MiwwLTIxLjMzMyw5LjU1MS0yMS4zMzMsMjEuMzMzICBjMC4wOTUsNy40ODQsNC4xNTYsMTQuMzU1LDEwLjY2NywxOC4wNDh2MTMuOTUyYzAsNS44OTEsNC43NzYsMTAuNjY3LDEwLjY2NywxMC42NjdjNS44OTEsMCwxMC42NjctNC43NzYsMTAuNjY3LTEwLjY2N3YtMTMuOTUyICBDNDMyLjUyMSw0NDAuNTc0LDQzNi41ODIsNDMzLjcwMyw0MzYuNjc3LDQyNi4yMTl6Ii8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="/>
                                                </a>
                                            </li>

                                            <li class="@if(Route::currentRouteName() === 'login') active @endif dropdown ">
                                                <a class="dropdown-item dropdown-toggle" href="#">
                                                    Giriş
                                                </a>

                                                <ul class="dropdown-menu">
                                                    {{ Session::put('login-after-url', Request::path() ) }}
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="{{ route('login') }}"><i
                                                                    class="fas fa-sign-in-alt"></i> Site Girişi</a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item"
                                                           href="{{ url('auth/redirect/facebook') }}"><i
                                                                    class="fab fa-facebook-f"></i> Facebook Giriş </a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item"
                                                           href="{{ url('auth/redirect/google') }}"><i
                                                                    class="fab fa-google"></i> Google Giriş</a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item"
                                                           href="{{ url('auth/redirect/twitter') }}"><i
                                                                    class="fab fa-twitter"></i> Twitter Giriş</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        @endguest
                                    </ul>
                                </nav>
                            </div>

                            @auth()

                                <div class="testimonial testimonial-primary">
                                    <div class="testimonial-author">
                                        <div class="testimonial-author-thumbnail img-thumbnail">
                                            <a href="{{ route('account.management', ['link'=>Auth::id().'-'.str_slug(Auth::user()->kullanici)]) }}">
                                                <img data-toggle="tooltip" data-placement="bottom" title=""
                                                     data-original-title="{{ Auth::user()->kullanici }}"
                                                     src="{{ userPhoto(Auth::user()) }}"
                                                     alt="{{ Auth::user()->kullanici }}" style="max-height: 50px">
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <img style="height: 50px;margin-left: 8px;margin-top: -10px" data-toggle="tooltip"
                                         data-placement="bottom" title="" data-original-title="Çıkış Yap"
                                         src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUzIDUzIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MyA1MzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiM1NTYwODA7IiBwb2ludHM9IjAuNSwwIDAuNSw0NiAyMi41LDQ2IDM0LjUsNDYgMzQuNSwyOSAzNC41LDE3IDM0LjUsMCAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzczODNCRjsiIHBvaW50cz0iMjIuNSw3IDAuNSwwIDAuNSw0NiAyMi41LDUzICIvPgo8bGluZSBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojRUZDRTRBO3N0cm9rZS13aWR0aDoyO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoxMDsiIHgxPSIzOS41IiB5MT0iMzUiIHgyPSI1MS41IiB5Mj0iMjMiLz4KPGxpbmUgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I0VGQ0U0QTtzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MTA7IiB4MT0iMzkuNSIgeTE9IjExIiB4Mj0iNTEuNSIgeTI9IjIzIi8+CjxsaW5lIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNFRkNFNEE7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwOyIgeDE9IjUxLjUiIHkxPSIyMyIgeDI9IjI3LjUiIHkyPSIyMyIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K"/>
                                </a>
                            @endauth

                            <ul class="header-social-icons social-icons d-none d-sm-block">
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/esinavsalonu/"
                                                                     target="_blank" title="Facebook"><i
                                                class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/esinavsalonu"
                                                                    target="_blank" title="Twitter"><i
                                                class="fab fa-twitter"></i></a></li>
                            </ul>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                    data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>

{{--

<header id="header"
        data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 164, 'stickySetTop': '-164px', 'stickyChangeLogo': false}">
    <div class="header-body border-0">
        <div class="header-top header-top-default border-bottom-0 bg-color-primary" style="background-color: #81b3b3;">
            <div class="container">
                <div class="header-row py-2">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills text-uppercase text-2">
                                    <li class="nav-item nav-item-anim-icon"><a
                                                class="nav-link pl-0 text-light opacity-7" href="about-us.html"><i
                                                    class="fas fa-angle-right"></i> <strong>About Us</strong></a></li>
                                    <li class="nav-item nav-item-anim-icon"><a
                                                class="nav-link text-light opacity-7 pr-0" href="contact-us.html"><i
                                                    class="fas fa-angle-right"></i> <strong>Contact Us</strong></a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-row">
                            <ul class="header-social-icons social-icons d-none d-sm-block social-icons-clean social-icons-icon-light">
                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank"
                                                                     title="Facebook"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank"
                                                                    title="Twitter"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank"
                                                                     title="Linkedin"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row py-3">
                <div class="header-column justify-content-start w-50 order-md-1 d-none d-md-flex">
                    <div class="header-row">
                        <ul class="header-extra-info">
                            <li class="m-0">
                                <div class="feature-box feature-box-style-2 align-items-center">
                                    <div class="feature-box-icon">
                                        <i class="far fa-clock text-7 p-relative"></i>
                                    </div>
                                    <div class="feature-box-info">
                                        <p class="pb-0 font-weight-semibold line-height-5 text-2">MON - FRI: 10:00 -
                                            18:00<br>SAT - SUN: 10:00 - 14:00</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="header-column justify-content-start justify-content-md-center order-1 order-md-2">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{ url('/') }}">
                                <img alt="esinavsalonu.com" width="150" height="48" src="{{ asset('img/assets/logo.png') }}">
                            </a>
                            <span class="alternative-font "><em>EsinavSalonu.Com</em></span>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end w-50 order-2 order-md-3">
                    <div class="header-row">
                        <ul class="header-extra-info">
                            <li class="m-0">
                                <div class="feature-box reverse-allres feature-box-style-2 align-items-center">
                                    <div class="feature-box-icon">
                                        <i class="fab fa-whatsapp text-7 p-relative" style="top: -2px;"></i>
                                    </div>
                                    <div class="feature-box-info">
                                        <p class="pb-0 font-weight-semibold line-height-5 text-2">(123) 456-7890<br>(123)
                                            456-7891</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-nav-bar header-nav-bar-top-border bg-light">
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column">
                        <div class="header-row justify-content-end">
                            <div class="header-nav p-0">
                                <div class="header-nav header-nav-line header-nav-divisor header-nav-spaced justify-content-lg-center">
                                    <div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                        <nav class="collapse">
                                            <ul class="nav nav-pills flex-column flex-lg-row" id="mainNav">
                                                @include('front.menu.yks')

                                                @include('front.menu.gys')
                                                <li class="dropdown"><a class="dropdown-item dropdown-toggle" href="#">
                                                        Portfolio </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Single
                                                                Project</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-wide-slider.html">Wide
                                                                        Slider</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-small-slider.html">Small
                                                                        Slider</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-full-width-slider.html">Full
                                                                        Width Slider</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-gallery.html">Gallery</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-carousel.html">Carousel</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-medias.html">Medias</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-full-width-video.html">Full
                                                                        Width Video</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-masonry-images.html">Masonry
                                                                        Images</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-left-sidebar.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-right-sidebar.html">Right
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-left-and-right-sidebars.html">Left
                                                                        and Right Sidebars</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-sticky-sidebar.html">Sticky
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-single-extended.html">Extended</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Grid
                                                                Layouts</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-1-column.html">1 Column</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-2-columns.html">2
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-3-columns.html">3
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-4-columns.html">4
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-5-columns.html">5
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-6-columns.html">6
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-no-margins.html">No
                                                                        Margins</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-full-width-no-margins.html">Full
                                                                        Width No Margins</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-grid-1-column-title-and-description.html">Title
                                                                        and Description</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Masonry
                                                                Layouts</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-2-columns.html">2
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-3-columns.html">3
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-4-columns.html">4
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-5-columns.html">5
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-6-columns.html">6
                                                                        Columns</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-no-margins.html">No
                                                                        Margins</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-masonry-full-width.html">Full
                                                                        Width</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Sidebar
                                                                Layouts</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-sidebar-right.html">Right
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-sidebar-left-and-right.html">Left
                                                                        and Right Sidebars</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-sidebar-sticky.html">Sticky
                                                                        Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Ajax</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-ajax-page.html">Ajax on Page</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-ajax-modal.html">Ajax on
                                                                        Modal</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Extra</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-timeline.html">Timeline</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-lightbox.html">Lightbox</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-load-more.html">Load
                                                                        More</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-infinite-scroll.html">Infinite
                                                                        Scroll</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-pagination.html">Pagination</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="portfolio-extra-combination-filters.html">Combination
                                                                        Filters</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown"><a class="dropdown-item dropdown-toggle" href="#">
                                                        Blog </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Large
                                                                Image</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="blog-large-image-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-large-image-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-large-image-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-large-image-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Medium
                                                                Image</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="blog-medium-image-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-medium-image-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Grid</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-4-columns.html">4 Columns</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-3-columns.html">3 Columns</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-full-width.html">Full Width</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-no-margins.html">No Margins</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-no-margins-full-width.html">No
                                                                        Margins Full Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-grid-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Masonry</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-4-columns.html">4 Columns</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-3-columns.html">3 Columns</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-no-margins.html">No
                                                                        Margins</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-no-margins-full-width.html">No
                                                                        Margins Full Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-masonry-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Timeline</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item" href="blog-timeline.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-timeline-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-timeline-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Single
                                                                Post</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item" href="blog-post.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-slider-gallery.html">Slider
                                                                        Gallery</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-image-gallery.html">Image
                                                                        Gallery</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-embedded-video.html">Embedded
                                                                        Video</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-html5-video.html">HTML5 Video</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-blockquote.html">Blockquote</a>
                                                                </li>
                                                                <li><a class="dropdown-item" href="blog-post-link.html">Link</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-embedded-audio.html">Embedded
                                                                        Audio</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-small-image.html">Small Image</a>
                                                                </li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Post
                                                                Comments</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post.html#comments">Default</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-comments-facebook.html#comments">Facebook
                                                                        Comments</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="blog-post-comments-disqus.html#comments">Disqus
                                                                        Comments</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown"><a class="dropdown-item dropdown-toggle" href="#">
                                                        Shop </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Single
                                                                Product</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="shop-product-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-product-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-product-sidebar-right.html">Right
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-product-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a class="dropdown-item" href="shop-4-columns.html">4
                                                                Columns</a></li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">3
                                                                Columns</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="shop-3-columns-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-3-columns-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-3-columns-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">2
                                                                Columns</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="shop-2-columns-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-2-columns-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-2-columns-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-2-columns-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">1
                                                                Column</a>
                                                            <ul class="dropdown-menu">
                                                                <li><a class="dropdown-item"
                                                                       href="shop-1-column-full-width.html">Full
                                                                        Width</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-1-column-sidebar-left.html">Left
                                                                        Sidebar</a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-1-column-sidebar-right.html">Right
                                                                        Sidebar </a></li>
                                                                <li><a class="dropdown-item"
                                                                       href="shop-1-column-sidebar-left-and-right.html">Left
                                                                        and Right Sidebar</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a class="dropdown-item" href="shop-cart.html">Cart</a></li>
                                                        <li><a class="dropdown-item" href="shop-login.html">Login</a>
                                                        </li>
                                                        <li><a class="dropdown-item"
                                                               href="shop-checkout.html">Checkout</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                            data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
--}}
