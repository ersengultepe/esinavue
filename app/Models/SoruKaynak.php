<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoruKaynak extends Model
{
    protected $table = 'soru_kaynaklari';

    protected $fillable = [
        //sinav kaynağı örnek : Gazi Üniversitesi Sgk Görevde Yükselme Sınavı 2016
        'ad',
        'tarih', //2016
        'telif', //0=>Hayır, 1=>Evet
        'duzenleyen_kurum', //gazi üniversitesi
        'duzenlenen_kurum', // sgk
        'user_id', //kayıt atan kullanıcı
        'ip',
        'soru_sayisi'
    ];



}
