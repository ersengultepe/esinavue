<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //login-register sayfasında session oluşturulur, login olunca kaldığı sayfaya döndürülür
        if (Auth::guard($guard)->check()) {
              $urlPrev = Session::get('url-previous');
              Session::forget('url-previous');
            return redirect($urlPrev);
        }

        return $next($request);
    }
}
