<!-- start: sidebar -->
<aside class="sidebar-left " id="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            www.esinavsalonu.com
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li>
                        <a href="{{ route('account.management', ['link' => Auth::id().'-'.str_slug(Auth::user()->kullanici)]) }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span> Hesap Yönetimi</span>
                        </a>
                    </li>
                    <li class="nav-parent {{ (request()->segment(2) == 'soru') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-space-shuttle" aria-hidden="true"></i>
                            <span>Soru Yönetimi</span>
                        </a>
                        <ul class="nav nav-children" id="eee">
                            <li class="{{ (request()->segment(3) === 'ekle') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSoruEkle') }}">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Soru Ekle
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'soru-kategori') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSoruKategorileri') }}">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                                    Kategori Bazında Sorular
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'kaynak') ? 'nav-active' : '' }}">
                                <a href="#">
                                    <i class="fa fa-fire-extinguisher" aria-hidden="true"></i>
                                    Kaynak Bazında Sorular
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'onaysiz') ? 'nav-active' : '' }}">
                                <a href="{{ route('getOnaysiz') }}">
                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                    Onaysız Sorular
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'cevapsiz') ? 'nav-active' : '' }}">
                                <a href="{{ route('getCevapsiz') }}">
                                    <i class="fa fa-square-o" aria-hidden="true"></i>
                                    Cevapsız Sorular
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'kategorisiz-sorular') ? 'nav-active' : '' }}">
                                <a href="{{ route('getKategorisizSorular') }}">
                                    <i class="fa fa-square-o" aria-hidden="true"></i>
                                    Kategorisiz Sorular
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ (request()->segment(2) == 'soru-kaynak') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-institution" aria-hidden="true"></i>
                            <span>Soru Kaynak Yönetimi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ (request()->segment(3) === 'listele') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSoruKaynakListele') }}">
                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                    Soru Kaynakları
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'ekle') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSoruKaynakEkle') }}">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                                    Soru Kaynak Ekle
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ (request()->segment(2) == 'sinav') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-institution" aria-hidden="true"></i>
                            <span>Sınav Yönetimi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ (request()->segment(3) === 'listele' || request()->segment(4) === 'sorular') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSinavlar') }}">
                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                    Sınavlar
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'sinav-kategori') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSinavKategorileri') }}">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                                    Sınav Kategorileri
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'tipler' || request()->segment(3) === 'sinav-tip'|| request()->segment(3) === 'tip')   ? 'nav-active' : '' }}">
                                <a href="{{ route('getSinavTip') }}">
                                    <i class="fa fa-fire-extinguisher" aria-hidden="true"></i>
                                    Sınav Tipleri
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'onaysiz') ? 'nav-active' : '' }}">
                                <a href="{{ route('getOnaysizSinavlar') }}">
                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                    Onaysız Sınavlar
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'kategorisiz') ? 'nav-active' : '' }}">
                                <a href="{{ route('getKategorisizSinavlar') }}">
                                    <i class="fa fa-square-o" aria-hidden="true"></i>
                                    Kategorisiz Sınavlar
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ (request()->segment(2) == 'yorum') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-comment" aria-hidden="true"></i>
                            <span>Yorum Yönetimi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ (request()->segment(3) === 'onaylananlar') ? 'nav-active' : '' }}">
                                <a href=" {{ route('yorumlar') }}">
                                    <i class="fa fa-comments" aria-hidden="true"></i>
                                    Onaylanan Yorumlar
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'onay-bekleyenler') ? 'nav-active' : '' }}">
                                <a href="{{ route('onaysizYorumlar') }}">
                                    <i class="fa fa-comment-o" aria-hidden="true"></i>
                                    Onay Bekleyen Yorumlar
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'silinenler') ? 'nav-active' : '' }}">
                                <a href="{{ route('silinenYorumlar') }}">
                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    Silinen Yorumlar
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ (request()->segment(2) == 'uye') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-comment" aria-hidden="true"></i>
                            <span>Üye Yönetimi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ (request()->segment(3) === 'onaylananlar') ? 'nav-active' : '' }}">
                                <a href=" {{ route('getOnayliUyeler') }}">
                                    <i class="fa fa-comments" aria-hidden="true"></i>
                                    Onaylanan Üyeler
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'onay-bekleyenler') ? 'nav-active' : '' }}">
                                <a href="{{ route('getOnayBekleyenUyeler') }}">
                                    <i class="fa fa-comment-o" aria-hidden="true"></i>
                                    Onay Bekleyen Üyeler
                                </a>
                            </li>
                            <li class="{{ (request()->segment(3) === 'silinenler') ? 'nav-active' : '' }}">
                                <a href="{{ route('getSilinenUyeler') }}">
                                    <i class="fa fa-comments-o" aria-hidden="true"></i>
                                    Silinen Üyeler
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent {{ (request()->segment(2) == 'ebulten') ? 'nav-active nav-expanded' : '' }}">
                        <a>
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            <span>Ebülten Yönetimi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li class="{{ (request()->segment(2) === 'ebulten') ? 'nav-active' : '' }}">
                                <a href="{{ route('ebulten') }}">
                                    <i class="fa fa-cubes" aria-hidden="true"></i>
                                    Ebülten Aboneleri
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>

            <hr class="separator" />

        </div>

    </div>

</aside>
<!-- end: sidebar -->


