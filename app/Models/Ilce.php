<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ilce extends Model
{
    protected $table = 'ilces';

    protected $fillable = [
        'sehir_id',
        'ilce'
    ];

    public function sehir(){
        return $this->belongsTo('App\Sehir');
    }

}


