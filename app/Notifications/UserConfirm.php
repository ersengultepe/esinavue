<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Log;

class UserConfirm extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        Log::info($notifiable->kullanici.' isimli kullanıcı üyeliğini aktif hale getirmeyi başardı...');
        return (new MailMessage)
                    ->greeting('Tebrikler'. ' '. $notifiable->ad)
                    ->subject('EsinavSalonu.com | Üyeliğiniz Aktif Oldu...')
                    ->line('EsinavSalonu.com Üyeliğinizi Aktif Hale Getirdiniz.')
                    ->line('Sınavlarımızı isterseniz hemen çözmeye başlayabilirsiniz.')
                    ->action('Sınav Ekranı', route('ekran'))
                    ->line('Çalışmalarınızda başarılar dileriz.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
