<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Takim extends Model
{
    protected $table = 'takims';

    protected $fillable = [
        'takim',
        'logo'
    ];
}
