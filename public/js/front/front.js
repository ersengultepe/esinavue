let fr = new Vue({
    el: '#footer',
    name: 'Front',
    data:{
        ebultenemail: '',
        message: null,
        ad_soyad: null,
        email: null,
        mesaj: null,
        sendmesaj:null,
        preloadebulten: false,
        preloademesaj: false,
        userId: document.getElementById('user-id').textContent,
        mesajAlani:'initial',
        ebultenAlani:'inline-flex'
    },
    methods:{
        ebultenKayit()
        {
            this.preloadebulten = true;
            axios.post('https://'+window.location.host+'/set/ebulten', {
                email : this.ebultenemail,
            }).then(response =>{
                this.message = response.data;
                this.ebultenemail = '';

                this.ebultenAlani='none';
                this.preloadebulten = false;
            })
        },
        ebultenChange()
        {
            this.message = null;
        },
        mesajGonder()
        {
            this.preloademesaj = true;
            axios.post('https://'+window.location.host+'/set/mesaj-gonder',{
                email : this.email,
                ad_soyad : this.ad_soyad,
                mesaj: this.mesaj,
                userId: this.userId
            }).then(response =>{
                this.sendmesaj = response.data;
                setTimeout(() =>{

                    this.mesajAlani = 'none';
                    this.preloademesaj = false;

                },1200);

                if(this.sendmesaj.ok !== undefined)
                {
                    this.email = '';
                    this.mesaj = '';
                    this.ad_soyad = '';

                }
            });
        },
        mesajChange()
        {
            this.sendmesaj = null;
        }
    }
});