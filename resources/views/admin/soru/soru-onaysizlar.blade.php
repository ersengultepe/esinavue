@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
@stop

@section('content')
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                    <a href="{{ route('getSoruKategorileri') }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                        Geri Dön </a>
                </button>
                <span>{{ str_limit(ucWordsTr(mb_strtolower(\Illuminate\Support\Facades\Session::get('breadcrumb'), 'UTF-8'))) }} </span>
                <small>Toplam {{ $sorular->total() }} Soru</small>

            </h2>
        </header>
        {{ $sorular->links() }}
        <div class="panel-body">
            @if(count($sorular) > 0)

                <?php $i = 1; $abcde = ['A', 'B', 'C', 'D', 'E']; $j = 0; ?>
                @foreach($sorular as $soru)
                    <div class="col-md-6">

                        <div class="form-group">
                            <button type="button" id="bt_{{ $soru->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Güncelle"
                                    class="col-md-2 mb-xs mt-xs mr-xs btn btn-primary soru-update" style="margin-bottom: 1px;margin-top: 1px !important;">
                                <i class="fa fa-edit"></i> Soru {{ $loop->iteration }}</button>
                            <div class="switch switch-sm switch-success" id="swr_{{ $soru->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Onay Durumu Güncelle">
                                <input type="checkbox" class="soru-onay-update" id="sw_{{ $soru->id }}" name="switch" data-plugin-ios-switch @if($soru->durum === 1) checked @endif />
                            </div>
                            <div class="col-md-8" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Kategorisini Güncelle">
                                <select name="soru_kategori" data-plugin-selectTwo class="form-control populate soru-kategori-update" id="sc_{{ $soru->id }}">
                                    @foreach($soruKategorileri as $kategori)
                                        <option value="{{ $kategori->id }}" @if($kategori->id === $soru->soru_kategori_id)selected @endif >{!! $kategori->kategori_adi !!}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <textarea name="soru" class="ckeditor" id="tx_{{  $soru->id  }}" rows="2">{!! turkceAsci($soru->soru) !!}</textarea>

                        @foreach($soru->cevaplar as $cevap)
                            <div class="form-group @if($cevap->dogru == 1 ) has-success @endif" style="margin: 1px !important;">
                                <button class="col-md-1 btn btn-outline-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cevap Güncelle">
                                    <a href="{{ route('getCevapUpdate', ['soru_id'=>$soru->id, 'cevap_id'=>$cevap->id]) }}">
                                        {{ $abcde[$loop->index].') ' }}
                                    </a>
                                </button>
                                <div class="col-md-11">
                                    <div class="input-group input-group-icon">
                                        @if($cevap->dogru == 1 )
                                            <span class="input-group-addon">
												<span class="icon"><i class="fa fa-check"></i></span>
											</span>
                                        @endif
                                        <input name="cevap" class="form-control" type="text" size="90" id="cevap{{ $cevap->id }}"
                                               value="{!! turkceAsci($cevap->cevap) !!}" >
                                    </div>

                                </div>
                            </div>
                        @endforeach

                    </div>
                    @if($loop->iteration%2==0)
                        <div class="col-md-12">
                            <hr style="font-size: 3em">
                        </div>
                    @endif

                @endforeach
                <br>

            @else
                <div class="col-md-12">
                    <h4>Listelenecek Soru Bulunamadı..!</h4>
                </div>
            @endif
        </div>
        {{ $sorular->links() }}
    </section>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.modals.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>

    <script>
        $('.soru-update').click(function () {
            let buton_id = this.id;
            soru_id = buton_id.replace('bt_', '');
            updateSoru = CKEDITOR.instances['tx_'+soru_id].getData();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/soru/update',
                data:{
                    'id': soru_id,
                    'soru': updateSoru
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Soru güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.soru-kategori-update').change(function () {
            let select_id = this.id;
            soru_id = select_id.replace('sc_', '');
            soru_kategori_id = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/soru/kategori-update',
                data:{
                    'soru_id': soru_id,
                    'soru_kategori_id': soru_kategori_id,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Soru kategorisi güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.soru-onay-update').change(function () {
            let chk_id = this.id;
            let soru_id = chk_id.replace('sw_', '');
            let durum = $('#swr_' + soru_id + ' .ios-switch').hasClass('on') ? 1 : 0;

            $.ajax({
                type: "POST",
                url: document.location.origin + '/ajax/soru/durum-update',
                data: {
                    'soru_id': soru_id,
                    'durum': durum
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Cevap güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });
    </script>
@stop
