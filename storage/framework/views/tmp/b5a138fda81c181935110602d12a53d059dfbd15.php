<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="heading heading-border heading-bottom-border">
                        <h3><strong>En Çok Çözülen </strong>Sınavlar</h3>
                    </div>

                    <?php $__currentLoopData = $enCokCozulenSinavlar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sinav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="feature-box">
                            <div class="feature-box-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($sinav->adi); ?>">
                                <h4 class="heading-primary mb-0">
                                    <a  href="<?php echo e(route('sinav', [$sinav->link])); ?>" style="text-decoration: none">
                                        <?php echo e(str_limit(' '.$sinav->adi,43)); ?>

                                    </a>
                                </h4>

                                <br>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
                <div class="col-sm-6">

                    <div class="heading heading-border heading-bottom-border">
                        <h3><strong>Yeni</strong> Eklenen Sınavlar</h3>
                    </div>

                    <?php $__currentLoopData = $yeniEklenenSinavlar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sinav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="feature-box">
                            <div class="feature-box-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($sinav->adi); ?>">
                                <h4 class="heading-primary mb-0">
                                    <a  href="<?php echo e(route('sinav', [$sinav->link])); ?>" style="text-decoration: none">
                                        <?php echo e(str_limit($sinav->adi,43)); ?>

                                    </a>
                                </h4>

                                <br>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
    </div>
</div>