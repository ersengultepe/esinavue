<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegister extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $confirmLink = env('APP_URL').'/confirm_account/'.$notifiable->md5_password.'-'.$notifiable->id;
        return (new MailMessage)
                    ->greeting('Merhaba'.' '.$notifiable->ad)
                    ->subject('EsinavSalonu.com | Kayıt Oldunuz..!')
                    ->line("Esinavsalonu.com sitesine üye oldunuz. Görevde Yükselme ve Unvan Değişikliği, KPSS, YDS, Ehliyet vb. sınavları online olarak ücretsiz çözebilirsiniz.")
                    ->line("Çıkmış sorular ve deneme sınavlarından faydalanabilirsiniz.")
                    ->line("Üyeliğinizi Aktifleştirmek için aşağıdaki butona tıklayınız.")
                    ->action('Onayla', url($confirmLink))
                    ->line('Tüm soru ve önerileriniz için [Sık Sorulan Sorular](http://www.esinavsalonu.com/sss) sayfamızdan destek alabilirsiniz.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
