<?php

namespace App\Http\Controllers;

use App\Models\Cevap;
use App\Models\Sinav;
use App\Models\SinavDetay;
use App\Models\SinavKategori;
use App\Models\SinavTip;
use App\Models\Soru;
use App\Models\SoruKategori;
use App\Models\UyelerDetay;
use App\Models\Yorum;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Image;

class SinavController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Admin Metodları
    |--------------------------------------------------------------------------
    | Kokpit diye adlandırdığım Admin panelinde çalışan ekranların metodları
    |
    */
    public function indexSinavlar() : View
    {
        $sinavlar = Sinav::where('durum',1)->orderBy('adi')->paginate(50);
        $soruKategorileri = SoruKategori::all();
        Session::put('breadcrumb', 'Sınavlar');
        return view('admin.sinav.sinavlar',compact('sinavlar', 'soruKategorileri'));
    }

    public function getUpdate(Int $sinavId)  : View
    {
        $sinav = Sinav::find($sinavId);
        $sinavKategorileri = SinavKategori::all();
        return view('admin.sinav.sinav-edit-page', compact('sinav', 'sinavKategorileri'));
    }

    public function ajaxSinavDurumUpdate(Request $request)  : array
    {
        Sinav::find($request->sinav_id)->update([
            'durum' => $request->durum
        ]);

        return ['title'=> 'Başarılı', 'text' => 'Sınav durumu başarıyla güncellendi...'];
    }

    public function ajaxSinavNameUpdate(Request $request)  : array
    {
        Sinav::find($request->sinav_id)->update([
            'adi' => $request->name,
            'link' => str_slug($request->name)
        ]);

        return ['title'=> 'Başarılı', 'text' => 'Sınav adı başarıyla güncellendi...'];
    }

    public function ajaxSinavKategoriUpdate(Request $request) : array
    {
        Sinav::find($request->sinav_id)->update([
            'kategori_id' => $request->sinav_kategori_id
        ]);

        return ['title'=> 'Başarılı', 'text' => 'Sınav adı başarıyla güncellendi...'];
    }

    public function ajaxSinavDelete(Request $request) : array
    {
        SinavDetay::where('sinav_id', $request->sinav_id)->delete();
        Sinav::find($request->sinav_id)->delete();

        return ['title'=> 'Başarılı', 'text' => 'Sınav başarıyla veritabanından silindi...'];
    }

    public function getSinavKategorileri() : View
    {
        $kategoriler = SinavKategori::where('durum',1)->orderBy('kategori_adi')->get();
        Session::put('breadcrumb', 'Sınav Kategorileri');
        return view('admin.sinav.sinav-kategorileri',compact('kategoriler'));
    }

    public function getKatSinavInsert() : View
    {
        $soruKategorileri = SoruKategori::all();
        return view('admin.sinav.sinav-kategori-insert', compact('soruKategorileri'));
    }

    public function ajaxSinavKategoriPatternInsert(Request $request) : array
    {
        $insert = new SinavKategori();
        $insert->kategori_adi = $request->sinav_kategori_adi;
        $insert->sinav_format = $request->sinav_format;
        $insert->soru_kategori_ids = implode(',', $request->soru_kategori_ids);
        $insert->soru_adetleri = implode(',', $request->soru_adetleri);
        $insert->durum = 1;
        $insert->ekleyen = Auth::id();
        $insert->ip = $request->ip();
        $insert->save();

        return ['title'=> 'Başarılı', 'text'=> 'Sınav Kategorisi başarıyla eklendi..:)'];
    }

    public function ajaxSinavKategoriSinavInsert(Int $id)
    {
        try{
            /**
             * $id sınav kategorisinin soru kategorileri ile soru adetleri ele alınır,
             * DB'de bulunan sorularlar karşılaştırılır,
             * Bu sınav kategorisinden üretilen sınavlarda kullanılan sorular hariç tutularak tabiki
             * Kullanılabilecek soru sayıları ve adetleri tespit edilir.
             * En soruya sahip soru kategorisine göre değerlendirilerek üretilebilecek sınav sayısı tespit edilir.
             * Her yeni sınav oluşturdukça orda kullanılan sorularhavuzdan çıkarılır.
             */

            //İlgili sınav kategorisi çekildi
            $sinavKategori = $this->getSinavKategori($id);

            //soru tablosundan bu sınav kategorisinin soruları çekildi
            $sinavKategorisineBagliSorular = $this->getSinavKategorisineBagliSorular($sinavKategori);

            if(count($sinavKategorisineBagliSorular ) == 0){
                Session::flash('error', 'Yeterli sayıda soru olmadığından hiç sınav oluşturulamadı');
                return redirect()->route('getSinavKategorileri');
            }

            //Bu sınav kategorisinden üretilen sınavlarda kullanılan soru_id leri çekildi, group by her soru id nin bir kere gelmesi için (daha önce aynı soru birden çok sınavda kullanılmış olabilir.)
            $sinavDetaysdaBulunanKategorininSorulari = $this->getSinavDetaysdaBulunanKategorininSorulari($sinavKategori);

            //sinav_kategori_id si belirtilen kategori için daha önce aynı kategorideki sınavlarda hiç kullanılmamış sorular çekildi
            $yeniSinavlarIcinKullanilabilecekSorular = $this->getYeniSinavlarIcinKullanilabilecekSorular($sinavKategorisineBagliSorular, $sinavDetaysdaBulunanKategorininSorulari);

            // burada json formatındaki koleksiyonun alt kırılımından değerler alıp
            // array_count_values da kullanılabilecek şekilde key value yapıldı
            // kullanılabilecek hangi soru kategorisinden kaç soru var tespit edildi
            // değere göre yani soru sayısına göre sıralandı
            $soruKategoriIdKullanilabilecekSoruSayilari = $this->getSoruKategoriIdKullanilabilecekSoruSayilari($yeniSinavlarIcinKullanilabilecekSorular);

            if(count($soruKategoriIdKullanilabilecekSoruSayilari ) == 0){
                Session::flash('error', 'Yeterli sayıda soru olmadığından hiç sınav oluşturulamadı');
                return redirect()->route('getSinavKategorileri');
            }

            //soru adetleri indexleri (key) 0 dan başlayacak şekilde alındı.
            $soruAdetleriKeyless = $this->getSoruAdetleriKeyless($sinavKategori);

            //soru kategori id leri de indexleri (key) 0 dan başlayacak şekilde alındı.
            $soruKategoriIdsKeyless = $this->getSoruKategoriIdsKeyless($sinavKategori);

            //soru kategori id KEY soru adet VALUE olacak şekilde tek dizide birleştirildi.
            $soruAdetleri = $this->getSoruAdetleriCombine($soruKategoriIdsKeyless, $soruAdetleriKeyless);

            //bir veya daha fazla soru kategorisinde yeterli soru yoksa işlemi iptal edildi
            //** Kullanılabilecek soruların soru kategorilerine baktım, eğer $sinavKategori->soru_kategori_ids de bulunan
            //soru kategorilerinin hepsinden soru yoksa burda hata döndürdüm.

            if(false ===  $this->isHerKategoridenSoruVarmi($soruKategoriIdsKeyless, $soruKategoriIdKullanilabilecekSoruSayilari)){
                Session::flash('error', 'Yeterli sayıda soru olmadığından hiç sınav oluşturulamadı');
                return redirect()->route('getSinavKategorileri');
            }

            $donguSayisi = $this->getOlusabilecekSinavSayisi($soruKategoriIdKullanilabilecekSoruSayilari, $soruAdetleri);

            if(($donguSayisi) === 0){
//                    return ['title' => 'Başarısız', 'text'=> 'Yeterli sayıda soru olmadığından hiç sınav oluşturulamadı'];
                Session::flash('error', 'Yeterli sayıda soru olmadığından hiç sınav oluşturulamadı');
                return redirect()->route('getSinavKategorileri');
            }

            //Sınav adını belirlemek için bu kategoriden daha önce oluşturulan sınav sayısı belirlenir.
            //            $buKategoridenDahaOnceOlusanToplamSinavSayisi = $sinavKategori->sinavlar->count();
            $buKategoridenDahaOnceOlusanToplamSinavSayisi = $this->getBuKategoridenDahaOnceOlusanToplamSinavSayisi($sinavKategori);

            for ($i=0; $i<$donguSayisi; $i++){
                $sinav = $this->getSinavGenerate( $sinavKategori, $buKategoridenDahaOnceOlusanToplamSinavSayisi, $i);

                //for içinde olduğu için her sınav oluştuktan sonra sıfırlanıyor ve yeni sınav için hazır hale geliyor.
                $eklenecekSorular = [];
                foreach($soruAdetleri as $key=>$value){
                    // array_keys($yeniSinavlarIcinKullanilabilecekSorular, $key--soru_kategori_id) ile sıradaki soru_kategorisinin soruları yakalanır
                    // array_slice(array_keys($yeniSinavlarIcinKullanilabilecekSorular, $key), 0, $value--soru adedi)) ile yakalanan sorular 0 indexten soru adedi kadar çekilir.
                    // ancak array_slice o değerleri $yeniSinavlarIcinKullanilabilecekSorular dizisinden çıkarmaz bize sadece o parçanın kopyasını verir
                    array_push($eklenecekSorular, array_slice(array_keys($yeniSinavlarIcinKullanilabilecekSorular, $key), 0, $value));
                }

                /*  array_flatten($eklenecekSorular) ile çok boyutlu diziler olmaktan çıkarılır, tek boyutlu hale getirilir.
                    0 = {array} [2]
                    0 = {int} 15936
                    1 = {int} 15937
                    1 = {array} [3]
                    0 = {int} 11120
                    1 = {int} 11121
                    2 = {int} 11122
                    array_except($yeniSinavlarIcinKullanilabilecekSorular, array_flatten($eklenecekSorular)) ile yeni oluşturulan sıradaki sınavın soruları havuzdan çıkarılır.
                */

                //diziyi keyleri koruyarak karıştırır, shuffle keyleri bozuyor. https://stackoverflow.com/a/4102880/2800772
                uksort($yeniSinavlarIcinKullanilabilecekSorular, function() { return rand() > rand(); });
                //kullanılan soruları çıkart
                $yeniSinavlarIcinKullanilabilecekSorular = array_except($yeniSinavlarIcinKullanilabilecekSorular, array_flatten($eklenecekSorular));

                //Model::insert kullanmak için buna uygun dizi oluşturdum.
                //for içinde olduğu için her sınav oluştuktan sonra sıfırlanıyor ve yeni sınav için hazır hale geliyor.
                $insertArray = [];
                foreach (array_flatten($eklenecekSorular) as $key=>$soruId){
                    $insertArray[] = ['sinav_id'=>$sinav->id, 'soru_id'=>$soruId, 'ekleyen'=>1, 'ip'=>request()->ip(), 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()];
                }

                //sınav soruları sinav_detays tablosuna insert ediliyor.
                SinavDetay::insert($insertArray);

            }
            Session::flash('success', $sinavKategori->kategori_adi.' kategorisine toplam '.$donguSayisi.' adet yeni sınav eklendi...');
            return redirect()->route('getSinavKategorileri');
        }catch (\Exception $e)
        {
            Session::flash('error', $e->getMessage().'------'.$e->getLine().'--------------'.$e->getFile());
            return redirect()->route('getSinavKategorileri');
        }
    }

    private function getSinavKategori(int $id) : SinavKategori
    {
        try{
            return SinavKategori::findOrFail($id);
        }catch (\Exception $exception){
            return $exception->getMessage().'--'.$exception->getLine().'---'.$exception->getCode();
        }
    }

    private function getSinavKategorisineBagliSorular(SinavKategori $sinavKategori)
    {
        return Soru::whereIn('soru_kategori_id', explode(',' , $sinavKategori->soru_kategori_ids))->select('id', 'soru_kategori_id')->get();

    }

    private function getSinavDetaysdaBulunanKategorininSorulari(SinavKategori $sinavKategori)
    {
        return SinavDetay::whereIn('sinav_id', array_pluck($sinavKategori->sinavlar, 'id'))->select('soru_id as id')->groupBy('soru_id')->get();
    }

    private function getYeniSinavlarIcinKullanilabilecekSorular(Collection $sinavKategorisineBagliSorular, Collection $sinavDetaysdaBulunanKategorininSorulari) : array
    {
        $yeniSinavlarIcinKullanilabilecekSorular = $sinavKategorisineBagliSorular->diff($sinavDetaysdaBulunanKategorininSorulari)->all();
        return array_combine(array_pluck($yeniSinavlarIcinKullanilabilecekSorular, 'id'), array_pluck($yeniSinavlarIcinKullanilabilecekSorular, 'soru_kategori_id'));
    }

    private function getSoruKategoriIdKullanilabilecekSoruSayilari(array $yeniSinavlarIcinKullanilabilecekSorular) : array
    {
        $yeniSinavlarIcinKullanilabilecekSorular = array_count_values($yeniSinavlarIcinKullanilabilecekSorular);
        asort($yeniSinavlarIcinKullanilabilecekSorular);
        return $yeniSinavlarIcinKullanilabilecekSorular;
    }

    private function getSoruAdetleriKeyless(SinavKategori $sinavKategori) : array
    {
        return explode(',' , $sinavKategori->soru_adetleri);
    }

    private function getSoruKategoriIdsKeyless(SinavKategori $sinavKategori) : array
    {
        return explode(',' , $sinavKategori->soru_kategori_ids);
    }

    private function getSoruAdetleriCombine(array $soruKategoriIdsKeyless, array $soruAdetleriKeyless) : array
    {
        return array_combine($soruKategoriIdsKeyless, $soruAdetleriKeyless);
    }

    private function isHerKategoridenSoruVarmi(array $soruKategoriIdsKeyless, array $soruKategoriIdKullanilabilecekSoruSayilari) : bool
    {
        if(count(array_diff($soruKategoriIdsKeyless, array_keys($soruKategoriIdKullanilabilecekSoruSayilari))) > 0 ){
            return false;
        }
        return true;
    }

    private function getOlusabilecekSinavSayisi(array $soruKategoriIdKullanilabilecekSoruSayilari, array $soruAdetleri) : int
    {
        $olusabilecekSinavSayilari =  [];

        $flag = 1;
        foreach ($soruKategoriIdKullanilabilecekSoruSayilari as $key => $value){

            //Eğer soru adeti gerekli olanla eşit yada fazla ise
            //Örn: 657 S.Kanun 7 soru var, gerekli olan 3
            // 7/3 2,2 yapar, floor bunu min tamsayıya çevirecek 2 sınav çıkar burdan diyeceğiz.

            if($value >= $soruAdetleri[$key] ){
                $olusabilecekSinavSayilari[$key] = floor($value/$soruAdetleri[$key]);
            }else{
                //değilse zaten soru yetersiz sıradaki soru kategorisinden
                // o nedenle döngüden çıkar, aşağıdaki if e takılır ve hata döndürecektir.
                $flag = 0; break;
            }
        }

        if ($flag === 0){
            return 0;
        }else{
            //döngü sayısı aynı zamanda oluşacak sınav sayısıdır.
            return min($olusabilecekSinavSayilari);
        }
    }

    private function getBuKategoridenDahaOnceOlusanToplamSinavSayisi (SinavKategori $sinavKategori) : int
    {
        return $sinavKategori->sinavlar->count();
    }

    private function getSinavGenerate(SinavKategori $sinavKategori, int $buKategoridenDahaOnceOlusanToplamSinavSayisi, int $i) : Sinav
    {
        $sinav = new Sinav();
        $ad = $sinavKategori->kategori_adi.' Deneme Sınavı '. ($buKategoridenDahaOnceOlusanToplamSinavSayisi+$i+1);
        $sinav->adi = $ad;
        $sinav->link = str_slug($ad);
        $sinav->kategori_id = $sinavKategori->id;
        $sinav->durum = 1;
        $sinav->ekleyen = 1;
        $sinav->ip = \request()->ip();
        $sinav->saveOrFail();
        return $sinav;
    }

    public function getKatSinavEdit(Int $id) : View
    {
        $soruKategorileri = SoruKategori::all();
        $sinavKategori = SinavKategori::find($id);

        $soruAdetleri = explode(',', $sinavKategori->soru_adetleri);
        $soruKategoriIds = explode(',', $sinavKategori->soru_kategori_ids);

        $soruKategoriAndSoruAdedi = array_combine($soruKategoriIds, $soruAdetleri);

        $selectedSoruKategorileri = $soruKategorileri->whereIn('id', collect($soruKategoriIds));

        return view('admin.sinav.sinav-kategori-edit-page', compact('sinavKategori','soruKategorileri', 'soruKategoriAndSoruAdedi', 'selectedSoruKategorileri', 'soruAdetleri'));
    }

    public function ajaxSinavKategoriPatternUpdate(Request $request) : array
    {
        SinavKategori::find($request->sinav_kategori_id)->update([
            'kategori_adi' => $request->sinav_kategori_adi,
            'sinav_format' => $request->sinav_format,
            'soru_kategori_ids' => implode(',', $request->soru_kategori_ids),
            'soru_adetleri' => implode(',', $request->soru_adetleri)
        ]);
        return ['title'=> 'Başarılı', 'text'=>'Sınav Kategorisi başarıyla güncellendi'];
    }

    public function getKatSinavlar(Int $id) : View
    {
        $sinavlar = Sinav::where('kategori_id', $id)->where('durum', 1)->orderBy('adi', 'ASC')->paginate(50);
        Session::put('breadcrumb', SinavKategori::find($id)->kategori_adi.' Sınavları');
        return view('admin.sinav.sinav-kategori-sinavlari',compact('sinavlar'));
    }

    public function getSinavSorulari(Int $id) :View
    {
        $soruIds = SinavDetay::where('sinav_id',$id)->get();
        $sorular = Soru::whereIn('id', $soruIds->pluck('soru_id'))->where('durum', 1)->paginate(50);
        $soruKategorileri = SoruKategori::all();
        Session::put('breadcrumb', Sinav::find($id)->adi.' Soruları');
        return view('admin.sinav.sinav-sorulari', compact('sorular', 'soruKategorileri'));
    }

    public function getSinavTip() : View
    {
        $sinavTips = SinavTip::where('durum', 1)->orderBy('sinav_tip', 'ASC')->paginate(50);
        Session::put('breadcrumb', 'Sınav Tipleri');
        return view('admin.sinav.sinav-tipleri', compact('sinavTips'));
    }

    public function getSinavTipPage(Int $id) : View
    {
        $sinavTipi = SinavTip::find($id);

        $soruKatIds = explode(',', $sinavTipi->soru_kategori_ids);
        $soruKategorileri   = SoruKategori::all();
        $selectedSoruKats    = $soruKategorileri->whereIn('id', $soruKatIds);

        $sinavKatIds = explode(',', $sinavTipi->sinav_kategori_ids);
        $sinavKategorileri  = SinavKategori::all();
        $selectedSinavKats  = $sinavKategorileri->whereIn('id', $sinavKatIds);

        Session::put('breadcrumb', $sinavTipi->sinav_tip.' İsimli Sınav Tipi');
        return view('admin.sinav.sinav-tip-page', compact('sinavTipi', 'soruKategorileri', 'selectedSoruKats', 'sinavKategorileri', 'selectedSinavKats'));
    }

    public function setSinavTipEkle(Request $request)
    {
        $insert = new SinavTip();
        $insert->sinav_tip = $request->sinav_tip;
        $insert->link = str_slug($request->sinav_tip);
        $insert->logo = '-';
        $insert->logo2 = '-';
        $insert->soru_kategori_ids  = implode(',', $request->soru_kategori_ids);
        $insert->sinav_kategori_ids = implode(',', $request->sinav_kategori_ids);
        $insert->durum = 1;
        $insert->ekleyen = Auth::id();
        $insert->ip = $request->ip();
        $insert->save();

        return redirect()->route('getSinavTip');
    }

    public function dropzoneTip(Request $request, Int $id) : String
    {
        $photo = $request->file('file');
        $imgSavePath = public_path('/resim/sinav/sinav_tip') . '/' . $photo->getClientOriginalName();
        $imgAssetPath = '/resim/sinav/sinav_tip/'.$photo->getClientOriginalName();

        Image::make($photo)
            ->resize(200, 200)
            ->save($imgSavePath);

        SinavTip::find($id)->update([
            'logo' => $imgAssetPath
        ]);

        return asset($imgAssetPath);
    }

    public function getSinavTipEkle() : View
    {
        $soruKategorileri = SoruKategori::all();
        $sinavKategorileri = SinavKategori::all();
        Session::put('breadcrumb', 'Yeni Sınav Tipi Ekleme Sayfası');
        return view('admin.sinav.sinav-tip-ekle', compact('soruKategorileri', 'sinavKategorileri'));
    }

    public function ajaxSinavTipSoruKategoriUpdate(Request $request) : array
    {
        SinavTip::find($request->sinav_tip_id)->update([
            'soru_kategori_ids' => implode(',', $request->soru_kategori_ids)
        ]);

        return ['title' => 'Başarılı', 'text'=> 'Soru Kategorileri başarıyla güncellendi.. :)'];
    }

    public function ajaxSinavTipSinavKategoriUpdate(Request $request) : array
    {
        SinavTip::find($request->sinav_tip_id)->update([
            'sinav_kategori_ids' => implode(',', $request->sinav_kategori_ids)
        ]);

        return ['title' => 'Başarılı', 'text'=> 'Sınav Kategorileri başarıyla güncellendi.. :)'];
    }

    public function ajaxSinavTipNameUpdate(Request $request) : array
    {
        SinavTip::find($request->sinav_tip_id)->update([
            'sinav_tip' => $request->name
        ]);

        return ['title' => 'Başarılı', 'text'=> 'Sınav Tipinin adı başarıyla güncellendi.. :)'];
    }

    public function getOnaysizSinavlar() : View
    {
        $sinavlar = Sinav::where('durum',0)->orderBy('id', 'DESC')->paginate(50);
        Session::put('breadcrumb', 'Onaysız Sınavlar');
        return view('admin.sinav.sinav-onaysiz-sinavlar', compact('sinavlar'));
    }

    public function getKategorisizSinavlar() : View
    {
        $sinavlar = Sinav::where('kategori_id',0)->orderBy('id', 'DESC')->paginate(50);
        Session::put('breadcrumb', 'Kategorisiz Sınavlar');
        return view('admin.sinav.sinav-kategorisiz-sinavlar', compact('sinavlar'));
    }

    //////////////ADMIN METHODS///////////////////

    public function getEnCokCozulenSinavlar()
    {
        return Cache::remember('enCokCozulenSinavlar', 60, function (){
            return DB::select('SELECT 
                        s.adi, s.link
                    FROM (select * from sinavs where durum = 1) s
                        
                            right JOIN
                        (SELECT 
                            sinav_id, COUNT(sinav_id) cozum
                        FROM
                            (SELECT 
                            sinav_id
                        FROM
                            uyeler_detays
                        WHERE
                            durum = 1
                            and sinav_id > 0
                        ORDER BY id DESC
                        LIMIT 2000) d
                        GROUP BY sinav_id
                        ORDER BY cozum DESC
                        ) d ON d.sinav_id = s.id
                        LIMIT 5');
        });
    }

    //TODO Sınav Ekleme Modülünde Cache i temizlemeyi unutma..!
    public function getYeniEklenenSinavlar()
    {
        return Cache::rememberForever('yeniEklenenSinavlar', function (){
            return DB::select('SELECT adi, link
                                  FROM sinavs
                                  WHERE durum = 1
                                ORDER BY id DESC
                                 LIMIT 5');
        });
    }

    //TODO Sınav Ekleme Modülünde Cache i temizlemeyi unutma..!
    public function getSinavSayisi()
    {
        return Cache::rememberForever('sinavSayisi', function (){
            return collect(DB::select('select count(id) sayi from sinavs where durum=1'))->first();
        });
    }

    //TODO Yeni sinav tipi eklenince cache i temizle..!
    //TODO Yeni sinav eklenince cache i temizle..!
    //TODO Yeni sinav kategorisi eklenince cache i temizle..!
    public function sinavEkrani() : View
    {
        $sinavTipleri = Cache::rememberForever('sinavTipleri', function (){
            return DB::select('select id, sinav_tip, link, sinav_say(id) as sinav_sayisi, logo from sinav_tips');
        });

        return view('front.sinav-ekrani', compact('sinavTipleri'));
    }

    public function sinavEkraniDatalar($url)
    {
        //TODO $url parametresini temizlemeden sakın sorguya gönderme..!
        $sinavlar = Cache::rememberForever('sinavEkraniSinavlariListele'.$url, function () use($url){
            return collect(DB::select('CALL PSINAVLAR(0, "'.$url.'" , @sinavlar)' ));
        });

        //TODO sinav kategorileri eklerken falan cache i temizlemeyi unutma..!
        $sinavKategorileri = Cache::rememberForever('sinavEkraniSinavKategorileriListele'.$url, function () use($url){
            return collect(DB::select('CALL PSINAV_KATEGORILERI(0, "'.$url.'" , @sinavlar)' ));
        });

        $sinavTip = Cache::rememberForever('sinavTipAdi'.$url, function () use($url){
            return collect(DB::select('select sinav_tip, logo from sinav_tips where link = :url', ['url'=>$url]))->first();
        });

        return ['sinavTip'=>$sinavTip, 'sinavKategorileri'=> $sinavKategorileri, 'sinavlar'=>$sinavlar];
    }

    //TODO Yeni sınav eklenince cache i temizle..!
    public function sinavEkraniSinavlariListele(String $url) : View
    {
        $datalar = self::sinavEkraniDatalar($url);
        $sinavlar = $datalar['sinavlar'];
        $sinavKategorileri = $datalar['sinavKategorileri'];
        $sinavTip = $datalar['sinavTip'];

        return view('front.sinav-ekrani-sinavlar', compact('sinavlar', 'sinavKategorileri', 'sinavTip'));
    }

    //////////////SINAV SAYFASI///////////////////
    public function getSorular(Int $page=1, Int $userId=0, String $url) : array
    {
        $limit = 10;
        $baslangic = $page*$limit-9;

        return Cache::remember('sinav-sorulari-'.$url.'-'.$page, 45, function () use($url, $baslangic, $limit, $page)
        {
            $sinav = Sinav::where('link', $url)->firstOrFail();

            $soruIds = DB::table('sinav_detays')
                            ->join('sorus', 'sinav_detays.soru_id', '=', 'sorus.id')
                            ->where('sinav_id', $sinav->id)
                            ->where('sorus.durum', 1)
                            ->select('sinav_detays.*')
                            ->get();

            $ids = $soruIds->pluck('soru_id')->toArray();

            $yinelenenSorular = self::getYinelenenSoruIds($ids);

            if ($yinelenenSorular !== false)
            {
                self::setDeleteSinavDetayYinelenenSoruIds($sinav, $yinelenenSorular);
            }

            $ids = array_unique($ids);

            $sayfaSorularIds = array_slice($ids, $baslangic-1, $limit);

            $sorular = Soru::whereIn('id', collect($sayfaSorularIds))->inRandomOrder()->get();

            foreach ($sorular as $soru)
            {
                $soru->icerik = trim(html_entity_decode($soru->soru));
                $soru->sira = $baslangic;
                $soru->puan = self::getSoruPuan(count($ids), $soru->dogru, $soru->yanlis);
                $baslangic++;
            }

            $cevaplar = Cevap::whereIn('soru_id', $sorular->pluck('id'))->inRandomOrder()->get();

            return ['sorular'=>$sorular, 'cevaplar'=>$cevaplar,'sayfa'=>$page];
        });
    }

    private function getSoruPuan(Int $soruSayisi=0, Int $dogruAdedi=0, Int $yanlisAdedi=0) : float
    {

      if($soruSayisi<=0)
      {
          return 0;
      }


      if($dogruAdedi <= 0)
          $dogruAdedi=0;

      if($yanlisAdedi <= 0)
          $yanlisAdedi=0;

      $barem_tolerans = 5;
      $ort_soru_puan = 100 / $soruSayisi;
      $soru_puan_baremi = $barem_tolerans / 100 * $ort_soru_puan;
      $fark = $dogruAdedi - $yanlisAdedi;

      IF ($fark < -70)
        $puan        = $ort_soru_puan+$soru_puan_baremi;
      elseif( $fark < -20 )
        $puan        = $ort_soru_puan+($soru_puan_baremi/2);
      elseif( $fark > 70 )
        $puan       = $ort_soru_puan-$soru_puan_baremi;
      elseif( $fark > 20 )
        $puan       = $ort_soru_puan-($soru_puan_baremi/2);
      else
        $puan = $ort_soru_puan;

      return $puan;
}

    private function getYinelenenSoruIds(Array $ids)
    {
        $yinelenenSoruIdsInSinavDetay = array_count_values($ids);

        $yinelenenler = [];
        foreach ($yinelenenSoruIdsInSinavDetay as $k => $v)
        {
            $v>1 ? $yinelenenler[] = $k : null;
        }

        return count($yinelenenler) === 0 ? false : $yinelenenler;
    }

    private function setSinavDetayEkle(Int $sinavId, Int $soruId) : bool
    {
        $sinavDetay = new SinavDetay();
        $sinavDetay->sinav_id = $sinavId;
        $sinavDetay->soru_id = $soruId;
        $sinavDetay->ekleyen = 1;
        $sinavDetay->ip = request()->ip();
        $sinavDetay->saveOrFail();
        return true;
    }

    private function setDeleteSinavDetayYinelenenSoruIds(Sinav $sinav, Array $ids) : bool
    {
        foreach ($ids as $soruId)
        {   //Eğer birden fazla yinelenen soru_id varsa (2 veya 3 kere de olabilir) hepsini silip aynı soruyu bir kere ekliyorum.
            SinavDetay::where('sinav_id', $sinav->id)->where('soru_id', $soruId)->delete();
            self::setSinavDetayEkle($sinav->id, $soruId);
        }
        Cache::forget('sinavGeneralInfo'.$sinav->link);
        Log::info('Sınav Detay Tablosu', ['Yinelenen Soru ID' => $sinav->id.' nolu sınavın sinav_detays tablosundan bulunan '.$soruId.' nolu sorusu silindi..!']);
        return true;
    }

    public function getSinavView(String $url)
    {
        //Kullanıcının sayfayı her yenilemesinde farklı bir token oluşacak ve
        // bu sayede sınav sonuçları sürekli update edilmeyip
        // aynı sinav_id de farklı sınavlar çözülmüş olacak
        //sinav kategorisini bul
        //sınav soru sayısını bul
        //sınav adını çek
        //Kaç kez çözüldüğünü çek
        //Önceki sonraki sınav ad ve linkini çek
        //Sınav birincilerini çek
        //Sınava yapılan yorumları çek

        $sinav = Sinav::where('link', $url)->firstOrFail();
        $arr = explode(' ', $sinav->adi);
        shuffle($arr);
        $keywords = implode(',', $arr);

        return view('front.sinav.index', compact('url', 'sinav', 'keywords'));
    }

    public function getSinavGeneralInfos(String $url)
    {
        return Cache::remember('sinavGeneralInfo'.$url, 120, function () use($url){
            return collect(DB::select('SELECT ssd.*, count(ud.sinav_id) cozum_sayisi, replace(round(avg(ud.puan), 2), \'.\', \',\') ortalama_puan
                                        FROM   (SELECT s.id, s.adi, s.link, count(sd.soru_id) soru_sayisi
                                                FROM sinavs s RIGHT JOIN (select d.* from sinav_detays d, sorus sr where d.soru_id=sr.id and sr.durum=1) sd ON s.id = sd.sinav_id
                                                WHERE  s.link = ?) ssd
                                               LEFT JOIN uyeler_detays ud ON ud.sinav_id = ssd.id', [$url]));
        });
    }

    public function getKacKezCozdun()
    {
        return collect(DB::select('select count(*) kac_kez_cozdun from uyeler_detays where user_id = ? 
                                         and sinav_id=(select id from sinavs where link=?)',
            [request()->userId, request()->url]));
    }

    //TODO yorum ile ilgili crud olursa cache temizle..!
    public function getSinavComments(String $url)
    {
        return Cache::rememberForever('yorumlar'.$url, function () use ($url) {
            return DB::select('SELECT 
                                            u.id,
                                            u.kullanici,
                                            u.foto,
                                            u.fb_id,
                                            y.yorum,
                                            y.kullanici AS kAdi,
                                            y.created_at
                                        FROM
                                            (SELECT 
                                                id, kullanici, foto, fb_id
                                            FROM
                                                users UNION SELECT 0, \'Ziyaretçi\', \'images/user.png\', 0) u,
                                            (SELECT 
                                                *
                                            FROM
                                                yorums
                                            WHERE
                                                tur = 2 AND onay = 1) y,
                                            sinavs s
                                        WHERE
                                            u.id = y.user_id AND s.id = y.icerik
                                                AND s.link = ?
                                        ORDER BY y.id DESC', [$url]);
        });
    }

    public function getSinavSuccessfulUsers(String $url) : array
    {
        return Cache::remember('basariListesi'.$url, 120, function () use ($url) {
            return DB::select('SELECT d.user_id as id,
                                           u.kullanici,
                                           u.foto,
                                           MAX(d.puan) AS sinav_puan,
                                           d.sure,
                                           u.fb_id
                                      FROM uyeler_detays d
                                           LEFT JOIN sinavs s ON d.sinav_id = s.id
                                           LEFT JOIN users u ON d.user_id = u.id
                                     WHERE     d.durum = 1
                                           AND u.id > 1
                                           AND u.durum = 1
                                           AND s.link = ?
                                    GROUP BY d.user_id
                                    ORDER BY sinav_puan DESC, d.sure ASC, d.created_at DESC
                                     LIMIT 5 ', [$url]);
                                            });
    }

    public function getCevapCheck(Int $id)
    {
        $cevap =  collect(DB::select('select soru_id, id from cevaps where soru_id = (select soru_id from cevaps where id = ?) and dogru=1', [ $id ]))->first();
        if($cevap === null){
            //TODO log kaydı ekle
            return false;
        }

        $res['cevap_id'] = $cevap->id;
        $res['soru_id'] = $cevap->soru_id;

        return $res;
    }

    public function setCevapInsert() : bool
    {
        //İlk kez çözmüşse sınavı insert edilecek
        $ins = new UyelerDetay();
        $ins->user_id = request()->user_id;
        $ins->sinav_id = request()->sinav_id;
        $ins->puan = round(request()->puan, 2);
        $ins->dogru = request()->dogru;
        $ins->yanlis = request()->yanlis;
        $ins->bos = request()->bos;
        $ins->sure = request()->sure;
        $ins->ip = request()->ip();
        $ins->token = request()->token;
        $ins->cevap_soru_ids = json_encode(request()->cevap_soru_ids); //TODO request()->cevap_soru_ids;
        $ins->durum = 1;
        $ins->saveOrFail();
        return true;
    }

    public function setCevapUpdate(Int $id) : bool
    {
        //son 1 gün içinde kullanıcı bu sınavı çözmüşse mevcut kayıt update edilir.
        UyelerDetay::where('id', $id)->update([
            'user_id' => request()->user_id,
            'puan' => round(request()->puan, 2),
            'dogru' => request()->dogru,
            'yanlis' => request()->yanlis,
            'bos' => request()->bos,
            'sure' => request()->sure,
            'ip' => request()->ip(),
            'durum'=> 1,
            'cevap_soru_ids' => json_encode(request()->cevap_soru_ids) //TODO request()->cevap_soru_ids
        ]);
        return true;
    }

    public function setSoruDogruYanlisSayisi(Int $soru_id, Int $cevap_dogrumu) : bool
    {
        if($cevap_dogrumu == 1)
        {
            Soru::where('id', $soru_id)->increment('dogru');
        }else{
            Soru::where('id', $soru_id)->increment('yanlis');
        }
        return true;
    }

    public function setCevapSave() : String
    {
        $res = collect(DB::select('SELECT *
                          FROM uyeler_detays
                         WHERE token = ?
                               AND sinav_id = ?
                               AND durum = 1
                        ', [request()->token, request()->sinav_id]))->first();

        if($res !== null){
            $this->setCevapUpdate($res->id);
        }else{
            $this->setCevapInsert();
        }
        self::setSoruDogruYanlisSayisi(request()->soru_id, request()->cevap_dogrumu );
        return 'ok';
    }

    //\\\\\\\\\\\\\SINAV SAYFASI//////////////\\

    //////////////COMMENT METHODS///////////////////
    //TODO Admin yorum gönderirse doğrudan kaydet ve göster
    public function setYorumGonder()
    {   $userId = request()->user_id ?? 0;

        if($userId > 0){

            $res = $this->yorumSingleValidate(request()->yorum);

            if ($res !== true)
            {
                return $res;
            }

            $this->yorumInsert($userId);
            return Response::json(['ok' => 'Yorumunuzu başarıyla aldık, yönetici onayından sonra yayınlanacaktır.']);
        }

        $res = $this->yorumAllValidate();

        if ($res !== true)
        {
            return $res;
        }

        $this->yorumInsert($userId);

        return Response::json(['ok' => 'Yorumunuzu başarıyla aldık, yönetici onayından sonra yayınlanacaktır.']);
    }

    public function emailValidate($email)
    {
        $kurallar = array(
            'email' => 'required|email|max:80',
        );

        $hataMesajlari = array(
            'email.required' => 'E-posta adresinizi belirtmelisiniz..!',
            'email.email' => 'Lütfen geçerli bir E-posta adresi yazınız..!',
            'email.max' => 'En fazla 100 karakter yazabilirsiniz..!'
        );

        $denetim = Validator::make(['email'=> $email], $kurallar, $hataMesajlari);

        if ($denetim->fails())
        {
            return Response::json(['hata' => $denetim->errors()->first()]);
        }

        return true;
    }

    public function yorumSingleValidate($yorum)
    {
        $kurallar = array(
            'yorum' => 'required|string',
        );

        $hataMesajlari = array(
            'yorum.required' =>'Yorum yazmadınız.',
            'yorum.string' => 'Rakam, harf ve noktalama işaretleri dışındaki karakterleri kullanmamalısınız.'
        );

        $denetim = Validator::make(['yorum'=> $yorum], $kurallar, $hataMesajlari);

        if ($denetim->fails())
        {
            return Response::json(['hata' => $denetim->errors()->first()]);
        }

        return true;
    }

    public function yorumAllValidate()
    {
        $kurallar = array(
            'kullanici' => 'required|string|max:80',
            'email' => 'required|email|max:80',
            'yorum' => 'required|string',
        );

        $hataMesajlari = array(
            'email.required' => 'E-posta adresinizi belirtmelisiniz.',
            'email.email' => 'Lütfen geçerli bir E-posta adresi yazınız..!',
            'email.max' => 'En fazla 100 karakter yazabilirsiniz.',
            'kullanici.required' => 'İsminizi boş bırakmayınız.',
            'kullanici.string' => 'Rakam, harf ve noktalama işaretleri dışındaki karakterleri kullanmamalısınız.',
            'kullanici.max' =>'En fazla 80 karakter yazabilirsiniz.',
            'yorum.required' =>'Yorum yazmadınız.',
            'yorum.string' => 'Rakam, harf ve noktalama işaretleri dışındaki karakterleri kullanmamalısınız.'
        );

        $denetim = Validator::make(['kullanici' => request()->kullanici, 'email'=> request()->email, 'yorum' =>request()->yorum], $kurallar, $hataMesajlari);

        if ($denetim->fails())
        {
            return Response::json(['hata' => $denetim->errors()->first()]);
        }

        return true;
    }

    private function yorumInsert($userId)
    {
        $e = new Yorum();
        $e->yorum = request()->yorum;
        $e->tur = request()->tur;
        $e->icerik = request()->icerik;
        $e->user_id = $userId;
        $e->email = request()->email;
        $e->kullanici = request()->kullanici;
        $e->ip = request()->ip();
        $e->onay = request()->onay;
        $e->save();
        return true;
    }

    public function getYorumlar($sinav_id)
    {
        return DB::select('SELECT y.id,
                                        case 
                                          when  y.user_id > 0 then (select kullanici from users where id=y.user_id)
                                          else y.kullanici 
                                        end kullanici_adi,
                                        case 
                                          when  y.user_id > 0 then (select case when foto = \'resim/user.png\' then \'/resim/uyeler/user.png\' else foto end foto from users where id=y.user_id)
                                          else \'/resim/uyeler/user.png\' 
                                        end user_foto,
                                       FTURKCE(y.yorum) yorum,
                                       s.link,
                                       y.user_id,
                                       y.created_at as tarih
                                  FROM yorums y
                                  left join sinavs s on s.id = y.icerik
                                 WHERE y.tur = 2 AND y.icerik = ? AND y.onay = 1
                                 order by y.created_at desc 
                                            ', [$sinav_id]);
    }
    //\\\\\\\\\\\\\COMMENT METHODS//////////////\\
}