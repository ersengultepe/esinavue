<footer id="footer">
    <div v-if="preloadebulten === true" id="preloadx">
        <Socket style="margin-top:30%;margin-left:50%;margin-right:50%;z-index: 9999;position: fixed;">
        </Socket>
    </div>

    <div v-if="preloademesaj === true" id="preloadx">
        <Plane style="margin-top:30%;margin-left:50%;margin-right:50%;z-index: 9999;position: fixed;">
        </Plane>
    </div>
    <div class="container">
        <div class="row">
            <div class="footer-ribbon">
                <span>EsinavSalonu.Com</span>
            </div>
            <div class="col-lg-3">
                <div class="newsletter">
                    <h4>Ebülten</h4>
                    <p>Sürekli genişleyen ve büyüyen sınav yelpazemizden uzak kalmayın. E-postanızı girin ve bültenimize abone olun.</p>

                    <div v-if="message !== null">
                        <ebulten-error v-if="message.hata !== undefined" :message="message"></ebulten-error>
                        <ebulten-success v-if="message.ok !== undefined" :message="message"></ebulten-success>
                    </div>

                    <div class="input-group">
                        <input class="form-control form-control-sm" :keyup="ebultenChange" v-model="ebultenemail" placeholder="eposta adresiniz" name="email" id="ebulten" type="email" required >
                        <span class="input-group-append">
                            <button class="btn btn-light" @click="ebultenKayit()" type="button" style="z-index: 1 !important">Abone Ol!</button>
                        </span>
                    </div>

                </div>
            </div>
            <div class="col-lg-4">
                <div class="col-lg-12">

                    <h4>Mesaj Gönder</h4>
                    <div class="form-group row">

                        <div v-if="sendmesaj !== null " class="col-lg-12">
                            <mesaj-error v-if="sendmesaj.hata !== undefined" :sendmesaj="sendmesaj"></mesaj-error>
                            <mesaj-success v-if="sendmesaj.ok !== undefined" :sendmesaj="sendmesaj"></mesaj-success>
                        </div>

                        <div class="col-lg-12">
                            <span id="user-id" style="display: none"><?php echo e(Auth::id()); ?></span>
                            <input class="form-control" name="ad_soyad" v-model="ad_soyad" placeholder="Adınız & Soyadınız" id="" type="text" required  style="margin-bottom: 2%">
                            <input class="form-control" name="email" v-model="email" placeholder="Eposta Adresiniz" id="" type="email" required  style="margin-bottom: 2%">
                            <textarea class="form-control" name="mesaj" v-model="mesaj" placeholder="Görüş veya Öneriniz" rows="2" id="" required   style="height: 80%;"></textarea>
                            <button class="btn btn-light" @click="mesajGonder()" type="button">Gönder</button>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-5">
                <h4>Son Yorumlar</h4>
                <div class="contact-details">
                    
                        <ul class="comments" >
                            <?php $__currentLoopData = $sonYorumlar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $yorum): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <blockquote class="blockquote-reverse">
                                    <p><?php echo str_limit($yorum->yorum, 180); ?></p>
                                    <footer><a href="<?php echo e($yorum->user_id >0 ? userSlug($yorum->kullanici_adi, $yorum->user_id) : '#'); ?>"><?php echo e($yorum->kullanici_adi); ?></a> tarafından <cite title="Source Title"><?php echo e($yorum->tarih); ?></cite> tarihinde yazıldı.</footer>
                                </blockquote>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    
                </div>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-1">
                    <a href="<?php echo e(route('anasayfa')); ?>" class="logo">
                        <img alt="esinavsalonu.com web site logo" class="img-fluid" src="<?php echo e(asset('img/assets/logo.png')); ?>" >
                    </a>
                </div>
                <div class="col-lg-7">
                    <p>© 2014 - <?php echo e(date('Y')); ?>. Tüm hakları saklıdır.</p>
                </div>
                <div class="col-lg-4">
                    <nav id="sub-menu">
                        <ul>
                            <li><a href="<?php echo e(route('sss')); ?>">Sık Sorulan Sorular</a></li>
                            <li><a href="#">Site Haritası</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
