<?php
namespace App\Providers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
class GoogleDriveServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('google', function($app, $config) {
            $client = new \Google_Client();
            $client->setClientId('717942501396-7e2dlfavcu6homgek823p3l9nq9jkvbs.apps.googleusercontent.com');
            $client->setClientSecret('-fk86OHe7sde74X0MtMmVXo9');
            $client->refreshToken('1/EdhA6VcVUDeFIVqNiyBETsXkagMBfIyUoHVVe2SDDwU');
            $service = new \Google_Service_Drive($client);
            $adapter = new \Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter($service, '1hvf7kUgt-O0Fp7ESDO7SGRbGkgb8ZxLO');
            return new \League\Flysystem\Filesystem($adapter);
        });
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
