@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
@stop

@section('content')
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                    <a href="{{ url()->previous() }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                        Geri Dön </a>
                </button>
                <span>{{ str_limit(ucWordsTr(mb_strtolower(Session::get('breadcrumb'), 'UTF-8'))) }} </span>
                <small>Toplam {{ $sorular->total() }} Soru</small>

            </h2>
        </header>

        @include('admin.soru.soru-cevaplar')

    </section>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.modals.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>

    <script>
        $('.soru-update').click(function () {
            let buton_id = this.id;
            soru_id = buton_id.replace('bt_', '');
            updateSoru = CKEDITOR.instances['tx_'+soru_id].getData();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/soru/update',
                data:{
                    'id': soru_id,
                    'soru': updateSoru
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Soru güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.soru-kategori-update').change(function () {
            let select_id = this.id;
            soru_id = select_id.replace('sc_', '');
            soru_kategori_id = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/soru/kategori-update',
                data:{
                    'soru_id': soru_id,
                    'soru_kategori_id': soru_kategori_id,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Soru kategorisi güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.soru-onay-update').change(function () {
            let chk_id = this.id;
            let soru_id = chk_id.replace('sw_', '');
            let durum = $('#swr_'+soru_id+' .ios-switch').hasClass('on') ? 1 : 0;

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/soru/durum-update',
                data:{
                    'soru_id' : soru_id,
                    'durum' : durum
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Cevap güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });


        });
    </script>
@stop

