<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKapsamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kapsam', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dsc')->comment('Kamu Personel Seçme Sınavı gibi');
            $table->string('slug')->comment('kamu-personel-secme-sinavi gibi')->unique();
            $table->string('tag')->comment('KPSS, YDS, YKS, ÖABT gibi')->unique();
            $table->ipAddress('ip');
            $table->unsignedInteger('user_id');
            $table->enum('durum', [1,0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kapsam');
    }
}
