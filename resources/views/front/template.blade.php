<!DOCTYPE html>
<html lang="tr_TR">
<head>
    @yield('title')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    @yield('meta')
    <script src="{{ asset('js/app.js') }}"></script>
    <meta name="author" content="esinavsalonu">
    <meta name="env" content="{{env('APP_ENV', 'production')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('/img/favicon.png') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <script async src="{{ asset('js/toastr.min.js') }}"></script>
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <!-- Web Fonts  -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light&subset=latin-ext" rel="stylesheet" type="text/css">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2211050103220921",
            enable_page_level_ads: true
        });
    </script>
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/skins/default.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/font-awesome/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/animate/animate.min.css') }}">	
	<script src="//cdn2.admatic.com.tr/showad/showad.js" async></script>
    @yield('css')
    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('/css/skins/skin-corporate-5.css') }}">
    <!-- Head Libs -->
    <script async src="{{ asset('/vendor/modernizr/modernizr.min.js') }}"></script>
    <script>
        // Tooltips
        $(function () {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        });
    </script>
</head>

<body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay
      data-plugin-options="{'hideDelay': 500}">
<div class="loading-overlay">
    <div class="bounce-loader">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div id="sinav">
    @yield('content')
</div>

@include('front.part.footer')

@yield('js')
<script async src="{{ asset('js/front/front.js') }}"></script>

<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true
    };

   @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>

<!-- Admatic interstitial 800x600 Ad Code START -->
<ins data-publisher="adm-pub-190101685456" data-ad-type="interstitial" class="adm-ads-area" data-ad-network="182901247802" data-ad-sid="209" data-ad-width="800" data-ad-height="600"></ins>
<!-- Admatic interstitial 800x600 Ad Code END -->


<!-- Admatic Scroll 300x250 Ad Code START -->
{{--<ins data-publisher="adm-pub-190101685456" data-ad-type="Scroll" class="adm-ads-area" data-ad-network="182901247802" data-ad-sid="304" data-ad-width="300" data-ad-height="250"></ins>--}}
<!-- Admatic Scroll 300x250 Ad Code END -->


<!-- Admatic inpage x Ad Code START -->
{{--<ins data-publisher="adm-pub-190101685456" data-ad-type="inpage" class="adm-ads-area" data-ad-network="182901247802" data-ad-sid="600"></ins>--}}
<!-- Admatic inpage x Ad Code END -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40136905-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-40136905-8');
</script>

</body>
</html>
