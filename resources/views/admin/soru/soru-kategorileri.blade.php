@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Kayıtlı Tüm Soru Kategorilerimiz</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Soru Kategori Adı</th>
                            <th>Eklenme Tarihi</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($soruKategorileri as $kat)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('getKategoriWithSorular', ['id'=>$kat->id]) }}">
                                        {{ str_limit($kat->kategori_adi) }}
                                    </a>
                                </td>
                                <td>{{ $kat->created_at }}</td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{ $soruKategorileri->links() }}
                </div>
            </div>
        </section>
    </div>
    @stop