<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoruHatalari extends Model
{
    protected $table = 'soru_hatalaris';

    protected $fillable = [
        'soru_id',
        'user_id',
        'sinav_id',
        'hata',
        'durum',
        'hatali_mi',
        'ip'
    ];
}
