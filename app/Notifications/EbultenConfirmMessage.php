<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EbultenConfirmMessage extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $confirmLink = env('APP_URL').'/confirm_ebulten/'.str_random(6).'=='.str_random(4).base64_encode($notifiable->email).str_random(10);
        return (new MailMessage)
            ->greeting('Merhaba')
            ->subject('EsinavSalonu.com | Ebülten Aboneliğiniz Hk..!')
            ->line("Bu epostayı Esinavsalonu.com sitesinin e-bültenlerine abone olmak istediğiniz için almaktasınız.")
            ->line("Bundan böyle sürekli güncellenen sınavlarımızdan haberdar olacaksınız.")
            ->line("Aboneliğinizi aktifleştirmek için aşağıdaki butona tıklayınız.")
            ->action('Onayla', url($confirmLink))
            ->line('Tüm soru ve önerileriniz için [Sık Sorulan Sorular](http://www.esinavsalonu.com/sss) sayfamızdan destek alabilirsiniz.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
