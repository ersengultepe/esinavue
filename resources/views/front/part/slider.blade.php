<div class="slider-container rev_slider_wrapper">
    <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 500, 'responsiveLevels': [4096,1200,992,500]}">
        <ul>
            <li data-transition="fade">

                <img src="{{ asset('resim/revolution-slider/esinavsalonu-slide-'.rand(1,15).'.JPG') }}"
                     alt=""
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">

                <div class="tp-caption"
                     data-x="['177','177','center','center']" data-hoffset="['0','0','-150','-220']"
                     data-y="180"
                     data-start="1000"
                     data-transform_in="x:[-300%];opacity:0;s:500;"><img src="{{ asset('img/slides/slide-title-border-light.png') }}" alt=""></div>

                <div class="tp-caption top-label"
                     data-x="['227','227','center','center']"
                     data-y="172"
                     data-fontsize="['24','24','24','36']"
                     data-start="500"
                     data-transform_in="y:[-300%];opacity:0;s:500;">Kaçıncı Sıradasınız ?</div>

                <div class="tp-caption"
                     data-x="['480','480','center','center']" data-hoffset="['0','0','150','220']"
                     data-y="180"
                     data-start="1000"
                     data-transform_in="x:[300%];opacity:0;s:500;"><img src="{{ asset('img/slides/slide-title-border-light.png') }}" alt=""></div>

                <div class="tp-caption main-label"
                     data-x="['135','135','center','center']"
                     data-y="['210','210','210','230']"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-fontsize="['62','62','62','82']"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     data-mask_in="x:0px;y:0px;">TÜRKİYE'NİN EN BÜYÜK SINAV SALONU</div>

                <div class="tp-caption bottom-label"
                     data-x="['185','185','center','center']"
                     data-y="['280','280','280','315']"
                     data-start="2000"
                     data-fontsize="['20','20','20','30']"
                     data-transform_in="y:[100%];opacity:0;s:500;">KPSS, YDS, Ehliyet, Özel Güvenlik, Görevde Yükselme ve Unvan Değişikliği Sınavları</div>

            </li>
            <li data-transition="fade">

                <img src="{{ asset('resim/revolution-slider/esinavsalonu-slide-'.rand(16,30).'.JPG') }}"
                     alt=""
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">

                <div class="tp-caption featured-label"
                     data-x="center"
                     data-y="210"
                     data-start="500"
                     data-fontsize="['52','52','52','82']"
                     style="z-index: 5"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;">ESINAVSALONU.COM'A HOŞGELDİNİZ</div>

                <div class="tp-caption bottom-label"
                     data-x="center"
                     data-y="['270','270','270','290']"
                     data-start="1000"
                     data-fontsize="['23','23','23','38']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;s:600;e:Power4.easeInOut;"
                     data-transform_out="opacity:0;s:500;"
                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                     data-splitin="chars"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="font-size: 23px; line-height: 30px;"
                     data-elementdelay="0.05">Online olarak pek çok türde sınav çözebileceğiniz en güzel sitedesiniz</div>

            </li>
        </ul>
    </div>
</div>