@extends('admin.template')

@section('css')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title sinaw" id="{{ $sinav->id }} ">
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                            <a href="{{ url()->previous() }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                                Geri Dön </a>
                        </button>
                        {{ breadCrumb() }} <br>
                        <small>{{ $sinav->adi }}</small>
                    </h2>

                </header>

                <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Sınav Durumu</label>
                            <div class="col-md-6">
                                <div class="switch switch-sm switch-success" id="swr_{{ $sinav->id }}"
                                     data-toggle="tooltip" data-placement="top" title="" data-original-title="Sınav Onay Durumu Güncelle">
                                    <input type="checkbox" class="sinav-onay-update" id="sw_{{ $sinav->id }}"
                                           name="switch" data-plugin-ios-switch @if($sinav->durum === 1) checked @endif />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Sınav</label>
                            <div class="col-md-6" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sınav Adını Güncelle">
                                <input type="text" class="form-control sinav-name-update" id="inputDefault" value="{{ $sinav->adi }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Sınav Kategorileri</label>
                            <div class="col-md-6" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sınav Kategorisini Güncelle">
                                <select data-plugin-selectTwo class="form-control populate sinav-kategori-update">
                                    @foreach($sinavKategorileri as $sinavKat)
                                        <option value="{{ $sinavKat->id }}" @if($sinavKat->id === $sinav->kategori_id) selected @endif>{{ $sinavKat->kategori_adi }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                </div>
            </section>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>

    <script>
        $('.sinav-kategori-update').change(function () {
            let sinav_id = $('.sinaw').attr('id');
            let sinav_kategori_id = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav/kategori/update',
                data:{
                    'sinav_id': sinav_id,
                    'sinav_kategori_id': sinav_kategori_id,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav kategorisi güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.sinav-name-update').change(function () {
            let sinav_id = $('.sinaw').attr('id');
            let name = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav/name/update',
                data:{
                    'sinav_id': sinav_id,
                    'name': name,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav adı güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.sinav-onay-update').change(function () {
            let chk_id = this.id;
            let sinav_id = chk_id.replace('sw_', '');
            let durum = $('#swr_' + sinav_id + ' .ios-switch').hasClass('on') ? 1 : 0;

            $.ajax({
                type: "POST",
                url: document.location.origin + '/ajax/sinav/durum/update',
                data: {
                    'sinav_id': sinav_id,
                    'durum': durum
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav durumu güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });
    </script>
@stop
