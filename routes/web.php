<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Sinav;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::view('/', 'front.index')->name('anasayfa');
Route::get('menu', function (){
return $yksHTML = file_get_contents(base_path('resources\views\front\menu\yks.blade.php'));
    return str_contains('YKS - 12. Sınıf - Matematik (Sayisal Yeterlilik) - Analitik Geometri', '12. Sınıf') == true ? 'ok': 'err';

    $sinavlar = Sinav::all();

    $yks = $sinavlar->filter(function ($value, $key){
       return  substr($value['adi'], 0, 5) === 'YKS -';
    });

    $dersler = collect();
    foreach ($yks as $k)
    {
        $sinavAdiDizi = explode('-', $k->adi);
        //oluşan dizinin 2. indexi dersi ifade eder.
        $dersler = $dersler->push(['sinif'=>trim($sinavAdiDizi[1]), 'ders'=>trim($sinavAdiDizi[2])]);
    }
      $siniflar = $dersler->pluck('sinif')->unique();
    return $a = $dersler->where('sinif', $siniflar->last())->unique()->pluck('ders');
     $a->pluck('ders');
});

Route::get('sinav-ekrani', 'SinavController@sinavEkrani')->name('ekran');
Route::view('sik-sorulan-sorular', 'front.main.sss')->name('sss');

Route::get('sinavlar/{url?}', 'SinavController@sinavEkraniSinavlariListele')->name('sinavlar');

Route::get('sinav-getir/{page}/{userId}/{url}', 'SinavController@getSorular');
Route::get('get/question-image/{qId}', 'SoruController@getQuestionImage')->name('soruPaylas');
Route::get('set/question-image/{qId}', 'SoruController@saveQuestionImage');
Route::get('sinav/{url}', 'SinavController@getSinavView')->name('sinav');

Route::get('confirm_ebulten/{link}', 'UserController@ebultenInsert');

//Axios Ajax Sınav Sayfası
Route::get('/sinav-infos/{url}', 'SinavController@getSinavGeneralInfos');
Route::post('kac_kez_cozdun', 'SinavController@getKacKezCozdun');
Route::get('/sinav-success/{url}', 'SinavController@getSinavSuccessfulUsers');
Route::post('/sinav-cevap-save/', 'SinavController@setCevapSave');
Route::get('/sinav-cevap/{id}', 'SinavController@getCevapCheck');

Route::post('set/yorum-gonder', 'SinavController@setYorumGonder');
Route::post('set/hatali-soru-bildir', 'SoruController@setHataliSoruBildir');

Route::get('get/sehirler', 'UserController@sehirler')->name('sehirler');
Route::get('get/ilceler/{sehir_id}', 'UserController@ilceler')->name('ilceler');
Route::get('get/meslekler', 'UserController@meslekler')->name('meslekler');
Route::get('get/takimlar', 'UserController@takimlar')->name('takimlar');
Route::get('get/sinavlar', 'UserController@sinavlar')->name('userSinavlar');
Route::get('get/yorumlar/{id}', 'SinavController@getYorumlar')->name('yorumlar');

Route::post('set/ebulten', 'UserController@ebultenCheck');
Route::post('set/mesaj-gonder', 'UserController@mesajGonder');

// Authentication Routes...
Route::get('giris', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('giris', 'Auth\LoginController@login');
Route::post('cikis', 'Auth\LoginController@logout')->name('logout');

Route::get('auth/callback/{provider}', 'SocialAuthController@callback');
Route::get('auth/redirect/{provider}', 'SocialAuthController@redirect');

// Registration Routes...
Route::get('kayit', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('kayit', 'Auth\RegisterController@register');
Route::get('confirm_account/{url}', 'Auth\RegisterController@confirmAccount')->name('confirm');
Route::view('confirm_account', 'auth.confirm')->name('confirm.page');
Route::post('confirm_account_send_code', 'Auth\RegisterController@confirmAccountSendCode')->name('confirm.send');

Route::group(['prefix'=>'sifre'], function (){
    // Password Reset Routes...
    Route::get('sifirla', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('sifirla/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('sifirla', 'Auth\ResetPasswordController@reset');
});

Route::get('uye/{link}', 'UserController@getUserProfilePage')->name('profilePage');
//Route::get('ortalama', 'UserController@getUserProfilePage')->name('profilePage');

Route::get('hesap-yonetimi/{link}', 'UserController@hesapGoruntule')->name('account.management')->middleware('auth');

Route::group(['prefix' => 'kokpit', 'middleware'=> 'admin'], function (){
    Route::get('/', 'SoruController@indexKokpit')->middleware('admin')->name('admin');

    Route::group(['prefix'=> 'soru'], function (){
        Route::get('ekle', 'SoruController@getSoruEkle')->name('getSoruEkle');
        Route::post('ekle', 'SoruController@setSoruEkle')->name('setSoruEkle');
        Route::post('ekle-insert', 'SoruController@insertSoruEkle')->name('insertSoruEkle');
        Route::get('onaysiz', 'SoruController@getOnaysizSorular')->name('getOnaysiz');
        Route::get('cevapsiz', 'SoruController@getCevapsizSorular')->name('getCevapsiz');
        Route::get('soru-kategori', 'SoruController@getSoruKategorileri')->name('getSoruKategorileri');
        Route::get('kategorisiz-sorular', 'SoruController@getKategorisizSorular')->name('getKategorisizSorular');
    });

    Route::group(['prefix'=> 'cevap'], function (){
        Route::get('update/{soru_id}/{cevap_id}', 'CevapController@getCevapUpdate')->name('getCevapUpdate');
    });

    Route::group(['prefix'=>'soru-kaynak'], function (){
        Route::get('ekle', 'SoruKaynakController@getSoruKaynakEkle')->name('getSoruKaynakEkle');
        Route::post('ekle', 'SoruKaynakController@setSoruKaynakEkle')->name('setSoruKaynakEkle');
        Route::get('listele', 'SoruKaynakController@getSoruKaynakListele')->name('getSoruKaynakListele');
    });

    Route::group(['prefix' => 'soru-kategori'], function (){
        Route::get('{id}/sorular', 'SoruController@getKategoriWithSorular')->name('getKategoriWithSorular');
    });

    Route::group(['prefix'=>'sinav'], function (){
       Route::get('listele', 'SinavController@indexSinavlar')->name('getSinavlar');
       Route::get('update/{id}', 'SinavController@getUpdate')->name('getUpdate');
       Route::get('onaysiz', 'SinavController@getOnaysizSinavlar')->name('getOnaysizSinavlar');
       Route::get('kategorisiz', 'SinavController@getKategorisizSinavlar')->name('getKategorisizSinavlar');
       Route::get('{id}/sorular', 'SinavController@getSinavSorulari')->name('getSinavSorulari');
       Route::get('tipler', 'SinavController@getSinavTip')->name('getSinavTip');
       Route::get('sinav-tip/{id}', 'SinavController@getSinavTipPage')->name('getSinavTipPage');
       Route::get('tip/ekle', 'SinavController@getSinavTipEkle')->name('getSinavTipEkle');
       Route::post('tip/save', 'SinavController@setSinavTipEkle')->name('setSinavTipEkle');
       Route::post('dropzone-tip/{id}', 'SinavController@dropzoneTip')->name('dropzoneTip');

       Route::group(['prefix' => 'sinav-kategori'], function (){
            Route::get('listele', 'SinavController@getSinavKategorileri')->name('getSinavKategorileri');
            Route::get('{id}/sinavlari', 'SinavController@getKatSinavlar')->name('getKatSinavlar');
            Route::get('{id}/edit', 'SinavController@getKatSinavEdit')->name('getKatSinavEdit');
            Route::get('insert', 'SinavController@getKatSinavInsert')->name('getKatSinavInsert');
        });
    });

    Route::group(['prefix'=>'yorum'], function(){
       Route::get('onaylananlar', 'YorumController@index')->name('yorumlar');
       Route::get('onay-bekleyenler', 'YorumController@onaysizYorumlar')->name('onaysizYorumlar');
       Route::get('silinenler', 'YorumController@silinenYorumlar')->name('silinenYorumlar');
    });

    Route::group(['prefix' => 'uye'], function (){
       Route::get('onaylananlar', 'UserController@getOnayliUyeler')->name('getOnayliUyeler');
       Route::get('onay-bekleyenler', 'UserController@getOnayBekleyenUyeler')->name('getOnayBekleyenUyeler');
       Route::get('silinenler', 'UserController@getSilinenUyeler')->name('getSilinenUyeler');
    });

    Route::group(['prefix' => 'ebulten'], function(){
        Route::get('/', 'UserController@ebulten')->name('ebulten');
    });

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
}); 

//Admin Panel Ajax Rotaları
Route::group(['prefix' => 'ajax', 'middleware'=> 'web'], function () {
    Route::group(['prefix' => 'soru-kaynak'], function (){
        Route::post('update', 'SoruKaynakController@ajaxSoruKaynakUpdate');
        Route::post('delete', 'SoruKaynakController@ajaxSoruKaynakDelete');
        Route::post('insert', 'SoruKaynakController@setSoruKaynakEkle');
    });

    Route::group(['prefix' => 'soru'], function (){
        Route::post('update', 'SoruController@ajaxSoruUpdate');
        Route::post('kategori-update', 'SoruController@ajaxSoruKategoriUpdate');
        Route::post('durum-update', 'SoruController@ajaxSoruDurumUpdate');
    });

    Route::group(['prefix' => 'cevap'], function (){
        Route::post('update', 'CevapController@ajaxCevapUpdate');
        Route::post('dogru-update', 'CevapController@ajaxCevapDogruUpdate');
    });

    Route::group(['prefix' => 'sinav-tip'], function (){
        Route::post('soru-kategori/update', 'SinavController@ajaxSinavTipSoruKategoriUpdate');
        Route::post('sinav-kategori/update', 'SinavController@ajaxSinavTipSinavKategoriUpdate');
        Route::post('name/update', 'SinavController@ajaxSinavTipNameUpdate');
    });

    Route::group(['prefix' => 'sinav'], function (){
        Route::post('durum/update', 'SinavController@ajaxSinavDurumUpdate');
        Route::post('name/update', 'SinavController@ajaxSinavNameUpdate');
        Route::post('kategori/update', 'SinavController@ajaxSinavKategoriUpdate'); //tekil sınavın kategorisi değiştirir
        Route::post('delete', 'SinavController@ajaxSinavDelete');
    });

    Route::group(['prefix' => 'sinav-kategori'], function (){
        //ilgili sınav kategorisinden soru sayısı yettiğince yeni sınavlar ekler
        Route::get('sinavs/insert/{sinavKatId}', 'SinavController@ajaxSinavKategoriSinavInsert')->name('insertSinavs');
        Route::post('pattern/insert', 'SinavController@ajaxSinavKategoriPatternInsert');
        Route::post('pattern/update', 'SinavController@ajaxSinavKategoriPatternUpdate');
    });

    Route::group(['prefix' => 'soru-kategori'], function (){
        Route::post('insert', 'SoruController@ajaxSoruKategoriInsert');
    });

    Route::group(['prefix' => 'yorum'], function (){
        Route::post('onay-update', 'YorumController@ajaxYorumOnayUpdate');
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('onay-update', 'UserController@ajaxUserDurumUpdate');
    });
});

Auth::routes();

