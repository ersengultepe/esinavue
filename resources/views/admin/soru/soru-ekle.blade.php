@extends('admin.template')

@section('css')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title">Veritabanımıza Yeni Sorular Ekliyoruz</h2>
                    <small>Regex ile ayrıştırma yöntemi kullanacağız</small>
                </header>
                <div class="panel-body">
                    <form class="form-horizontal form-bordered" method="post" action="{{ route('setSoruEkle') }}">
                    @csrf
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-lg btn-block">
                            <i class="fa fa-spinner"></i>
                            Soruları Ayıkla
                        </button>
                        <br>
                        <!-- start: Sınav Olarak Kaydedilsin mi -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="isSinav">Sınav Olarak Kayededilsin mi?</label>
                            <div class="col-md-6">
                                <select name="sinav_mi" class="form-control mb-md" data-toggle="tooltip" data-trigger="hover" data-original-title="Doğrudan Sınav Olarak da Kaydedilebilir" id="isSinav">
                                    <option value="1">Hayır</option>
                                    <option value="0">Evet</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: Sınav Olarak Kaydedilsin mi -->

                        <!-- start: E Şıkkı Var mı -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="hasE">E Şıkkı Var mı?</label>
                            <div class="col-md-6">
                                <select name="e_varmi" class="form-control mb-md" data-toggle="tooltip" data-trigger="hover" data-original-title="E şıkkı olup olmadığına göre farklı algoritmalar çalışacak" id="hasE">
                                    <option value="0">Hayır</option>
                                    <option value="1">Evet</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: E Şıkkı Var mı -->

                        <!-- start: Soru Biçimi -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="soruBicimi">Soru Biçimi</label>
                            <div class="col-md-6">
                                <select name="soru_bicimi" class="select2_single form-control" tabindex="1" data-toggle="tooltip" data-trigger="hover" data-original-title="Soru biçimine göre Regex ile yakalayacağız" id="soruBicimi">
                                    <option value='/[0-9]{1,}\.(.*?)' >1.</option>
                                    <option value='/Soru [0-9]{1,}\.(.*?)' >Soru 1.</option>
                                    <option value='/Soru [0-9]{1,}\)(.*?)' >Soru 1)</option>
                                    <option value='/Soru [0-9]{1,}\-(.*?)' >Soru 1-</option>
                                    <option value='/Soru [0-9]{1,}(.*?)' >Soru 1</option>
                                    <option value='/SORU [0-9]{1,}(.*?)' >SORU 1</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: Soru Biçimi -->

                        <!-- start: Cevap Biçimi -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="cevapBicimi">Cevap Biçimi</label>
                            <div class="col-md-6">
                                <select name="cevap_bicimi" class="select2_single form-control" tabindex="1" data-toggle="tooltip" data-trigger="hover" data-original-title="Cevap biçimine göre Regex ile yakalayacağız" id="cevapBicimi">
                                    <option value="parantez">Cevap A)</option>
                                    <option value="nokta">cevap A.</option>
                                    <option value="tire">Cevap A-</option>
                                    <option value="noktaK">Cevap a.</option>
                                    <option value="parantezK">Cevap a)</option>
                                    <option value="tireK">Cevap a-</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: Cevap Biçimi -->

                        <!-- start: Soru Kaynakları -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="soruKaynak">Soru Kaynağı</label>
                            <div class="col-md-6" data-toggle="tooltip" data-trigger="hover" data-original-title="Soruların Alındığı Sınavlar Vb.">
                                <select name="soru_kaynak_id" data-plugin-selectTwo class="select2_single form-control" tabindex="1" id="soruKaynak">
                                    @foreach($soruKaynaklari as $kaynak)
                                        <option value="{{ $kaynak->id }}">{!! str_limit($kaynak->ad) !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- end: Soru Kaynakları -->

                        <!-- start: Soru Kategorisi -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="soruKat">Soru Kategorisi</label>
                            <div class="col-md-6" data-toggle="tooltip" data-trigger="hover" data-original-title="Soruların Kategorisini Seçiniz">
                                <select name="soru_kategori" data-plugin-selectTwo class="select2_single form-control populate" tabindex="1" id="soruKat">
                                    @foreach($soruKategorileri as $kat)
                                        <option value="{{ $kat->id }}">{!! str_limit($kat->kategori_adi) !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- end: Soru Kategorisi -->

                        <!-- start: Eklenecek Sorular -->
                        <div class="form-group">
                            <label class="control-label col-md-3" for="eklenecekSorular">Eklenecek Sorular</label>
                            <div class="col-md-6" data-toggle="tooltip" data-trigger="hover" data-original-title="Soruları bu alana yazınız..!" id="eklenecekSorular">
                                    <textarea id="eklenecekSorular" required="required" class="form-control" name="metin"
                                              style="margin: 0px -2.5px 0px 0px; height: 650px;"></textarea>
                            </div>
                        </div>
                        <!-- end: Eklenecek Sorular -->

                        <!-- start: Cevap Anahtarı -->
                        <div class="form-group">
                            <label class="control-label col-md-3" for="cevapAnahtari">Cevap Anahtarı</label>
                            <div class="col-md-6" data-toggle="tooltip" data-trigger="hover" data-original-title="Cevap Anahtarını buraya ekleyiniz" id="cevapAnahtari">
                                    <textarea id="message" required="required" class="form-control" name="dogru_cevaplar"
                                              style="margin: 0px -2.5px 0px 0px; height: 650px;"></textarea>
                            </div>
                        </div>
                        <!-- end: Cevap Anahtarı -->
                        <br>
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-lg btn-block">
                            <i class="fa fa-spinner"></i>
                            Soruları Ayıkla
                        </button>

                    </form>
                </div>
            </section>
        </div>
    </div>
    @stop
@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    @stop