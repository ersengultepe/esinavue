<div class="col-lg-3">
    <aside class="sidebar">

        <div class="tabs mb-4 pb-2" v-if="success_users.length > 0">
            <ul class="nav nav-tabs">
                <li class="nav-item active"><a class="nav-link" href="#popularPosts" data-toggle="tab"><i class="fas fa-star"></i> Sınav Başarı Listesi</a></li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="popularPosts">
                    <ul class="simple-post-list">

                        <li v-for="user of success_users">
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a :href="userSlug(user.id, user.kullanici)">
                                        <img :id="user.id" :src="userPhoto(user.foto)" :alt="user.kullanici" style="height:50px;width:50px;animation-duration: 5s; animation-delay: 0ms;" class="appear-animation animated tada appear-animation-visible" data-appear-animation="tada" data-appear-animation-delay="0" data-appear-animation-duration="1s" >
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a :href="userSlug(user.id, user.kullanici)">@{{ user.kullanici }}</a>
                                <div class="post-meta">
                                    @{{ user.sinav_puan }}
                                </div>
                                <div class="post-meta">
                                    @{{ user.sure }}
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>
        </div>

        <hr>

        <h4 class="heading-primary">Reklam</h4>
        <div class="col-md-12">

            <!-- esinavsalonu yan alan -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:300px;height:600px"
				 data-ad-client="ca-pub-2211050103220921"
				 data-ad-slot="9358695495"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>

		</div>

    </aside>
</div>