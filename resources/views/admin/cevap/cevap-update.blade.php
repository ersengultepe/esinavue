@extends('admin.template')

@section('css')

@stop

@section('content')
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                    <a href="{{ url()->previous() }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                        Geri Dön
                    </a>
                </button>
            </h2>
        </header>

        <div class="panel-body">
            @foreach($cevaplar as $cevap)
                <div class="col-md-6">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="panel-actions">
                                <a href="#" class="fa fa-caret-down"></a>
                                <a href="#" class="fa fa-times"></a>
                            </div>

                            <div class="form-group">
                                <button type="button" id="bt_{{ $cevap->id }}" class="col-md-3 mb-xs mt-xs mr-xs btn btn-primary cevap-update"
                                        style="margin-bottom: 1px;margin-top: 1px !important;"><i class="fa fa-edit"></i> Cevabı Güncelle</button>
                                <div class="switch switch-sm switch-success" id="swr_{{ $cevap->id }}">
                                    <input type="checkbox" class="cevap-dogru-update" id="sw_{{ $cevap->id }}" name="switch" data-plugin-ios-switch @if($cevap->dogru === 1) checked @endif />
                                </div>
                            </div>
                        </header>
                        <div class="panel-body">
                            <textarea name="cevap" class="ckeditor" id="cv_{{  $cevap->id  }}" rows="5">{!! turkceAsci($cevap->cevap) !!}</textarea>
                        </div>
                    </section>
                </div>

                @endforeach
        </div>

    </section>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>
    <script>
        $('.cevap-update').click(function () {
            let buton_id = this.id;
            cevap_id = buton_id.replace('bt_', '');
            updateCevap = CKEDITOR.instances['cv_'+cevap_id].getData();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/cevap/update',
                data:{
                    'id': cevap_id,
                    'cevap': updateCevap
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Cevap güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.cevap-dogru-update').change(function () {
            let chk_id = this.id;
            let cevap_id = chk_id.replace('sw_', '');
            let dogru = $('div.ios-switch').hasClass('on') ? 'checked' : 'unchecked';
            let soru_id = {{ $soru->id }}

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/cevap/dogru-update',
                data:{
                    'cevap_id': cevap_id,
                    'soru_id' : soru_id,
                    'dogru' : dogru
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Cevap güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });

                $('div.ios-switch').removeClass('on');

            if (dogru === 'checked'){
                $('#swr_'+cevap_id+' .ios-switch').addClass('on');
            }else{
                $('#swr_'+cevap_id+' .ios-switch').removeClass('on');
                $('#swr_'+cevap_id+' .ios-switch').addClass('off');
            }

        });
    </script>
@stop
