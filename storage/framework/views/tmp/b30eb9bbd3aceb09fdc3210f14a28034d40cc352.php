<?php $__env->startSection('title'); ?>
    <title><?php echo e(config('app.name').' | '.config('app.slogan')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-shop.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active">Sınav Ekranı </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Sınav Ekranı</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <hr class="d-lg-none tall">

                    <div class="toggle toggle-primary" data-plugin-toggle="" data-plugin-options="{ 'isAccordion': true }">
                        <section class="toggle active">
                            <label>GÖREVDE YÜKSELME VE UNVAN DEĞİŞİKLİĞİ SINAVLARI</label>
                            <div class="toggle-content" style="display: block;">
                                <div class="row mt-lg-12">
                                    <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(in_array($tip->id, [13,18,19,23]) === false): ?>
                                                <div class="col-lg-2">
                                            <a href="<?php echo e(route('sinavlar',[$tip->link])); ?>">

                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($tip->sinav_tip); ?>">
												<img class="img-fluid" src="<?php echo e(asset($tip->logo)); ?>" alt="<?php echo e($tip->sinav_tip); ?>">
											</span>
                                            
                                            </a>
                                            </div>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>ÖSYM SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($tip->id ==18 || $tip->id==19): ?>
                                        <div class="col-lg-2">
                                            <a href="<?php echo e(route('sinavlar',[$tip->link])); ?>">
                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($tip->sinav_tip); ?>">
												<img class="img-fluid" src="<?php echo e(asset($tip->logo)); ?>" alt="<?php echo e($tip->sinav_tip); ?>">
											</span>
                                                
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>ÖZEL GÜVENLİK SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                    <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($tip->id == 13): ?>
                                            <div class="col-lg-2">
                                            <a href="<?php echo e(route('sinavlar',[$tip->link])); ?>">
                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($tip->sinav_tip); ?>">
												<img class="img-fluid" src="<?php echo e(asset($tip->logo)); ?>" alt="<?php echo e($tip->sinav_tip); ?>">
											</span>
                                            
                                            </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>EHLİYET SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                    <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($tip->id == 23): ?>
                                            <div class="col-lg-2">
                                            <a href="<?php echo e(route('sinavlar',[$tip->link])); ?>">
                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo e($tip->sinav_tip); ?>">
												<img class="img-fluid" src="<?php echo e(asset($tip->logo)); ?>" alt="<?php echo e($tip->sinav_tip); ?>">
											</span>
                                            
                                            </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

    <!-- Vendor -->
    <script src="<?php echo e(asset('/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/popper/umd/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/common/common.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/isotope/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/vide/vide.min.js')); ?>"></script>

    <!-- Theme Base, Components and Settings -->
        <script src="<?php echo e(asset('/js/theme.js')); ?>"></script>

    <!-- Current Page Vendor and Views -->
    
    

    <!-- Theme Custom -->
        <script src="<?php echo e(asset('/js/custom.js')); ?>"></script>

    <!-- Theme Initialization Files -->
        <script src="<?php echo e(asset('/js/theme.init.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>