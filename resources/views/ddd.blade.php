@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları ESINAVSALONU Ersen ">
    <meta property="og:title" content="İnsanın aklına olan güveni, insana saygı ve değeri, pozitif bilimlerin tek yol gösterici olduğunu temel alan aydınlanma felsefesidir. Buna göre aşağıdakilerden hangisi aydınlanma felsefesinin savunucularından değildir?" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="Online Deneme Sınavları Esinav" />
    <meta property="og:site_name" content="esinav" />
    <meta property="og:locale" content="tr_TR" />
    <meta property="article:author" content="https://www.facebook.com/esinavsalonu/" />
    <meta property="article:section" content="Türkiye" />
    <meta property="og:url" content="http://softexts.xyz/get/question-image/84705" />
    <meta property="og:image" content="http://softexts.xyz/resim/soru_resimleri/84705.jpg" />
    <meta property="og:image:alt" content="EsinavSalonu.com" />
    <meta property="fb:pages" content="1469945773313460" />
@stop

@section('css')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '242614719760687',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/navigation.css') }}">
@stop

@section('content')
    @include('front.part.header')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active"><a href="{{ route('ekran') }}">Sınav Ekranı</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    {{--<div class="col">--}}
                    <div class="col-lg-1">
                        <img class="img-fluid rounded mb-4" src="{{ asset($sinavTip->logo) }}"
                             alt="{{ $sinavTip->sinav_tip }}">

                    </div>
                    <div class="col-lg-11">
                        <h1>{{ $sinavTip->sinav_tip }} </h1>
                    </div>
                    {{--</div>--}}
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">

                <div id="shareBtn" class="btn btn-success clearfix">Paylaş</div>

                <a href="http://twitter.com/share?url='http://localhost:8000/get/question-image/84458'&via=trucsweb&image-src='//localhost:8000/resim/soru_resimleri/84458.jpg'&text='esinav' ">tweeeeeet</a>

                {{--<a class="twitter-timeline" data-lang="tr" data-dnt="true" data-theme="light" href="https://twitter.com/ESINAVSALONU">National Park Tweets - Curated tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--}}

                @if($sinavKategorileri->where('sinav_format',3)->isEmpty() !== true)
                    <div class="col-lg-12">
                        <h2>GEÇMİŞ SINAVLAR & DENEME SINAVLARI</h2>
                        @foreach($sinavKategorileri->where('sinav_format',3) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                @endif

                @if($sinavKategorileri->where('sinav_format', 1)->isEmpty() !== true )
                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ORTAK SINAVLAR</h2>
                        @foreach($sinavKategorileri->where('sinav_format', 1) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach

                        <br>
                        <hr class="tall">
                    </div>
                @endif


                @if($sinavKategorileri->where('sinav_format', 2)->isEmpty() !== true)

                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ALAN SINAVLARI</h2>

                        @foreach($sinavKategorileri->where('sinav_format', 2) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                @endif
            </div>
        </div>

    </div>

@stop

@section('js')

    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('/vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('/vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/vendor/vide/vide.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('/js/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('/js/theme.init.js') }}"></script>

    <script>
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: window.location.href.replace('localhost:8000', 'esinavsalonu.com'),
            }, function(response){});
        }
    </script>

@stop@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
    <meta property="fb:app_id"          content="1234567890" />
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="http://newsblog.org/news/136756249803614" />
    <meta property="og:title"           content="Introducing our New Site" />
    <meta property="og:image"           content="https://scontent-sea1-1.xx.fbcdn.net/hphotos-xap1/t39.2178-6/851565_496755187057665_544240989_n.jpg" />
    <meta property="og:description"    content="http://samples.ogp.me/390580850990722" />
@stop

@section('css')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '242614719760687',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/navigation.css') }}">
@stop

@section('content')
    @include('front.part.header')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active"><a href="{{ route('ekran') }}">Sınav Ekranı</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    {{--<div class="col">--}}
                    <div class="col-lg-1">
                        <img class="img-fluid rounded mb-4" src="{{ asset($sinavTip->logo) }}"
                             alt="{{ $sinavTip->sinav_tip }}">

                    </div>
                    <div class="col-lg-11">
                        <h1>{{ $sinavTip->sinav_tip }} </h1>
                    </div>
                    {{--</div>--}}
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">

                <div id="shareBtn" class="btn btn-success clearfix">Paylaş</div>

                <a href="http://twitter.com/share?url='http://localhost:8000/get/question-image/84458'&via=trucsweb&image-src='//localhost:8000/resim/soru_resimleri/84458.jpg'&text='esinav' ">tweeeeeet</a>

                {{--<a class="twitter-timeline" data-lang="tr" data-dnt="true" data-theme="light" href="https://twitter.com/ESINAVSALONU">National Park Tweets - Curated tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>--}}

                @if($sinavKategorileri->where('sinav_format',3)->isEmpty() !== true)
                    <div class="col-lg-12">
                        <h2>GEÇMİŞ SINAVLAR & DENEME SINAVLARI</h2>
                        @foreach($sinavKategorileri->where('sinav_format',3) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                @endif

                @if($sinavKategorileri->where('sinav_format', 1)->isEmpty() !== true )
                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ORTAK SINAVLAR</h2>
                        @foreach($sinavKategorileri->where('sinav_format', 1) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach

                        <br>
                        <hr class="tall">
                    </div>
                @endif


                @if($sinavKategorileri->where('sinav_format', 2)->isEmpty() !== true)

                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ALAN SINAVLARI</h2>

                        @foreach($sinavKategorileri->where('sinav_format', 2) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                @endif
            </div>
        </div>

    </div>

@stop

@section('js')

    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('/vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('/vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/vendor/vide/vide.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('/js/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('/js/theme.init.js') }}"></script>

    <script>
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: window.location.href.replace('localhost:8000', 'esinavsalonu.com'),
            }, function(response){});
        }
    </script>

    {{--    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />--}}
    <!-- Specific Page Vendor CSS -->
    {{--    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />--}}
    {{--    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />--}}
    {{--    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/morris/morris.css') }}" />--}}
    {{--    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />--}}

    <!-- Vendor -->
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery/jquery.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/magnific-popup/magnific-popup.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>--}}
    {{--<!-- Specific Page Vendor -->--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-appear/jquery.appear.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.pie.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.categories.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.resize.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/raphael/raphael.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/morris/morris.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/gauge/gauge.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/snap-svg/snap.svg.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/liquid-meter/liquid.meter.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>--}}
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-maskedinput/jquery.maskedinput.js') }}"></script>--}}

    {{--<!-- Specific Page Vendor -->--}}
    {{--<script src="{{ asset('pa/HTML/assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>--}}
@stop