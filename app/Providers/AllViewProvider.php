<?php

namespace App\Providers;

use App\Http\Controllers\UserController;
use App\Models\Sinav;
use App\Models\SinavKategori;
use App\Models\SinavTip;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AllViewProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('front.*', function($view){
            $sinavlar = Sinav::all();

            $yorum = new UserController();
            $sonYorumlar = $yorum->getYorumlar(3);

            ### <start>   YKS ###
            $yks = $sinavlar->filter(function ($value, $key){
                return  substr($value['adi'], 0, 5) === 'YKS -';
            });
            $dersler = collect();
            foreach ($yks as $k)
            {
                $sinavAdiDizi = explode('-', $k->adi);
                //oluşan dizinin 2. indexi dersi ifade eder.
                $dersler = $dersler->push(['sinif'=>trim($sinavAdiDizi[1]), 'ders'=>trim($sinavAdiDizi[2])]);
            }
            $siniflar = $dersler->pluck('sinif')->unique();
            ### </finish> YKS ###

            ### <start>   GÖREVDE YÜKSELME SINAVLARI ###
            $sinavTipleri = SinavTip::all();
            $sinavKategoriListesi = SinavKategori::all();

            /*$sinavKategoriVEsinavlari = collect();
            foreach ($sinavTipleri as $tip)
            {
                $sinavKategoriListesi = SinavKategori::all();
                $sinavKats = collect($tip->sinav_kategori_ids)->first();
                $sinavKategoriVEsinavlari = $sinavKategoriVEsinavlari->push(['tip'=>$tip->sinav_adi, 'sinavKategori'=>$])
            }*/
            ### </finish> GÖREVDE YÜKSELME SINAVLARI ###

            $view->with([
                'siniflar' => $siniflar,
                'dersler' => $dersler,
                'yks' => $yks,
                'sinavTipleri' => $sinavTipleri,
                'sinavKategoriListesi' => $sinavKategoriListesi,
                'sinavlar' => $sinavlar,
                'sonYorumlar'   => $sonYorumlar
            ]);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
