<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Yorum extends Model
{
    protected $table    =   'yorums';

    public $with = [
        'user'
    ];

    protected $fillable = [
        'yorum',
        'tur',
        'icerik',
        'ekleyen',
        'yorum_cevap_id',
        'user_id',
        'email',
        'kullanici',
        'ip',
        'onay',
        'tarih'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function adres()
    {   //Yorumun yapıldığı sınavı çek. Tür=2 sınav Tür =1 haber artık yok.
        return $this->belongsTo( Sinav::class, 'icerik', 'id');
    }
}
