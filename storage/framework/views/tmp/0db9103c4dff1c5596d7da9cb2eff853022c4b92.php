<?php $__env->startSection('title'); ?>
    <title><?php echo e(config('app.name').' | '.config('app.slogan')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="{{ desc }}">
    <meta property="og:title" content="{{ title }}"/>
    <meta property="og:type" content="article" />
    <meta property="og:description" content="SORU : {{ desc }}" />
    <meta property="og:site_name" content="esinav" />
    <meta property="og:locale" content="tr_TR" />
    <meta property="article:author" content="https://www.facebook.com/esinavsalonu/" />
    <meta property="article:section" content="Türkiye" />
    <meta property="og:url" content="{{ url }}" />
    <meta property="og:image" content="{{ img }}" />
    <meta property="og:image:alt" content="EsinavSalonu.com" />
    <meta property="fb:pages" content="1469945773313460" />

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@esinavsalonu">
    <meta name="twitter:title" content="{{ title }}">
    <meta name="twitter:description" content="{{ desc }}">
    <meta name="twitter:creator" content="@esinavsalonu">
    <meta name="twitter:image:src" content="{{ img }}">
    <meta name="twitter:image:alt" content="{{ desc }}" />
    <meta name="twitter:domain" content="http://softexts.xyz/">
    <?php echo csrf_field(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '242614719760687',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>
        window.twttr = (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
            if (d.getElementById(id)) return t;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);

            t._e = [];
            t.ready = function(f) {
                t._e.push(f);
            };

            return t;
        }(document, "script", "twitter-wjs"));
    </script>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-shop.css')); ?>">
    <style>
        .sidenav {
            width: auto;
            position: fixed;
            z-index: 1;
            top: 30%;
            left: 10px;
            background: #28a745;
            overflow-x: hidden;
            padding: 8px;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <a id="bastaraf"></a>
    <div v-if="preloadx=== true" id="preloadx">
        <Pencil style="margin-top:30%;margin-left:50%;margin-right:50%;z-index: 9999;position: fixed;">
        </Pencil>
    </div>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div role="main" class="main">

        <?php echo $__env->make('front.sinav.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="container">
            <div class="row">
                <span id="user-id" style="display: none"><?php echo e(Auth::id()); ?></span>
                <span id="user-kullanici" style="display: none"><?php if(auth()->guard()->check()): ?> <?php echo e(Auth::user()->kullanici); ?> <?php endif; ?></span>
                <span id="route-url-parameter" style="display: none"><?php echo e(Route::current()->parameters()['url']); ?></span>

                <div class="sidenav" style="background: initial;">
                    <div class="heading heading-tertiary heading-border heading-bottom-border">
                        <h3 class="heading-tertiary">Puan :<strong> {{ toplamPuan }} </strong></h3>
                    </div>
                    <div class="heading heading-secondary heading-border heading-bottom-border">
                        <h3 class="heading-secondary" id="sure"><strong>
                                <time v-model="sure">00:00:00</time>
                            </strong></h3>
                    </div>
                    <h4 class="heading-light background-color-primary" style="background-color: #2baab1 !important">
                        Doğru : {{ dogrular.length }}</h4>
                    <h4 class="heading-light background-color-primary"
                        style="background-color: rgb(227, 97, 89) !important">Yanlış : {{ yanlislar.length }}</h4>
                    <h4 class="heading-light background-color-primary"
                        style="background-color: rgb(0, 136, 204) !important">Boş : {{ (soru_sayisi -
                        Object.keys(secilenSiklar).length) }}</h4>
                </div>

                <?php echo $__env->make('front.sinav.sinav-alani', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <?php echo $__env->make('front.sinav.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            </div>

        </div>

    </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('js/front/front.js')); ?>"></script>
    <script src="<?php echo e(asset('js/sinav_sayfasi/sinav_values.js')); ?>"></script>
    <!-- Vendor -->
    <script src="<?php echo e(asset('/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/popper/umd/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/common/common.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/isotope/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/vide/vide.min.js')); ?>"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo e(asset('/js/theme.js')); ?>"></script>

    <!-- Theme Custom -->
    <script src="<?php echo e(asset('/js/custom.js')); ?>"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo e(asset('/js/theme.init.js')); ?>"></script>

    <script>
        let sure = document.getElementById('sure'),
            start = document.getElementById('start'),
            stop = document.getElementById('stop'),
            clear = document.getElementById('clear'),
            seconds = 0, minutes = 0, hours = 0,
            t;

        function add() {
            seconds++;
            if (seconds >= 60) {
                seconds = 0;
                minutes++;
                if (minutes >= 60) {
                    minutes = 0;
                    hours++;
                }
            }

            sure.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

            timer();
        }

        function timer() {
            t = setTimeout(add, 1000);
        }

        timer();
    </script>
    <script>
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: window.location.href,
                image: $('meta[property="og:image"]').attr('content'),
                redirect_uri : 'http://softexts.xyz',
                app_id: '242614719760687',
                hashtag : '#metehan'
            }, function(response){});
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>