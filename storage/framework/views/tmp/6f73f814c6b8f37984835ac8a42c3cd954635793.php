<?php $__env->startSection('title'); ?>
    <title><?php echo e(config('app.name').' | '.config('app.slogan')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds" />
    <meta name="description" content="Online Deneme Sınavları">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-shop.css')); ?>">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/settings.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/layers.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/rs-plugin/css/navigation.css')); ?>">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="body" >

        <div role="main" class="main" id="front">

            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <ul class="breadcrumb">
                                <li><a href="<?php echo e(route('anasayfa')); ?>">Anasayfa</a></li>
                                <li class="active">Sık Sorulan Sorular</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h1>Sık Sorulan Sorular</h1>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">

                <div class="row">
                    <div class="col">
                        <h2 class="mb-0">Sık Sorulan Sorular</h2>
                        <p>Site ile ilgili merak ettiğiniz sorular ve cevaplarını bulabilirsiniz.</p>

                    </div>
                </div>

                <div class="row">

                <div class="col-lg-12">

                <hr class="d-lg-none tall">

                <div class="toggle toggle-primary" data-plugin-toggle="" data-plugin-options="{ 'isAccordion': true }">
                    <section class="toggle active">
                        <label>Sınav puanı belirlenirken hangi kriterler uygulanıyor ?</label>
                        <div class="toggle-content" style="display: block;">
                            <p>Her soru için veritabanımızda kaç kez doğru ve yanlış çözüldüğü bilgisi tutulmaktadır. Her soru çözüldüğünde bu sayılar güncellenir.<br>
                                Soruların zorluk derecesi özel bir algoritma ile belirlenir. Sorular zorluk derecesine göre beş kategoride değerlendirilir. <br>
                                Bunlar sırasıyla; <strong>Çok zor, Zor, Normal, Kolay ve En Kolay</strong> olarak isimlendirilir.

                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: #dc3545;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>
                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: coral;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>
                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: grey;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>
                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: deepskyblue;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>
                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: forestgreen;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>

                                Örneğin; 10 soruluk bir sınavda bir sorunun değeri en fazla 10,5 olabilir, en az ise 9,5 olabilir. Bu sınırlar dışına soru puan değeri çıkamaz.
                            </p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Üyelerin toplam puanı nasıl hesaplanıyor, bir sınavı birden çok çözmek daha fazla puan getirir mi ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üyelerimizin her sınavdan elde ettiği puanlar veritabanında saklanmaktadır. Aynı sınav birden çok çözülmüşse bunların arasından en yüksek olanın puanı alınarak tüm çözülen sınavların puanları toplanır ve o üyenin toplam puanı elde edilmiş olur. Buradan da anlaşılacağı gibi bir sınavı çok çözmek değil o sınavdan en yüksek puanı almak önemlidir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Sitenin En Başarılıları hangi kriterlere göre belirleniyor?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Son 5000 sınav çözümü sonundaki verilere göre belirlenmektedir. Bir kriterde en başarılı çıkan üye diğer kriterlerde hariç tutulmaktadır. </p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Site üyeliği ücretsiz mi, üye olmak için neler yapmalıyım ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üyelik tamamen ücretsiz olup, üye olunduğunda sadece üyelere özel olan çok sayıda hizmetten faydalanılmaktadır.<br>
                            Üye olmak için hemek <a href="<?php echo e(route('register')); ?>">Kayıt</a> bağlantısını tıklamanız yeterlidir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Kayıt sırasında çok bilgi isteniliyor mu ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Sitemizin yenilenen sürümünde artık kayıt sırasında sadece Ad, Soyad, email ve şifre istenmektedir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Hesabımı silince bilgilerim de siliniyor mu ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Siteye şifrenizle giriş yaptıktan sonra sağ üst köşede resminizi tıkladığınızda hesabınıza ait sayfaya ulaşacaksınız.
                                Kişisel Ayarlarınız altında ilgili bağlantıyı tıklayarak hesabınızı hemen silebilirsiniz. <br>
                                Hesabınızı silmeniz halinde sitedeki tüm kayıtlı bilgileriniz <strong>kalıcı</strong> olarak silinecektir.
                                Bu bilgileri daha sonra geri döndürmek mümkün olmayacaktır. Çözdüğünüz sınavlara ait puanlar ise anonim hale gelecektir.
                            </p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Hesap bilgilerim başkalarıyla paylaşılıyor mu ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Kişisel bilgilerinizin hiç biri başkalarıyla paylaşılmamaktadır, tamamen esinavsalonu.com güvencesindedir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Sınavlara nasıl ulaşabilirim, hangi sayfadan hepsini görebilirim ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Sitemizde kayıtlı tüm sınavlara <a href="<?php echo e(route('ekran')); ?>">Sınav Ekranı</a> sayfasından ulaşabilirsiniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>E-bülten'e abone olursam ne sıklıkta mail gelir ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>E-bülten hizmeti sitemizde yayınlanan sınavlar ve hizmetler hakkında bilgi sahibi olmanız için verilmektedir. En fazla ayda bir kez e-bülten gönderilmektedir. Gelen e-bülten mailinde isterseniz en altta yer alan <strong>aboneliğimi iptal et</strong> bağlantısını tıklayarak veya hesabınıza erişip <strong>Kişisel Ayarlar</strong> kısmından da iptal edebilirsiniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Sınav çözdüğümde hangi bilgiler kaydediliyor ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Bir sınavı çözdüğünüzde sınavı ne kadar sürede çözdüğünüz, doğru, yanlış sayıları, puanınız, sorulara vermiş olduğunuz cevaplar, (daha sonra inceleyebilmeniz için) ve çözdüğünüz tarih bilgisi kaydedilmektedir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Yorum yazınca mutlaka yayınlanır mı ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Yazdığınız yorumlar yönetici onayının ardından yayınlanmaktadır. Bu süre en fazla 3 gündür.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Mesaj Gönder bölümünden gönderdiğim mesajlar sitede yayınlanıyor mu ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Mesaj gönder bölümünde görüş öneri ve sorularınızı gönderebilirsiniz. Buradan gönderilen mesajlar herhangi bir şekilde yayınlanmaz ancak sınav deneyiminizi daha iyileştirmek için bize bir yol gösterir.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Daha önce çözdüğüm sınavlara nasıl ulaşabilirim ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Çözdüğünüz Sınavlar</strong> kısmından ulaşabilirsiniz. Buradan <strong>Cevapları İncele</strong> bağlantısını tıklayarak cevaplarınızı görebilirsiniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Bir sınavda kaçıncı olduğumu nasıl öğrenebilirim ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Çözdüğünüz Sınavlar</strong> kısmından sınavları listeletebilir ve her sınava ilişkin bilgilerinizi görebilirsiniz. </p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Bir sınavda alınan ortalama puanı nasıl öğrenebilirim ve bu ortalama nasıl belirleniyor ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Çözdüğünüz Sınavlar</strong> kısmından sınavları listeletebilir ve her sınava ilişkin ortalama puanı görebilirsiniz.<br>
                            Ortalama puan hesaplanırken o sınavı çözenler içerisinde sınav sorularının <strong>en az %15'ine cevap verenlerin aldığı puanlar</strong> esas alınmaktadır.
                            </p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Hesabıma ilişkin bilgileri nasıl güncelleyebilirim ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Kişisel Ayarlarınız</strong> sayfasından güncelleyebilirisniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Hesabımı silmek için ne yapmalıyım ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Kişisel Ayarlarınız</strong> sayfasından <strong>Hesabımı Sil</strong> bağlantısını kullanabilirsiniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>İstatistiklerimi nasıl sıfırlarım ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>Üye girişi yaptıktan sonra sağ üst köşede bulunan resminize tıklayıp hesabınıza erişirsiniz. Sol tarafta bulunan <strong>Kişisel Ayarlarınız</strong> sayfasından <strong>İstatistiklerimi Sıfırla</strong> bağlantısını kullanabilirsiniz.</p>
                        </div>
                    </section>
                    <section class="toggle">
                        <label>Soruda hata varsa size nasıl bildirebilirim ?</label>
                        <div class="toggle-content" style="display: none;">
                            <p>
                                Aşağıdaki soru örneğinde göreceğiniz gibi <i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug"></i> simgesine tıklayarak açılan pencereye hatayı yazarak bize bildirebilirsiniz.
                                <br> Bu bildirimi yanlızca site üyeleri yapabilmektedir. Üye değilseniz yorum olarakta yazabilirsiniz.
                                <section id="6.34" class="call-to-action featured mb-5" style="border-top-color: #dc3545;"><h4 class="heading-primary"><strong>1. Soru: </strong><br> <span data-toggle="modal" data-target="#formModal"><i data-toggle="tooltip" data-placement="top" title="" data-original-title="Hata Bildirimi" class="fa fa-bug" style="float: left; margin-top: 3%;"></i></span></h4> <div class="col-sm-9 col-lg-9"><div class="call-to-action-content" style="text-align: left !important;"><span style="float: left;"><span style="font-size: small;"><strong>652 sayılı Millî Eğitim Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararname’ye göre atama ile ilgili aşağıda verilenlerden hangisi <ins><ins>yanlıştır</ins></ins>?</strong></span></span> </div></div></section>
                            </p>
                        </div>
                    </section>
                </div>
            </div>

                </div>
            </div>

        </div>

    </div>


    <?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- Vendor -->
    <script src="<?php echo e(asset('/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/popper/umd/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/common/common.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/isotope/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/vide/vide.min.js')); ?>"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo e(asset('/js/theme.js')); ?>"></script>

    <!-- Current Page Vendor and Views -->
    <script src="<?php echo e(asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')); ?>"></script>
    <script src="<?php echo e(asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')); ?>"></script>

    <!-- Theme Custom -->
    <script src="<?php echo e(asset('/js/custom.js')); ?>"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo e(asset('/js/theme.init.js')); ?>"></script>



    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-12345678-1', 'auto');
        ga('send', 'pageview');
    </script>
     -->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>