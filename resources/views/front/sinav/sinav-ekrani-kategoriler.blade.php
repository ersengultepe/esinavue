<div class="col-lg-12">
    <div class="toggle toggle-secondary" data-plugin-toggle="">
        <section class="toggle">
            <label>{{ $kategori->kategori_adi }}</label>
            <div class="toggle-content " style="display: none">

                <div class="col-lg-12">

                    <ol class="list list-ordened list-ordened-style-2">
                        @foreach($sinavlar->where('kategori_id', $kategori->id) as $sinav)
                            <li>
                                <a href="{{ route('sinav',[$sinav->link]) }}"
                                   data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="">
                                    @if($sinav->created_at <> null and \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($sinav->created_at)) < 10)
                                        <span class="badge badge-primary">Yeni </span>
                                    @endif &nbsp;{{ $sinav->adi }}
                                </a><span style="">{{ $sinav->cozum_sayisi }}
                                    kez çözüldü</span>
                            </li>
                        @endforeach
                    </ol>

                </div>

            </div>
        </section>
    </div>
</div>