@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
    <meta property="fb:app_id"          content="1234567890" />
    <meta property="og:type"            content="article" />
    <meta property="og:url"             content="http://newsblog.org/news/136756249803614" />
    <meta property="og:title"           content="Introducing our New Site" />
    <meta property="og:image"           content="https://scontent-sea1-1.xx.fbcdn.net/hphotos-xap1/t39.2178-6/851565_496755187057665_544240989_n.jpg" />
    <meta property="og:description"    content="http://samples.ogp.me/390580850990722" />
@stop

@section('css')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '242614719760687',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/rs-plugin/css/navigation.css') }}">
@stop

@section('content')
    @include('front.part.header')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active"><a href="{{ route('ekran') }}">Sınav Ekranı</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    {{--<div class="col">--}}
                    <div class="col-lg-1">
                        <img class="img-fluid rounded mb-4" src="{{ asset($sinavTip->logo) }}"
                             alt="{{ $sinavTip->sinav_tip }}">

                    </div>
                    <div class="col-lg-11">
                        <h1>{{ $sinavTip->sinav_tip }} </h1>
                    </div>
                    {{--</div>--}}
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
					<!-- 970x250 Heder Reklam -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:970px;height:250px"
						 data-ad-client="ca-pub-2211050103220921"
						 data-ad-slot="9398451499"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			
                @if($sinavKategorileri->where('sinav_format',3)->isEmpty() !== true)
                    <div class="col-lg-12">
                        <h2>GEÇMİŞ SINAVLAR & DENEME SINAVLARI</h2>
                        @foreach($sinavKategorileri->where('sinav_format',3) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                @endif

				<div class="col-md-8">

					<!-- 728x90 Büyük afiş -->
				<ins class="adsbygoogle"
					 style="display:inline-block;width:728px;height:90px"
					 data-ad-client="ca-pub-2211050103220921"
					 data-ad-slot="7836607398"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>

				</div>	
				
				<div class="col-md-3">

                    <!-- 336x280 Büyük Dikdörtgen -->
					<ins class="adsbygoogle"
						 style="display:inline-block;width:336px;height:280px"
						 data-ad-client="ca-pub-2211050103220921"
						 data-ad-slot="7261074082"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>

				</div>
				
                @if($sinavKategorileri->where('sinav_format', 1)->isEmpty() !== true )
                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ORTAK SINAVLAR</h2>
                        @foreach($sinavKategorileri->where('sinav_format', 1) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach

                        <br>
                        <hr class="tall">
                    </div>
                @endif
									
                @if($sinavKategorileri->where('sinav_format', 2)->isEmpty() !== true)

                    <div class="@if($sinavKategorileri->where('sinav_format', 3)->isEmpty()) col-lg-12 @else col-lg-6 @endif">
                        <h2>ALAN SINAVLARI</h2>

                        @foreach($sinavKategorileri->where('sinav_format', 2) as $kategori)
                            @include('front.sinav.sinav-ekrani-kategoriler')
                        @endforeach
                        <br>
                        <hr class="tall">
                    </div>
                    @endif
            </div>
        </div>

    </div>

@stop

@section('js')

    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('/vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.validation/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('/vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/vendor/vide/vide.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('/js/theme.js') }}"></script>

    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('/js/theme.init.js') }}"></script>

    <script>
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                href: window.location.href,
                image: ''
            }, function(response){});
        }
    </script>

@stop