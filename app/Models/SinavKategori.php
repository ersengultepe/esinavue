<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinavKategori extends Model
{
    protected $table = 'sinav_kategoris';

    protected $fillable = [
        'kategori_adi',
        'sinav_format',
        'soru_kategori_ids',
        'soru_adetleri',
        'durum',
        'ekleyen',
        'ip'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'ekleyen', 'id');
    }

    public function sinavlar()
    {
        return $this->hasMany(Sinav::class, 'kategori_id', 'id');
    }


}
