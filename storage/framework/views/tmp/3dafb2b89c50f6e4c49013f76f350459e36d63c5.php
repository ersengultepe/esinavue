<div class="row mt-5">
    <div class="col">
        <div class="heading heading-border heading-bottom-border">
            <h2 class="mb-2 word-rotator-title">

                <strong class="inverted">
                    <span class="word-rotator active" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                        <span class="word-rotator-items" style="width: 142.95px; transform: translate3d(0px, -126px, 0px); transition: transform 300ms ease 0s, width 300ms ease 0s;">
                            <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <span><?php echo $tip->sinav_tip; ?></span>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </span>
                    </span>
                </strong>

            </h2>
        </div>
        <div class="content-grid mt-4 mb-4">
            <?php $__currentLoopData = $sinavTipleri; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tip): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($loop->first): ?>
                <div class="row content-grid-row">
                <?php endif; ?>
                    <div class="content-grid-item col-lg-3 text-center">
                        <a href="<?php echo e(url('sinavlar/'.$tip->link)); ?>" target="_blank"><img class="img-fluid siyah-beyaz" src="<?php echo e(asset($tip->logo)); ?>" alt="<?php echo e($tip->sinav_tip); ?>"></a>
                    </div>
                <?php if($loop->iteration%4 === 0): ?>
                </div>
                <div class="row content-grid-row">
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
        </div>

        
        <hr class="tall">

    </div>
</div>