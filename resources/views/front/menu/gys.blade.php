<li class="dropdown"><a class="dropdown-item dropdown-toggle active"
                        href="#"> Görevde Yükselme & Unvan Değişikliği </a>
    <ul class="dropdown-menu">
        @foreach($sinavTipleri->whereIn('id', collect([4,6,9,12,14,15,16,17,20,22,24,25])) as $tip)
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ $tip->sinav_tip }}</a>
            <ul class="dropdown-menu" >
                <li class="dropdown-submenu"><a class="dropdown-item" href="#">Geçmiş & Deneme Sınavları</a>
                    <ul class="dropdown-menu">
                        @foreach($sinavKategoriListesi as $kat)
                            <li class="dropdown-submenu">
                                @if(str_contains($tip->sinav_kategori_ids, $kat->id) and $kat->sinav_format ==3)
                                    <a class="dropdown-item" href="#">
                                        {{ str_limit($kat->kategori_adi, 45) }}
                                    </a>
                                    <ul class="dropdown-menu"  style="max-height: 600px;overflow-y: auto;">
                                        @foreach($sinavlar as $snv)
                                            @if($snv->kategori_id == $kat->id)
                                                <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ str_limit($snv->adi,45) }}</a></li>
                                                @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="dropdown-submenu"><a class="dropdown-item" href="#">Alan Sınavları</a>
                    <ul class="dropdown-menu" >
                        @foreach($sinavKategoriListesi as $kat)
                            <li class="dropdown-submenu">
                                @if(str_contains($tip->sinav_kategori_ids, $kat->id) and $kat->sinav_format ==2)
                                    <a class="dropdown-item" href="#">
                                        {{ str_limit($kat->kategori_adi, 65) }}
                                    </a>
                                    <ul class="dropdown-menu"  style="max-height: 600px;overflow-y: auto;">
                                        @foreach($sinavlar as $snv)
                                            @if($snv->kategori_id == $kat->id)
                                                <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ str_limit($snv->adi,45) }}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="dropdown-submenu"><a class="dropdown-item" href="#">Ortak Konular</a>
                    <ul class="dropdown-menu" >
                        @foreach($sinavKategoriListesi as $kat)
                            <li class="dropdown-submenu">
                                @if(str_contains($tip->sinav_kategori_ids, $kat->id) and $kat->sinav_format ==1)
                                    <a class="dropdown-item" href="#">
                                        {{ str_limit($kat->kategori_adi, 65) }}
                                    </a>
                                    <ul class="dropdown-menu"  style="max-height: 600px;overflow-y: auto;">
                                        @foreach($sinavlar as $snv)
                                            @if($snv->kategori_id == $kat->id)
                                                <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ str_limit($snv->adi,45) }}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </li>

            </ul>


        </li>
        @endforeach

    </ul>
</li>