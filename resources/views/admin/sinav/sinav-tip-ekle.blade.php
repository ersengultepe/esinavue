@extends('admin.template')

@section('css')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
                    <h2 class="panel-title">
                        {{ breadCrumb() }}
                    </h2>
                </header>
                <div class="panel-body">
                    <div class="panel-body">
                        <form class="form-horizontal form-bordered" action="{{ route('setSinavTipEkle') }}" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputDefault">Sınav Tip</label>
                                <div class="col-md-6">
                                    <input name="sinav_tip" type="text" class="form-control sinav-tip-name-update" id="inputDefault" placeholder="Yeni Sınav Tipinin Adını Yaz">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru Kategorileri</label>
                                <div class="col-md-6">
                                    <select name="soru_kategori_ids[]" multiple data-plugin-selectTwo class="form-control populate sinav-tip-soru-kategori-update">
                                        @foreach($soruKategorileri as $soruKat)
                                            <option value="{{ $soruKat->id }}">{{ $soruKat->kategori_adi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Sınav Kategorileri</label>
                                <div class="col-md-6">
                                    <select name="sinav_kategori_ids[]" multiple data-plugin-selectTwo class="form-control populate sinav-tip-sinav-kategori-update">
                                        @foreach($sinavKategorileri as $sinavKat)
                                            <option value="{{ $sinavKat->id }}">{{ $sinavKat->kategori_adi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br><br>
                            <br><br>
                            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-block">Kaydet</button>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/forms/examples.advanced.form.js') }}"></script>
@stop
