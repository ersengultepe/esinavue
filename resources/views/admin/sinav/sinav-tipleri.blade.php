@extends('admin.template')

@section('content')
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                    <a href="{{ route('getSinavKategorileri') }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                        Geri Dön </a>
                </button>

                <button type="button" class="mb-xs mt-xs mr-xs btn btn-success">
                    <a href="{{ route('getSinavTipEkle') }}" style="color: white;text-decoration-line: none">  <i class="fa fa-save"></i>
                        Yeni Sınav Tip Ekle </a>
                </button>
                <span>{{ str_limit(ucWordsTr(mb_strtolower(Session::get('breadcrumb'), 'UTF-8'))) }} </span>
                <small>Toplam {{ $sinavTips->total() }} Sınav Tipi</small>

            </h2>
        </header>
        {{ $sinavTips->links() }}
        <div class="panel-body">
            <div class="tm-box appear-animation fadeInRight appear-animation-visible" data-appear-animation="fadeInRight">
                <div class="thumbnail-gallery">

                    @foreach($sinavTips as $tip)
                        <a class="img-thumbnail" href="{{ route('getSinavTipPage', $tip->id) }}">
                            <img src="{{ asset($tip->logo) }}" width="150">
                            <span class="zoom">
								<i class="fa fa-search"></i>
                            </span>
                        </a>
                        @endforeach

                </div>

            </div>
        </div>
        {{ $sinavTips->links() }}
    </section>
@stop

