@extends('admin.template')

@section('css')
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/dropzone/css/basic.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/dropzone/css/dropzone.css') }}" />
@stop

@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title tip" id="{{ $sinavTipi->id }} ">
                        <a class="img-thumbnail modal-basic" href="#modalCenterIcon">
                            <img id="tip-image" src="{{ asset($sinavTipi->logo) }}" width="80" alt/>
                        </a>
                        {{ breadCrumb() }} <br>
                        <small>{{ $sinavTipi->sinav_tip.'\'e Bağlı ' }}Soru ve Sınav Kategorileri</small>
                    </h2>

                </header>

                <div class="panel-body">
                    <!-- Modal Center Icon -->
                    <div id="modalCenterIcon" class="modal-block modal-block-primary mfp-hide">
                        <section class="panel">
                            <div class="panel-body text-center">
                                <div class="modal-wrapper">

                                    <div class="modal-text">
                                        <div class="col-sm-12">
                                            <form action="{{ route('dropzoneTip', ['id'=>$sinavTipi->id]) }}" class="dropzone dz-square dz-clickable" id="dropzone-example">
                                                <div class="dz-default dz-message">
                                                    <span>Sinav Tip Foto Yükle</span>
                                                    <span style="display: none" id="dropzone-result"></span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <footer class="panel-footer">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-primary modal-confirm">Kapat</button>
                                        <button class="btn btn-default modal-dismiss">İptal</button>
                                    </div>
                                </div>
                            </footer>
                        </section>
                    </div>


                    <form class="form-horizontal form-bordered" action="#">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="inputDefault">Sınav Tip</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control sinav-tip-name-update" id="inputDefault" value="{{ $sinavTipi->sinav_tip }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Soru Kategorileri</label>
                                <div class="col-md-6">
                                    <select multiple data-plugin-selectTwo class="form-control populate sinav-tip-soru-kategori-update">
                                        @foreach($soruKategorileri as $soruKat)
                                            <option value="{{ $soruKat->id }}" @if($selectedSoruKats->contains($soruKat->id) ) selected @endif>{{ $soruKat->kategori_adi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Sınav Kategorileri</label>
                                <div class="col-md-6">
                                    <select multiple data-plugin-selectTwo class="form-control populate sinav-tip-sinav-kategori-update">
                                        @foreach($sinavKategorileri as $sinavKat)
                                            <option value="{{ $sinavKat->id }}" @if($selectedSinavKats->contains($sinavKat->id) ) selected @endif>{{ $sinavKat->kategori_adi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                </div>
            </section>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/dropzone/dropzone.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.modals.js') }}"></script>
    <script>
        $('.sinav-tip-soru-kategori-update').change(function () {
            let sinav_tip_id = $('.tip').attr('id');
            let soru_kategori_ids = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav-tip/soru-kategori/update',
                data:{
                    'sinav_tip_id': sinav_tip_id,
                    'soru_kategori_ids': soru_kategori_ids,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Soru kategorileri güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.sinav-tip-sinav-kategori-update').change(function () {
            let sinav_tip_id = $('.tip').attr('id');
            let sinav_kategori_ids = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav-tip/sinav-kategori/update',
                data:{
                    'sinav_tip_id': sinav_tip_id,
                    'sinav_kategori_ids': sinav_kategori_ids,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav kategorileri güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $('.sinav-tip-name-update').change(function () {
            let sinav_tip_id = $('.tip').attr('id');
            let name = $(this).val();

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav-tip/name/update',
                data:{
                    'sinav_tip_id': sinav_tip_id,
                    'name': name,
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });
            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav Tip adı güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });

        $(document).on('click', '.modal-confirm', function (e) {
            e.preventDefault();
            $.magnificPopup.close();

            let uploadedImagePath = $('#dropzone-result').html();

            if(uploadedImagePath.length > 3){
                $('#tip-image').attr('src', uploadedImagePath);
                new PNotify({
                    title: 'Başarılı!',
                    text: 'Sınav Tip Logosu Değiştirildi..!',
                    type: 'success'
                });
            }
        });
    </script>
@stop
