<?php

namespace App\Http\Controllers;

use App\Models\SoruKaynak;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SoruKaynakController extends Controller
{
    public function getSoruKaynakListele(){
        $soruKaynaklari = SoruKaynak::all();
        return view('admin.soru.soru-kaynaklari', compact('soruKaynaklari'));
    }

    public function getSoruKaynakEkle(){
        return view('admin.soru.soru-kaynak-ekle');
    }

    protected function soruKaynakInsert(Request $request){
        $dt= Carbon::parse($request->tarih);
        $soruKaynak = new SoruKaynak();
        $soruKaynak->ad = $request->ad;
        $soruKaynak->telif = $request->telif;
        $soruKaynak->tarih = $dt->year.'-'.$dt->month.'-'.$dt->day;
        $soruKaynak->duzenleyen_kurum = $request->duzenleyen_kurum;
        $soruKaynak->duzenlenen_kurum = $request->duzenlenen_kurum;
        $soruKaynak->soru_sayisi = $request->soru_sayisi;
        $soruKaynak->user_id = Auth::id();
        $soruKaynak->ip = $request->ip();
        $soruKaynak->saveOrFail();
        return true;
    }

    public function setSoruKaynakEkle(Request $request){
        try{
            $this->soruKaynakInsert($request);
            Log::info('Yeni Sınav Kaynak Eklendi', ['Başarılı', $request->ad.' sınav kaynağı eklendi.']);
            Session::flash('sinav_kaynak_success', 'Yeni Bir Sınav Kaynağı Eklendi');
            return Redirect::route('getSoruEkle');

        }catch (Exception $e ){
            Log::error('Sınav Kaynak Ekleme Hatası', ['Hata', $e->getMessage().'-----'.$e->getLine()]);
//            Session::flash('sinav_kaynak_error', "Yeni Bir Sınav Kaynağı Eklerken Hata İle Karşılaşıldı <br>".$e->getMessage().'-----'.$e->getLine());
//            return redirect()->route('getSoruKaynak');
            return ['Hata', $e->getMessage().'-----'.$e->getLine()];
        }
    }

    public function ajaxSoruKaynakUpdate(Request $request){
        try{
            $dt= Carbon::parse($request->tarih);
            SoruKaynak::where('id',$request->id)->update([
                'ad' => $request->ad,
                'telif' => $request->telif,
                'tarih' => $dt->year.'-'.$dt->month.'-'.$dt->day,
                'duzenleyen_kurum' => $request->duzenleyen_kurum,
                'duzenlenen_kurum' => $request->duzenlenen_kurum,
                'soru_sayisi' => $request->soru_sayisi,
                'user_id' => Auth::id(),
                'ip' => $request->ip()
            ]);

            Log::info('Yeni Sınav Kaynak Eklendi', ['Başarılı', $request->ad.' sınav kaynağı eklendi.']);

            $notification = array(
                'title' => 'Başarılı',
                'text' => 'Soru Kaynağı Başarıyla Güncellendi..!'
            );
            Session::put($notification);
            return $notification;

        }catch (\Exception $e){
            Log::error('Sınav Kaynak Güncelleme Hatası', ['Hata', $e->getMessage().'-----'.$e->getLine()]);
            return response('Kayıt Güncellemesi Sırasında Hata Oluştu');
        }
    }

    public function ajaxSoruKaynakDelete(Request $request){
        $soruKaynak = SoruKaynak::find($request->id);
        $soruKaynak->delete();

        $notification = array(
            'title' => 'Başarılı',
            'text' => 'Soru Kaynağı Başarıyla Silindi..!'
        );
        Session::put($notification);
        return $notification;
    }
}
