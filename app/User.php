<?php

namespace App;

use App\Models\Sehir;
use App\Notifications\ResetPasswordNotification;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','yetki', 'email', 'ad','soyad','password','md5_password', 'orginal_password',
        'kullanici', 'foto', 'cinsiyet', 'dogum_tarihi', 'sehir', 'ilce', 'takim','tahsil',
        'meslek', 'kendi_hakkinda','motto', 'websitesi', 'sinav_tip', 'sinav_arsivi', 'fb_id',
        'tw_id','gp_id','ebulten','durum', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function city()
    {
        return $this->belongsTo(Sehir::class, 'sehir', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getDogumTarihiFFAttribute(){
        $dt = new Carbon($this->dogum_tarihi);
        return $dt->formatLocalized('%d %m %Y');
    }



}
