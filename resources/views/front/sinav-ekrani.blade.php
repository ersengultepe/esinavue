@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
@stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-shop.css') }}">
@stop

@section('content')
    @include('front.part.header')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active">Sınav Ekranı </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Sınav Ekranı</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">

                <div class="col-lg-12">

                    <hr class="d-lg-none tall">
					<div>

						<!-- 970x250 Heder Reklam -->
						<ins class="adsbygoogle"
							 style="display:inline-block;width:970px;height:250px"
							 data-ad-client="ca-pub-2211050103220921"
							 data-ad-slot="9398451499"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>

					</div>
                    <div class="toggle toggle-primary" data-plugin-toggle="" data-plugin-options="{ 'isAccordion': true }">
                        <section class="toggle active">
                            <label>MEB ONLINE KAZANIM  TESTLERİ</label>
                            <div class="toggle-content" style="display: block;">
                                <div class="row mt-lg-12">
                                    @foreach($sinavTipleri as $tip)
                                        @if($tip->id === 27)
                                                <div class="col-lg-2">
                                            <a href="{{ route('sinavlar',[$tip->link]) }}">

                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $tip->sinav_tip }}">
													<img class="img-fluid" src="{{ asset($tip->logo) }}" alt="{{ $tip->sinav_tip }}">
											</span>

                                            </a>
                                            </div>
                                            @endif
                                        @endforeach
                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>GÖREVDE YÜKSELME VE UNVAN DEĞİŞİKLİĞİ SINAVLARI</label>
                            <div class="toggle-content" style="display: block;">
                                <div class="row mt-lg-12">
                                    <!-- 728x90 Büyük afiş -->
                                    <ins class="adsbygoogle"
                                         style="display:inline-block;width:728px;height:90px"
                                         data-ad-client="ca-pub-2211050103220921"
                                         data-ad-slot="7836607398"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                    @foreach($sinavTipleri as $tip)
                                        @if(in_array($tip->id, [13,18,19,21,23]) === false)
                                            <div class="col-lg-2">
                                                <a href="{{ route('sinavlar',[$tip->link]) }}">

                                            <span class="img-thumbnail d-block" data-toggle="tooltip"
                                                  data-placement="top" title=""
                                                  data-original-title="{{ $tip->sinav_tip }}">
													<img class="img-fluid" src="{{ asset($tip->logo) }}"
                                                         alt="{{ $tip->sinav_tip }}">
											</span>

                                                </a>
                                            </div>
                                            @endif
                                        @endforeach
                                    <!-- 728x90 Büyük afiş -->
                                        <ins class="adsbygoogle"
                                             style="display:inline-block;width:728px;height:90px"
                                             data-ad-client="ca-pub-2211050103220921"
                                             data-ad-slot="7836607398"></ins>
                                        <script>
                                            (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>
                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>ÖSYM SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                @foreach($sinavTipleri as $tip)
                                    @if($tip->id ==18 || $tip->id==19)
                                        <div class="col-lg-2">

                                            <a href="{{ route('sinavlar',[$tip->link]) }}">
                                                <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $tip->sinav_tip }}">
												<img class="img-fluid" src="{{ asset($tip->logo) }}" alt="{{ $tip->sinav_tip }}">
											</span>

                                            </a>
                                        </div>
                                    @endif
                                @endforeach


                                    <!-- kara_250x250px -->
									<ins class="adsbygoogle"
										 style="display:inline-block;width:250px;height:250px"
										 data-ad-client="ca-pub-2211050103220921"
										 data-ad-slot="7383760690"></ins>
									<script>
									(adsbygoogle = window.adsbygoogle || []).push({});
									</script>

                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>ÖZEL GÜVENLİK SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                    @foreach($sinavTipleri as $tip)
                                        @if($tip->id == 13)
                                            <div class="col-lg-2">
                                            <a href="{{ route('sinavlar',[$tip->link]) }}">
                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $tip->sinav_tip }}">
												<img class="img-fluid" src="{{ asset($tip->logo) }}" alt="{{ $tip->sinav_tip }}">
											</span>

                                            </a>
                                            </div>
                                        @endif
                                    @endforeach

                                    <!-- 728x90 Büyük afiş -->
                                        <ins class="adsbygoogle"
                                             style="display:inline-block;width:728px;height:90px"
                                             data-ad-client="ca-pub-2211050103220921"
                                             data-ad-slot="7836607398"></ins>
                                        <script>
                                            (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>

                                </div>
                            </div>
                        </section>
                        <section class="toggle active">
                            <label>EHLİYET SINAVLARI</label>
                            <div class="toggle-content" style="display: none;">
                                <div class="row mt-lg-12">
                                    @foreach($sinavTipleri as $tip)
                                        @if($tip->id == 23)
                                            <div class="col-lg-2">
                                            <a href="{{ route('sinavlar',[$tip->link]) }}">
                                            <span class="img-thumbnail d-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{ $tip->sinav_tip }}">
												<img class="img-fluid" src="{{ asset($tip->logo) }}" alt="{{ $tip->sinav_tip }}">
											</span>

                                            </a>
                                            </div>
                                        @endif
                                    @endforeach

                                    <!-- 728x90 Büyük afiş -->
                                        <ins class="adsbygoogle"
                                             style="display:inline-block;width:728px;height:90px"
                                             data-ad-client="ca-pub-2211050103220921"
                                             data-ad-slot="7836607398"></ins>
                                        <script>
                                            (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <!-- Vendor -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
{{--    <script src="{{ asset('/vendor/jquery.appear/jquery.appear.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery.easing/jquery.easing.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/popper/umd/popper.min.js') }}"></script>--}}
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
{{--    <script src="{{ asset('/vendor/common/common.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery.validation/jquery.validation.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/isotope/jquery.isotope.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>--}}
{{--    <script src="{{ asset('/vendor/vide/vide.min.js') }}"></script>--}}

    <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('/js/theme.js') }}"></script>

    <!-- Theme Custom -->
        <script src="{{ asset('/js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
        <script src="{{ asset('/js/theme.init.js') }}"></script>

@stop