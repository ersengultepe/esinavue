<?php

namespace App\Http\Controllers;

use App\User;
use Laravel\Socialite\Contracts\Provider;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class SocialAuthController extends Controller
{

    public function callback($provider)
    {

        $user = $this->createOrGetUser(Socialite::driver($provider));

        auth()->login($user);
        //Login sonrasında kaldığı yerden devam etsin.
        return Session::has('login-after-url') ? redirect()->to(Session::get('login-after-url')) : redirect()->to('/');
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function getSocialColumn($providerName)
    {
        $social = '';
        Switch ($providerName)
        {
            case 'FacebookProvider' : $social = 'fb_id'; break;
            case 'TwitterProvider' : $social = 'tw_id'; break;
            case 'GoogleProvider' : $social = 'gp_id'; break;
            case 'github' : $social = 'git_id'; break;
            case 'bitbucket' : $social = 'bb_id'; break;
            case 'linkedin' : $social = 'lin_id'; break;
        }
        return $social;
    }

    private function createOrGetUser(Provider $provider)
    {
        $providerUser = $provider->user();
        $providerName = class_basename($provider);
        $socialColumn = $this->getSocialColumn($providerName);

        //Bu sosyal medya hesabından email elde edememişsek socialmediaId ile sorgula daha önce giriş yapmış mı?
        if($providerUser->getEmail() === null)
        {
            $user = User::where($socialColumn, $providerUser->getId())->where('durum', '<>', 3)->first();

            //eğer bu sosyal medya hesabı ilk kez giriş yapıyorsa yeni üye oluştur.
            if($user === null)
            {  //Oluşturulan user da email boş!
                $user = $this->insertSocialMediaUser($providerUser, $providerName, $socialColumn);
            }

            $this->updateSocialMediaID($user, $providerUser, $socialColumn);
            return $user;
        }

        //Önce bakalım, bu mail adresi ile kayıt olan var mı ?
        $user = User::whereEmail($providerUser->getEmail())->where('durum', '<>', 3)->where('id','>',0)->first();

        //bu sosyal medya hesabının email adresi kayıtlı değilse id ile bak
        if($user === null)
        {
            $user = User::where($socialColumn, $providerUser->getId())->where('durum', '<>', 3)->first();
            if($user === null)
            {
               $user = $this->insertSocialMediaUser($providerUser, $providerName, $socialColumn);
            }

            $this->updateSocialMediaID($user, $providerUser, $socialColumn);
        }

        $this->updateSocialMediaID($user, $providerUser, $socialColumn);

        return $user;
    }

    /**
     * @param User $user
     * @param $providerUser
     * @param $socialColumn
     * @return bool
     */
    private function updateSocialMediaID(User $user, $providerUser, $socialColumn){
        $user->{$socialColumn} = $providerUser->getId();
        $user->durum = 1;
        $user->save();
        return true;
    }

    /**
     * @param User $user
     * @param $providerUser
     * @return bool
     */
    private function updateSocialMediaEmail(User $user, $providerUser){
        $user->durum = 1;
        $user->email = $providerUser->getEmail();
        $user->save(); Log::info('socialmedia', ['sdadsad']);
        return true;
    }

    /**
     * @param $providerUser
     * @param $providerName
     * @param $socialColumn
     * @return mixed
     */
    private function insertSocialMediaUser($providerUser, $providerName, $socialColumn){
        $user = User::create([
            'email' => $providerUser->getEmail(),
            'ad' => $providerUser->getNickname(),
            'kullanici' => $providerUser->getName(),
            $socialColumn => $providerUser->getId(),
            'yetki' => 2,
            'foto' => $providerName === 'GoogleProvider'? str_replace('sz=50','sz=200',$providerUser->getAvatar()) : $providerUser->getAvatar(),
            'ebulten' => 'E',
            'durum' => 1
        ]);
        return $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
    }
}
