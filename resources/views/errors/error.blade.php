@extends('front.template')

@section('title')
    <title>{{ $errorMessage.' | '.config('app.name') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="{{ $errorMessage }}">
@stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('/css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/theme-blog.css') }}">
@stop

@section('content')
     @include('front.part.header')
    <div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col">
								<ul class="breadcrumb">
									<li><a href="{{ url('/') }}">Anasayfa</a></li>
									<li class="active">{{ $errorMessage }}</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<h1>{{ $errorMessage }}</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<section class="page-not-found">
						<div class="row justify-content-center">
							<div class="col-lg-7 text-center">
								<div class="page-not-found-main">
									<span style="font-size: 4em;color: lightcoral;text-transform: uppercase;">{{ $errorMessage }} <i class="fas fa-times"></i></span>

									<p style="margin-top: 25px ">{!! $errorExplain !!}</p>
								</div>
							</div>
							<div class="col-lg-4">
								<h4 class="heading-primary">Bunlar ilgilinizi çekebilir</h4>
								<ul class="nav nav-list flex-column">
									<li class="nav-item"><a class="nav-link" href="#">Anasayfa</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Sınav Ekranı</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Sık Sorulan Sorular</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Site Haritası</a></li>
									<li class="nav-item"><a class="nav-link" href="#">İletişim</a></li>
								</ul>
							</div>
						</div>
					</section>

				</div>

			</div>
@stop