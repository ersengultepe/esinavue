<?php

namespace App\Http\Controllers;

use App\Models\Cevap;
use App\Models\Sinav;
use App\Models\SinavDetay;
use App\Models\SoruKaynak;
use App\Models\Soru;
use App\Models\SoruHatalari;
use App\Models\SoruKategori;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


class SoruController extends Controller
{
    //todo Soru Ekleme Modülünde Cach i temizlemeyi unutma..!
    public function getSoruSayisi()
    {
        return Cache::rememberForever('soruSayisi', function () {
            return collect(DB::select('select COUNT(id) sayi from sorus where durum=1'))->first();
        });
    }

    public function getCozulenSoruSayisi()
    {
        return Cache::remember('cozulenSoruSayisi', 60, function () {
            return collect(DB::select('select (sum(dogru)+sum(yanlis)) sayi from uyeler_detays where durum=1 limit 1'))->first();
        });
    }

    public function getSoruEkle()
    {
        $soruKategorileri = SoruKategori::orderBy('kategori_adi', 'ASC')->get();
        $soruKaynaklari = SoruKaynak::orderBy('ad', 'ASC')->get();
        Session::put('breadcrumb','Soru Ekle');
        return view('admin.soru.soru-ekle', compact('soruKategorileri', 'soruKaynaklari'));
    }

    private function dogrularEslestir($dogrular, $cevapSikki, $durum=null){
        //durum 1 ise görüntüleme sırasında, değilse insert sırasında

        if (trim($dogrular) == $cevapSikki){
            return $durum == 1 ? '$&&$=> ' : 1;
        }else{
            return $durum == 1 ? null : 0;
        }
    }

    public function setSoruEkle(Request $request)
    {
        try{
            $sinav_mi       = $request->sinav_mi;
            $e_varmi        = $request->e_varmi;
            $soru_bicimi    = $request->soru_bicimi;
            $cevap_bicimi   = $request->cevap_bicimi;
            $soru_kategori  = $request->soru_kategori;
            $metin          = $request->metin;
            $metin          = $metin."<br>\n 101. ";
            $dogru_cevaplar = $request->dogru_cevaplar; //cevap anahtarı

            $Acevap='';
            $Bcevap='';
            $Ccevap='';
            $Dcevap='';
            $Ecevap='';

            if ($cevap_bicimi == 'parantez') {
                $Acevap = '/A\)(.*?)B\)/s';
                $Bcevap = '/B\)(.*?)C\)/s';
                $Ccevap = '/C\)(.*?)D\)/s';
                if ($e_varmi == 0) {
                    $Dcevap = '/D\)(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/D\)(.*?)E\)/s';
                    $Ecevap = '/E\)(.*?)\n\d+\./s';
                }
            } else if ($cevap_bicimi == 'tire') {
                $Acevap = '/A\-(.*?)B\-/s';
                $Bcevap = '/B\-(.*?)C\-/s';
                $Ccevap = '/C\-(.*?)D\-/s';
                if ($e_varmi == 0) {
                    $Dcevap = '/D\-(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/D\-(.*?)E\-/s';
                    $Ecevap = '/E\-(.*?)\n\d+\./s';
                }
            } else if ($cevap_bicimi == 'nokta') {
                $Acevap = '/A\.(.*?)B\./s';
                $Bcevap = '/B\.(.*?)C\./s';
                $Ccevap = '/C\.(.*?)D\./s';
                if ($e_varmi == 0) {
                    $Dcevap = '/D\.(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/D\.(.*?)E\./s';
                    $Ecevap = '/E\.(.*?)\n\d+\./s';
                }
            } else if ($cevap_bicimi == 'parantezK') {
                $Acevap = '/a\)(.*?)b\)/s';
                $Bcevap = '/b\)(.*?)c\)/s';
                $Ccevap = '/c\)(.*?)d\)/s';
                if ($e_varmi == 0) {
                    $Dcevap = '/d\)(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/d\)(.*?)e\)/s';
                    $Ecevap = '/e\)(.*?)\n\d+\./s';
                }
            } else if ($cevap_bicimi == 'tireK') {
                $Acevap = '/a\-(.*?)b\-/s';
                $Bcevap = '/b\-(.*?)c\-/s';
                $Ccevap = '/c\-(.*?)d\-/s';
                if ($e_varmi == 0) {
                    $Dcevap = '/d\-(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/d\-(.*?)e\-/s';
                    $Ecevap = '/e\-(.*?)\n\d+\./s';
                }
            } else if ($cevap_bicimi == 'noktaK') {
                $Acevap = '/a\.(.*?)b\./s';
                $Bcevap = '/b\.(.*?)c\./s';
                $Ccevap = '/c\.(.*?)d\./s';
                if ($e_varmi == 0) {
                    $Dcevap = '/d\.(.*?)\n\d+\./s';
                } else {
                    $Dcevap = '/d\.(.*?)e\./s';
                    $Ecevap = '/e\.(.*?)\n\d+\./s';
                }
            }

            if($e_varmi == 0){
                $soru_deseni = $soru_bicimi.substr($Dcevap,1,8).'\n/sm';
            }else{
                $soru_deseni = $soru_bicimi.substr($Ecevap,1,8).'\n/sm';
                //E ve D şıkları birden fazla satır içerdiğinde tamamını mevcut sistemde alamadığımızdan bu iki şıkka ayrı desen uyguluyorum.
                preg_match_all($Ecevap,$metin,$cevapE);
            }

            //soruların doğru cevap şıkları alındı ve $dogrular[1][$i] = B formatında elde ediliyor.
            preg_match_all('/\d+\.(.*?)$/m',$dogru_cevaplar,$dogrular);

            preg_match_all ($soru_deseni, $metin, $soruVeCevaplar);

            $sorular  = [];
            $cevaplar = [];

            $soru_deseni2 = $soru_bicimi.substr($Acevap,1,8).'\n/sm';

            for($i=0; $i<count($soruVeCevaplar[0]); $i++ ){

                preg_match_all ($soru_deseni2, $soruVeCevaplar[0][$i], $soru);

                $sorular[$i] = replaceSpace(trim($soru[1][0]));
                preg_match_all ($Acevap, $soruVeCevaplar[0][$i], $cevapA);
                preg_match_all ($Bcevap, $soruVeCevaplar[0][$i], $cevapB);
                preg_match_all ($Ccevap, $soruVeCevaplar[0][$i], $cevapC);
                preg_match_all($Dcevap,$metin,$cevapD);

                $cevaplar[$i][0] = self::dogrularEslestir($dogrular[1][$i],'A',1).replaceSpace(trim($cevapA[1][0])); //A)
                $cevaplar[$i][1] = self::dogrularEslestir($dogrular[1][$i],'B',1).replaceSpace(trim($cevapB[1][0])); //B)
                $cevaplar[$i][2] = self::dogrularEslestir($dogrular[1][$i],'C',1).replaceSpace(trim($cevapC[1][0])); //C)
                $cevaplar[$i][3] = self::dogrularEslestir($dogrular[1][$i],'D',1).replaceSpace(trim($cevapD[1][$i])); //D)

                if(isset($Ecevap) && !empty($Ecevap) ){
                    if(!isset($cevapE[1][$i]))
                        continue;
                    $cevaplar[$i][4] = self::dogrularEslestir($dogrular[1][$i],'E',1).replaceSpace(trim($cevapE[1][$i]));//E)
                }

            }

            Session::put('breadcrumb', 'Eklenen Sorular Listeleniyor');
            return view('admin.soru.soru-ekle-listele')->with([
                'sorular' => $sorular,
                'cevaplar' => $cevaplar,
                'sinav_mi' => $sinav_mi,
                'soru_kategori' => $soru_kategori,
                'dogrular' => $dogrular,
                'soru_kaynak_id' => $request->soru_kaynak_id
            ]);

        }catch (\Exception $e){
            return $e->getLine().' '.$e->getMessage().' '.$e->getCode().' '.$e->getFile();
        }
    }

    public function insertSoruEkle(Request $request){
        try{
            if($request->sinav_mi == 1){ //Evet sınav olarak kaydet bu soruları ise
                $ekle = new Sinav();
                $ekle->adi          =  $request->soru_kategori;
                $ekle->link         =  str_slug($request->soru_kategori);
                $ekle->durum        =  0;
                $ekle->ekleyen      =  Auth::id();
                $ekle->ip           =  $request->ip();
                $ekle->kategori_id  =  0;
                $ekle->save();
                $sinavId = $ekle->id;

            }

            for ($i=0; $i<count($request->soru); $i++){
                $soruEkle = new Soru();
                $soruEkle->soru             = turkceAsci($request->soru[$i]);
                $soruEkle->soru_kaynak_id   = $request->soru_kaynak_id;
                $soruEkle->soru_kategori_id = $request->soru_kategori;
                $soruEkle->ekleyen          = Auth::id();
                $soruEkle->ip               = $request->ip();
                $soruEkle->durum            = 1;
                $soruEkle->save();
                $soruId = $soruEkle->id;

                if($request->sinav_mi == 1) { //Evet sınav olarak kaydet bu soruları ise
                    /**
                     * Sınav Eklendi, ardından her soruyu bu sınava bağlıyoruz.
                     */
                    $sinavDetayaEkle = new SinavDetay();
                    $sinavDetayaEkle->sinav_id = $sinavId;
                    $sinavDetayaEkle->soru_id = $soruId;
                    $sinavDetayaEkle->ekleyen = Auth::id();
                    $sinavDetayaEkle->ip = $request->ip();
                    $sinavDetayaEkle->save();
                }

                for($j=0; $j<count($request->cevap[$i]); $j++){
                    $cevapEkle = new Cevap();
                    $cevapEkle->cevap       = turkceAsci(str_replace('$&&$=> ','',$request->cevap[$i][$j])); //Doğru cevaplara yerleştirdiğimiz stringler temizlendi.
                    $cevapEkle->soru_id     = $soruId;
                    $cevapEkle->ekleyen     = Auth::id();
                    $cevapEkle->ip          = $request->ip();
                    $cevapEkle->durum       = 1;
                    $cevapEkle->dogru       = str_contains($request->cevap[$i][$j],'$&&$=> ') == true ? 1 : 0; //Cevapta '$&&$=> ' varsa doğrudur, DB ye 1 olarak kaydedilir.;
                    $cevapEkle->save();
                }
            }
            return redirect()->route('getKategoriWithSorular', ['id'=>$request->soru_kategori]);
        }catch (\Exception $e){
            return $e->getFile().' '.$e->getMessage().' '.$e->getLine().' '.$e->getCode();
        }
    }

    private function soruKategoriConvertSinavGenerate(SoruKategori $soruKat)
    {
        $sinav= new Sinav();
        $sinav->adi = $soruKat->kategori_adi;
        $sinav->link = str_slug($soruKat->kategori_adi);
        $sinav->kategori_id = 0;
        $sinav->durum = 1;
        $sinav->ekleyen = 1;
        $sinav->ip = request()->ip();
        $sinav->saveOrFail();

        $soruIds = Soru::where('soru_kategori_id', $soruKat->id)->whereDurum(1)->get();

        $sinavDetaysArray = [];
        foreach ($soruIds->pluck('id') as $soruId)
        {
            array_push($sinavDetaysArray, ['sinav_id' => $sinav->id, 'soru_id'=>$soruId, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now(), 'ip'=> request()->ip()]);
        }

        $sinavDetay = new SinavDetay();
        $sinavDetay->insert($sinavDetaysArray);

        Log::notice('Sınav Eklendi', ['Toplam '.count($sinavDetaysArray).' soruya sahip'.$sinav->adi.' isimli sınav oluşturuldu' ]);
        return true;
    }

    public function indexKokpit()
    {
        Session::put('breadcrumb', 'Esinavsalonu Kokpit Mahalli');
        $uyeSayisi = User::where('durum',1)->count();
        $soruSayisi = Soru::where('durum',1)->count();
        $sinavSayisi = Sinav::where('durum',1)->count();
//        $yorumSayisi = Yorum::where('onay',1)->count();

        return view('admin.google-analytics', compact(
            'uyeSayisi',
            'soruSayisi',
            'sinavSayisi'
//            'yorumSayisi'
            ));
    }

    public function getSoruKategorileri(){
        $soruKategorileri = SoruKategori::orderBy('kategori_adi','ASC')->paginate(50);
        Session::put('breadcrumb', 'Kategori Bazında Sorular');
        return view('admin.soru.soru-kategorileri', compact('soruKategorileri'));
    }

    public function getKategoriWithSorular($id)
    {
        $sorular = Soru::where('soru_kategori_id', $id)->where('durum', 1)->paginate(50);
        $soruKategorileri = SoruKategori::all();
        $soruKategori = $soruKategorileri->where('id', $id)->first();
        Session::put('breadcrumb', $soruKategori->kategori_adi.' Soruları');
        return view('admin.soru.soru-kategori-sorulari', compact('sorular', 'soruKategorileri', 'soruKategori'));
    }

    public function getOnaysizSorular()
    {
        $sorular = Soru::where('durum', 0)->paginate(50);
        $soruKategorileri = SoruKategori::all();
        Session::put('breadcrumb', 'Onaysız Sorular');
        return view('admin.soru.soru-onaysizlar', compact('sorular', 'soruKategorileri'));
    }

    public function getKategorisizSorular()
    {
        $sorular = Soru::where('soru_kategori_id', 0)->orWhere('soru_kategori_id', null)->paginate(50);
        $soruKategorileri = SoruKategori::all();
        Session::put('breadcrumb', 'Kategorisiz Sorular');
        return view('admin.soru.soru-kategorisiz-sorular', compact('sorular', 'soruKategorileri'));
    }

    public function getCevapsizSorular()
    {
        /*-- cevap sayısı 4 ten az olanlar
        SELECT COUNT(soru_id) c, soru_id FROM cevaps GROUP BY soru_id HAVING c <4

        -- cevaplarında doğru şık seçili olmayanlar
        SELECT s.id FROM sorus s WHERE NOT EXISTS (SELECT c.soru_id FROM cevaps c WHERE c.soru_id=s.id and c.dogru=1 GROUP BY c.soru_id )

        -- sorusu olup cevapları olmayanlar
        SELECT s.id FROM sorus s WHERE NOT EXISTS (SELECT c.soru_id FROM cevaps c WHERE c.soru_id=s.id )

        -- cevabı olup sorusu olmayanlar
        SELECT soru_id FROM cevaps c WHERE NOT EXISTS (SELECT id FROM sorus s WHERE c.soru_id=s.id ) GROUP BY soru_id*/

        //Soru var ancak ona bağlı 4'ten az cevap şıkkı olanlar VE cevaplarında doğru şık seçili olmayanlar
         $soruIds = DB::select("SELECT COUNT(soru_id) c, soru_id FROM cevaps GROUP BY soru_id HAVING c <4
                                 UNION
                                 SELECT '' AS c, s.id AS soru_id FROM sorus s WHERE 
                                 NOT EXISTS (SELECT c.soru_id FROM cevaps c WHERE c.soru_id=s.id and c.dogru=1 GROUP BY c.soru_id )");

        $sorular = Soru::whereIn('id', collect($soruIds)->pluck('soru_id'))->paginate(50);
        $soruKategorileri = SoruKategori::all();
        Session::put('breadcrumb', 'Cevapsız Sorular');
        return view('admin.soru.cevapsiz-sorular', compact('sorular', 'soruKategorileri'));
    }

    public function setHataliSoruBildir(Request $request)
    {
        try {
            if (!$request->user_id > 0) {
                return response()->json([
                    "hata" => 'Öncelikle Üye Girişi Yapmalısınız..!'
                ]);
            }

            $hata = strip_tags(trim($request->hata));

            if ($request->soru_id > 0 && $request->sinav_id > 0 && $request->user_id > 0 && strlen($hata) > 0) {
                $ekle = new SoruHatalari();
                $ekle->soru_id = $request->soru_id;
                $ekle->sinav_id = $request->sinav_id;
                $ekle->user_id = $request->user_id;
                $ekle->hata = $hata;
                $ekle->hatali_mi = 1; //Sınav Sayfasından soru hatalı olarak bildirildiği için 1, hatalı değilse 0 olcak.
                $ekle->durum = 1; //Hatalı bildirildi, durumu 1 olan aynı soru_id li en az örn:3 kayıt olursa soru oylamaya çıkacak
                $ekle->ip = $request->ip();
                $ekle->save();

                Log::info('Hatalı Soru Bildirimi : '.$request->soru_id.' id no\'lu soru, '.$request->sinav_id.' no\'lu sınavda hatalı olarak bildirildi.');
                return response()->json([
                    "ok" => 'Bildiriminiz Başarıyla Kaydedildi ve İşleme Alındı.'
                ]);
            } else {
                return response()->json([
                    "hata" => 'Bir hata oluştu, daha sonra tekrar deneyiniz.'
                ]);
            }
        } catch (QueryException $e) {
            Log::error('Hatalı Soru Bildiriminde Query Hatası: ' . $e->getMessage() . ' Code:' . $e->getCode() . ' Line:' . $e->getLine().' SQL: '.$e->getSql());
            return response()->json([
                "sonuc" => false
            ]);
        } catch
        (\Exception $e) {
            Log::error('Hatalı Soru Bildiriminde Hata: ' . $e->getMessage() . ' Code:' . $e->getCode() . ' Line:' . $e->getLine());
            return response()->json([
                "hata" => 'Bir hata oluştu, daha sonra tekrar deneyiniz.'
            ]);
        }
    }

    public function getQuestion($qId){
//        return collect(DB::select('select FTURKCE(soru) as soru from sorus where id = ?', [$qId]))->first();
        return Soru::findOrFail($qId);
    }

    public function getAnswers($qId){
        return DB::select('select FTURKCE(cevap) as cevap from cevaps where soru_id = ?', [$qId]);
    }

    public function getCleanQuestion($question, $answers){

        $question = str_replace("<div><em>",'', $question);
        $question = str_replace("</em></div>","\n\n", $question);
        $question = str_replace("<br>","\n\n", $question);
        $question = str_replace("<br >","\n\n", $question);
        $question = str_replace("<br />","\n\n", $question);
        //$question = strip_tags($question);

        $socialQuestion = 'Soru :'.replaceSpace( $question)."\n\n";

        $i= 65; //A harfinin ASCII karşılığı
        foreach ($answers as $answer)
        {
            $socialQuestion = $socialQuestion."      ".chr($i).' ) '.replaceSpace($answer->cevap)."\n";
            $i++;
        }

        return strip_tags(wordwrap(html_entity_decode(htmlspecialchars_decode($socialQuestion), ENT_NOQUOTES,'UTF-8')));
    }

    public function setQuestionImage($qId){
        $question = $this->getQuestion($qId);

        $answers = $question->cevaplar;
//        $answers = $this->getAnswers($qId);

        return $this->getCleanQuestion($question->soru, $answers);
    }

    public function saveQuestionImage($qId){
        $soruVeCevaplari = $this->setQuestionImage($qId);

        $img = Image::make(public_path('img/soru-sm-800x800.JPG'));

        $img->text($soruVeCevaplari, 50,320,
            function($font) {
                $font->file(public_path('css/fonts/Roboto-Regular.ttf'));
                $font->size(18);
            }
        );

        $img->save(public_path('resim/soru_resimleri/'.$qId.'.jpg'));

        return true;
    }

    public function getQuestionImage($qId)
    {
        try{
            $question = $this->getQuestion($qId);
            $soruVeCevaplari = $this->setQuestionImage($qId);

            if($this->saveQuestionImage($qId))
            return view('front.sinav.soru-sosyal-medya')->with(['qId' => $qId, 'question' => $question, 'soruCevaplar' => $soruVeCevaplari]);

        }catch (\Exception $e){
            Log::error('paylaş', ['Hata'=>$e->getMessage().'---'.$e->getLine().'---'.$e->getCode()]);
            return abort(404);
        }
    }

    public function ajaxSoruUpdate(Request $request){
        Soru::where('id', $request->id)->update([
            'soru' => $request->soru
        ]);
        return ['title' => 'Başarılı', 'text'=>$request->id.' id numaralı soru güncellendi.. :)'];

    }

    public function ajaxSoruKategoriUpdate(Request $request){
        Soru::where('id', $request->soru_id)->update([
            'soru_kategori_id' => $request->soru_kategori_id
        ]);
        return ['title' => 'Başarılı', 'text'=>$request->id.' id numaralı sorunun kategorisi güncellendi.. :)'];
    }

    public function ajaxSoruDurumUpdate(Request $request){
        Soru::find($request->soru_id)->update([
            'durum' => $request->durum
        ]);
        return ['title' => 'Başarılı', 'text'=>$request->soru_id.' id numaralı sorunun durumu güncellendi.. :)'];
    }

    public function ajaxSoruKategoriInsert(Request $request){
        try{

            $insert = new SoruKategori();
            $insert->kategori_adi = $request->kategori_adi;
            $insert->ip = $request->ip();
            $insert->ekleyen = Auth::id();
            $insert->saveOrFail();

            return ['title' => 'Başarılı',
                    'id' =>$insert->id,
                    'kategori_adi' => $request->kategori_adi,
                    'text'=>$request->kategori_adi.' isimli soru kategorisi eklendi.. :)'];

        }catch (\Exception $e){
            Log::error('Soru Kategori Ekleme Hatası', ['Hata'=> 'Yeni soru kategorisi eklerken hata oluştu']);
        }
    }

//    public function getQuestionImage($qId)
//    {
//        //gelen soru id ile soru ve cevaplarına ulaşıp bunları resme dönüştüreceğim.
//         $question = collect(DB::select('select FTURKCE(soru) as soru from sorus where id = ?', [$qId]))->first();
//         $answers = DB::select('select FTURKCE(cevap) as cevap from cevaps where soru_id = ?', [$qId]);
//
//        function replaceSpace($string)
//        {
//            $string = preg_replace("/\s+/", " ", $string);
//            $string = trim($string);
//            return $string;
//        }
//        $question->soru = str_replace_array('\n\n', ['</li>', '<br>','<br >','</ul>','</em>'], $question->soru);
//        $question->soru = str_replace_array('', ['<li>', '<ul>','<em>'], $question->soru);
//
//        $question->soru = htmlspecialchars_decode($question->soru);
////        $socialQuestion = 'Soru :'.replaceSpace(strip_tags( $question->soru))."\n\n";
//        $socialQuestion = 'Soru :'.replaceSpace( $question->soru)."\n\n";
//
//         $i= 65; //A harfinin ASCII karşılığı
//         foreach ($answers as $answer)
//         {
//             $socialQuestion = $socialQuestion."      ".chr($i).' ) '.replaceSpace(strip_tags($answer->cevap))."\n\n";
//             $i++;
//         }
//        $q = wordwrap(strip_tags($socialQuestion));
//        // create Image from file
//        //$sizes = imagettfbbox(14, 0, public_path('/css/fonts/Roboto-Regular.ttf'), $q);
//
//        /*$width = $sizes[2] > 780 ? $sizes[2] : 780;
//        $height = $sizes[3]+ 30;*/
//
////        $img = Image::canvas($width,$height, '#EFF0F4');
//        $img = Image::make(public_path('/img/soru-sm-bgg.jpg'));
//
//        // write text
//        $img->text($q, 100,200,
//            function($font) {
//                $font->file(public_path('/css/fonts/Roboto-Regular.ttf'));
//                $font->size(18);
//                //$font->angle(10);
//                //$font->color('#B69005');
//                //$font->align('center');
//                //$font->valign('center');
//            }
//            );
//
//         $img->save(public_path('resim/soru_resimleri/').$qId.'.jpg');
//         return view('front.sinav.soru-sosyal-medya')->with(['qId' => $qId, 'question' => $question]);
////         return view('front.sinav.soru-sosyal-medya')->with(['qId' => $qId]);
//    }
}
