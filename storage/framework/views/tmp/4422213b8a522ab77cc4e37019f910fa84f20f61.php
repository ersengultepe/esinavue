<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true" :style="dsply">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="formModalLabel">Hatalı Soru Bildirimi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div v-if="hataliSoruMesaj !== null">
                    <soru-hata-error v-if="hataliSoruMesaj.hata !== undefined" :hatalisorumesaj="hataliSoruMesaj"></soru-hata-error>
                    <soru-hata-success v-if="hataliSoruMesaj.ok !== undefined" :hatalisorumesaj="hataliSoruMesaj"></soru-hata-success>
                </div>
                <div class="form-group row" v-if="dsply">
                    <label class="col-sm-3 text-left text-sm-right mb-0">Hata Açıklaması</label>
                    <div class="col-sm-9">
                        <textarea v-model="soruHata" rows="5" class="form-control" placeholder="Sorudaki hatayı ayrıntılarıyla yazınız..." required></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Kapat</button>
                <button v-if="dsply" @click="setHataliSoruBildir()" type="button" class="btn btn-primary">Gönder</button>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-9">

    <div class="row counters with-borders">
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="counter counter-primary">
                <strong :data-to="soru_sayisi">{{ soru_sayisi }}</strong>
                <label>Soru Sayısı</label>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
            <div class="counter counter-primary">
                <strong :data-to="ortalama_puan" data-plugin-options="{'decimals': 2}">{{ ortalama_puan }}</strong>
                <label>Ort. Puan</label>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
            <div class="counter counter-primary">
                <strong :data-to="kac_kez_cozdun">{{ kac_kez_cozdun }}</strong>
                <label>Kez Çözdün</label>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="counter counter-primary">
                <strong :data-to="cozum_sayisi">{{ cozum_sayisi }}</strong>
                <label>Çözüm Sayısı</label>
            </div>
        </div>
    </div>
    <br>

    <div v-if="sorular !== 'hata'">
        <div v-for="(soru, index) in sorular">
            <section class="call-to-action featured mb-5" v-if="soru.tip===1" :id="soru.puan"
                     @mouseover="
                     mouseOverVue('/resim/soru_resimleri/'+soru.id+'.jpg',
                     'sinav/'+sinav_link,
                     'title Esinavsalonu.com daki bu soruyu beğendim.',
                     'esinavsalonu desc'
                     )"
                     :style="getSoruZorlukSeviyesi(soru.puan, soru_sayisi)">

                <h4 class="heading-primary">
                    <strong>{{ soru.sira+'.' }} Soru: </strong><br>
                <?php if(auth()->guard()->check()): ?>
                    <!-- Soru Hatalıysa Bildir -->
                        <span @click="setSoruId(soru.id)" data-toggle="modal" data-target="#formModal">
                        <i style="float: left;margin-top: 3%" class="fa fa-bug" data-toggle="tooltip"
                           data-placement="top" title="" data-original-title="Hata Bildirimi"></i>
                    </span>
                    <?php endif; ?>
                </h4>

                <div class="col-sm-9 col-lg-9">
                    <div class="call-to-action-content" style="text-align: left !important;">
                    <span v-if="soru.tip===1" style="float:left">
                        <span v-html="soru.icerik" style="font-size: small">{{ soru.icerik.trim() }}</span>
                    </span>

                        <div style="float:left;margin-right: 1%;margin-left: 3%" class="radio col-sm-12"
                             v-for="loop of sorular" v-if="loop.soru_id === soru.soru_id">
                            <label v-if="loop.tip===2" style="float:left;margin-right: 1%;margin-left: 3%">
                                <input type="radio" :value="loop.id" :name="loop.soru_id"
                                       :checked="secilenSiklar[loop.soru_id] === loop.id"
                                       @click="getCevapCheck(loop.id, ' <?php echo e(session()->token()); ?>  ', loop.soru_id, soru.puan, <?php echo e(Auth::check() ? Auth::id() : 0); ?>)"
                                       :disabled="isDisable(loop.soru_id) !== undefined">
                                <span class="cr" :id="loop.id"><i class="cr-icon fa fa-circle" :id="loop.id"></i></span>
                                <span v-html="loop.icerik" :style="[{ color : cevapFind(loop.id, loop.soru_id) }]">
                                    <div v-show="preloadx=== false">
                                        {{ loop.icerik.trim() }}
                                    </div>
                                </span>

                                <span style="display: none"></span>
                            </label>
                        </div>
                        <br>

                        <div data-toggle="tooltip" data-placement="top" title="" data-original-title="İkinci tıklamanızda sorunun resmi çıkacaktır..!"
                             class="fb-share-button" :data-href="'https://esinavsalonu.com/get/question-image/'+soru.id" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                            <a target="_blank" :href="'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fesinavsalonu.com%2Fget%2Fquestion-image%2F'+soru.id+'&amp;src=sdkpreparse'" class="fb-xfbml-parse-ignore">Paylaş
                            </a>
                        </div>
                        <a class="twitter-share-button"  href="https://twitter.com/intent/tweet?text=<?php echo e(Route::current()->parameters()['url']); ?> isimli sınavda geçen soru">Tweet</a>

                        <span v-if="randNum(1,9) === parseInt(soru.sira)">REKLAM</span>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div v-else>
        <soru-goruntuleme-izni-yok></soru-goruntuleme-izni-yok>
    </div>

    <span v-for="n in Math.ceil(soru_sayisi/10)" style="margin-right: 1%">
        <a :class="n === isActive  ? 'btn btn-outline-primary active' : 'btn btn-outline-primary' "
           @click="getNextPageSorular(n)" href="#bastaraf">{{ n }}. Sayfa </a>
    </span>

    <div v-if="yorumlar !== null">
        <yorumlar :yorumlar="yorumlar" ref="userSlug()"></yorumlar>
    </div>
    <?php echo $__env->make('front.sinav.yorum-gonder', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div>