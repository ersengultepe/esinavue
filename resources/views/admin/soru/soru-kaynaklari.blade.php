@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
@stop

@section('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
<!-- Examples -->
<script src="{{ asset('pa/HTML/assets/javascripts/tables/soru.kaynak.datatables.editable.js') }}"></script>
@stop

@section('content')
    <!-- start: page -->
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">Soru Kaynakları</h2>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="mb-md">
                        <button id="addToTable" class="btn btn-primary">Ekle <i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                <thead>
                <tr>
                    <th>NO</th>
                    <th>Sınav & Soru Kaynağı</th>
                    <th>Sınavı Düzenleyen Kurum</th>
                    <th>Sınav Düzenlenen Kurum</th>
                    <th>Telif Var mı?</th>
                    <th>Sınav Tarihi</th>
                    <th>Soru Sayısı</th>
                    <th>Ekleyen Kullanıcı</th>
                    <th>İşlemler</th>
                </tr>
                </thead>
                <tbody>

                @foreach($soruKaynaklari as $kaynak)
                    <tr class="gradeX" id="{{ $kaynak->id }}">
                        <td>{{ $kaynak->id }}</td>
                        <td>{{ $kaynak->ad }}</td>
                        <td>{{ $kaynak->duzenleyen_kurum }}</td>
                        <td>{{ $kaynak->duzenlenen_kurum }}</td>
                        <td>{{ $kaynak->telif }}</td>
                        <td>{{ $kaynak->tarih }}</td>
                        <td>{{ $kaynak->soru_sayisi }}</td>
                        <td>{{ $kaynak->user_id }}</td>
                        <td class="actions">
                            <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                            <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                            <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </section>

    <div id="dialog" class="modal-block mfp-hide">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Are you sure?</h2>
            </header>
            <div class="panel-body">
                <div class="modal-wrapper">
                    <div class="modal-text">
                        <p>Are you sure that you want to delete this row?</p>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button id="dialogConfirm" class="btn btn-primary">Confirm</button>
                        <button id="dialogCancel" class="btn btn-default">Cancel</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
    <!-- end: page -->

    @stop