{{ $sorular->links() }}
<div class="panel-body">
    @if(count($sorular) > 0)

        <?php $i = 1; $abcde = ['A', 'B', 'C', 'D', 'E']; $j = 0; ?>
        @foreach($sorular as $soru)
            <div class="col-md-6">

                <div class="form-group">
                    <button type="button" id="bt_{{ $soru->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Güncelle"
                            class="col-md-2 mb-xs mt-xs mr-xs btn btn-primary soru-update" style="margin-bottom: 1px;margin-top: 1px !important;">
                        <i class="fa fa-edit"></i> Soru {{ $loop->iteration }}</button>

                    <div class="switch switch-sm switch-success" id="swr_{{ $soru->id }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Onay Durumunu Güncelle">
                        <input type="checkbox" class="soru-onay-update" id="sw_{{ $soru->id }}" name="switch" data-plugin-ios-switch @if($soru->durum === 1) checked @endif />
                    </div>

                    <div class="col-md-8" data-toggle="tooltip" data-placement="top" title="" data-original-title="Soru Kategorisini Güncelle">
                        <select name="soru_kategori" data-plugin-selectTwo class="form-control populate soru-kategori-update" id="sc_{{ $soru->id }}">
                            @foreach($soruKategorileri as $kategori)
                                <option value="{{ $kategori->id }}" @if($kategori->id === $soru->soru_kategori_id)selected @endif >{!! $kategori->kategori_adi !!}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <textarea name="soru" class="ckeditor" id="tx_{{  $soru->id  }}" rows="2">{!! turkceAsci($soru->soru) !!}</textarea>

                @foreach($soru->cevaplar as $cevap)
                    <div class="form-group @if($cevap->dogru == 1 ) has-success @endif" style="margin: 1px !important;">
                        <button class="col-md-1 btn btn-outline-warning">
                            <a href="{{ route('getCevapUpdate', ['soru_id'=>$soru->id, 'cevap_id'=>$cevap->id]) }}">
                                {{ $abcde[$loop->index].') ' }}
                            </a>
                        </button>
                        <div class="col-md-11">
                            <div class="input-group input-group-icon">
                                @if($cevap->dogru == 1 )
                                    <span class="input-group-addon">
												<span class="icon"><i class="fa fa-check"></i></span>
											</span>
                                @endif
                                <input name="cevap" class="form-control" type="text" size="90" id="cevap{{ $cevap->id }}"
                                       value="{!! turkceAsci($cevap->cevap) !!}" >
                            </div>

                        </div>
                    </div>
                @endforeach

            </div>
            @if($loop->iteration%2==0)
                <div class="col-md-12">
                    <hr style="font-size: 3em">
                </div>
            @endif

        @endforeach
        <br>

    @else
        <div class="col-md-12">
            <h4>Listelenecek Soru Bulunamadı..!</h4>
        </div>
    @endif
</div>
{{ $sorular->links() }}