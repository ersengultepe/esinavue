<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurum', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dsc')->comment('Sosyal Güvenlik Kurumu gibi');
            $table->string('slug')->comment('sosyal-guvenlik-kurumu')->unique();
            $table->string('tag')->comment('SGK, MEB, TODAİE gibi')->unique();
            $table->ipAddress('ip');
            $table->unsignedInteger('user_id');
            $table->enum('durum', [1,0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurum');
    }
}
