/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');

import Plane from 'vue-loading-spinner/src/components/Plane.vue';
import Pencil from 'vue-loading-spinner/src/components/Pencil.vue';
import Circle9 from 'vue-loading-spinner/src/components/Circle9.vue';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('makale', require('./components/makale.vue'));
Vue.component('soru-goruntuleme-izni-yok', require('./components/soru-goruntuleme-izni-yok.vue'));

Vue.component('ebulten-success', require('./components/ebulten-success.vue'));
Vue.component('ebulten-error', require('./components/ebulten-error.vue'));

Vue.component('mesaj-success', require('./components/mesaj-success.vue'));
Vue.component('mesaj-error', require('./components/mesaj-error.vue'));

Vue.component('yorum-error', require('./components/yorum-error.vue'));
Vue.component('yorum-success', require('./components/yorum-success.vue'));
Vue.component('yorumlar', require('./components/yorumlar.vue'));

Vue.component('soru-hata-error', require('./components/soru-hata-error.vue'));
Vue.component('soru-hata-success', require('./components/soru-hata-success.vue'));

Vue.component('Message', require('./components/message.vue'));

//Preloading components
Vue.component('Pencil', Pencil);
Vue.component('Plane', Plane);
Vue.component('Circle9', Circle9);

/*
const app = new Vue({
    el: '#app'
});*/
