<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UyelerDetay extends Model
{
    protected $fillable = [
        'user_id',
        'sinav_id',
        'puan',
        'dogru',
        'yanlis',
        'bos',
        'tarih',
        'ip',
        'token',
        'cevap_soru_ids',
        'durum',
    ];
}
