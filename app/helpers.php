<?php

use App\Models\UyelerDetay;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/**
 * Created by PhpStorm.
 * User: metehan
 * Date: 29.05.2018
 * Time: 05:39
 */

function userTitle($par){
    $par = $par ?? 'Ziyaretçi';
    return ucWordsTr(mb_strtolower($par,"UTF-8"));
}

function userPhoto($par){

    if(str_contains($par->foto,'facebook.com') )
    {
        return str_replace('normal', 'large', $par->foto);
    }

    if(str_contains($par->foto,'http:') )
    {
        return str_replace('http:', 'https:', $par->foto);
    }
    return asset($par->foto);
}

function userSlug($kullanici, $id){
    return url('uye/'.$id.'-'.str_slug($kullanici));
}

function sinavUrl($url){
    return url('sinav/'.$url);
}

function ucWordsTr($string) {
    $arr = explode(' ', $string);
    $return = ''; $i = 0;
    foreach ($arr as $str) {
        $chars = ['i','İ','ü', 'Ü', 'ö', 'Ö', 'ç', 'Ç', 'ğ', 'Ğ', 'ş', 'Ş'];
        $replace = ['İ','İ','Ü', 'Ü', 'Ö', 'Ö', 'Ç', 'Ç', 'Ğ', 'Ğ', 'Ş', 'Ş'];
        $firstChar = mb_substr($str,0,1,'UTF-8');
        if( in_array( $firstChar, $chars ) ) {
            $firstChar = str_replace( $chars, $replace, $firstChar );
            $return .= ($i>0 ? ' ' : '') . $firstChar . mb_substr($str,1);
        } else {
            $return .= ($i>0 ? ' ' : '') . ucfirst($str);
        }
        $i++;
    }
    return rtrim($return);
}

function replaceSpace($string)
{
    $string = preg_replace("/\s+/", " ", $string);
    $string = trim($string);
    return $string;
}

function breadCrumb()
{
    return Session::has('breadcrumb') ? Session::get('breadcrumb') : null;
}

function dogruCevapAddClass($par)
{
    return str_contains($par, '$&&$=> ') ? 'has-success' : null;
}

function turkceAsci($par)
{
/*
? &Ccedil;  ? &ccedil;

G &#286;  g &#287;

I &#304; i &#305;

? &Ouml; ? &ouml;

S &#350; s &#351;

? &Uuml; ? &uuml;
& &amp;
< &lt;
> &gt;
" &quot;
' &#039;
*/

    $par = str_replace( '&amp;&Ccedil;', 'Ç' ,$par);
    $par = str_replace( '&amp;&ccedil;', 'ç' ,$par);
    $par = str_replace( '&amp;&#286;', 'G' ,$par);
    $par = str_replace( '&amp;&#287;', 'g' ,$par);
    $par = str_replace( '&amp;&#304;', 'I' ,$par);
    $par = str_replace( '&amp;&#305;', 'i' ,$par);
    $par = str_replace( '&amp;&Ouml;', 'Ö' ,$par);
    $par = str_replace( '&amp;&ouml;', 'ö' ,$par);
    $par = str_replace( '&amp;&#350;', 'S' ,$par);
    $par = str_replace( '&amp;&#351;', 's' ,$par);
    $par = str_replace( '&amp;&Uuml;', 'Ü' ,$par);
    $par = str_replace( '&amp;&uuml;', 'ü' ,$par);

    $par = str_replace( '&amp;Ccedil;', 'Ç' ,$par);
    $par = str_replace( '&amp;ccedil;', 'ç' ,$par);
    $par = str_replace( '&amp;#286;', 'G' ,$par);
    $par = str_replace( '&amp;#287;', 'g' ,$par);
    $par = str_replace( '&amp;#304;', 'I' ,$par);
    $par = str_replace( '&amp;#305;', 'i' ,$par);
    $par = str_replace( '&amp;Ouml;', 'Ö' ,$par);
    $par = str_replace( '&amp;ouml;', 'ö' ,$par);
    $par = str_replace( '&amp;#350;', 'S' ,$par);
    $par = str_replace( '&amp;#351;', 's' ,$par);
    $par = str_replace( '&amp;Uuml;', 'Ü' ,$par);
    $par = str_replace( '&amp;uuml;', 'ü' ,$par);

    $par = str_replace( '&Ccedil;', 'Ç' ,$par);
    $par = str_replace( '&ccedil;', 'ç' ,$par);
    $par = str_replace( '&#286;', 'G' ,$par);
    $par = str_replace( '&#287;', 'g' ,$par);
    $par = str_replace( '&#304;', 'I' ,$par);
    $par = str_replace( '&#305;', 'i' ,$par);
    $par = str_replace( '&Ouml;', 'Ö' ,$par);
    $par = str_replace( '&ouml;', 'ö' ,$par);
    $par = str_replace( '&#350;', 'S' ,$par);
    $par = str_replace( '&#351;', 's' ,$par);
    $par = str_replace( '&Uuml;', 'Ü' ,$par);
    $par = str_replace( '&uuml;', 'ü' ,$par);

    $par = str_replace( '&lt;', '<' ,$par);
    $par = str_replace( '&gt;', '>' ,$par);
    $par = str_replace( '&quot;', '"' ,$par);
    $par = str_replace( '&#039;', '\'\'', $par);
    $par = str_replace( '&amp;', '&' ,$par);

    $par = str_replace( '&amp;&lt;', '<' ,$par);
    $par = str_replace( '&amp;&gt;', '>' ,$par);
    $par = str_replace( '&amp;&quot;', '"' ,$par);
    $par = str_replace( '&amp;&#039;', '\'\'',$par);

    $par = str_replace( '&amp;lt;', '<' ,$par);
    $par = str_replace( '&amp;gt;', '>' ,$par);
    $par = str_replace( '&amp;quot;', '"' ,$par);
    $par = str_replace( '&amp;#039;', '\'\'', $par);

    $par = str_replace( '\n', '' ,$par);
    $par = str_replace( '\t', '' ,$par);
    $par = str_replace( '&nbsp;', ' ' ,$par);
    $par = str_replace( '<p>', '' ,$par);
    $par = str_replace( '</p>', '' ,$par);
    return $par;
}

function replace_tr($text) {
    $text = trim($text);
    $search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü');
    $replace = array('c','c','g','g','i','i','o','o','s','s','u','u');
    $new_text = str_replace($search,$replace,$text);
    return $new_text;
}

function cleanStr($str)
{
    return strip_tags(wordwrap(html_entity_decode(htmlspecialchars_decode($str), ENT_NOQUOTES,'UTF-8')));
}

/**
 * Dosya adı ile google drive vb. buluttan dosya siler.
 * @param $fileName
 * @return bool|string
 */
function cloudFileDeleteFromName($fileName)
{
    $files =  Storage::cloud()->files();

    foreach ($files as $file)
    {
        $metaData = Storage::cloud()->getDriver()->getAdapter()->getMetadata($file);
        if($metaData['name'] === $fileName)
        {
            Storage::cloud()->delete($file);
            Log::info($fileName.' dosyası başarıyla silindi.');
            return true;
        }
    }
    return false;
}

/**
 * sh uzantılı sunucu dosyalarının execute etmek için kullanılır.
 * @param $filePath
 * @return string
 */
function shExecute($filePath)
{
    $process = new \Symfony\Component\Process\Process('sh '.$filePath);
    $process->run();

    // executes after the command finishes
    if (!$process->isSuccessful()) {
        return false;
        //throw new \Symfony\Component\Process\Exception\ProcessFailedException($process);
    }

    return true;
}

function userCity($userId)
{
    return Cache::remember($userId.'-sehir', 1440, function () use($userId){
        $uyeDetay = UyelerDetay::where('user_id', $userId)
            ->where('durum', 1)
            ->orderBy('id', 'DESC')
            ->select('ip')
            ->first();
        if ($uyeDetay !== null)
        {
            $geo = geoip()->getLocation($uyeDetay->ip)->toArray();
            return $geo['city'];
        }else{
            return null;
        }
    });

}