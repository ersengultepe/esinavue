<?php

namespace App\Http\Controllers;

use App\Models\Cevap;
use App\Models\Soru;
use Illuminate\Http\Request;

class CevapController extends Controller
{
    public function getCevapUpdate($soru_id, $cevap_id){
        $soru = Soru::find($soru_id);
        $cevaplar = $soru->cevaplar;
        return view('admin.cevap.cevap-update', compact('soru', 'cevaplar'));
    }

    public function ajaxCevapUpdate(Request $request){
        Cevap::find($request->id)->update([
            'cevap' => $request->cevap
        ]);

        return ['title' => 'Başarılı', 'text'=> $request->id.' id no\'lu Cevap başarıyla güncellendi.. :)'];
    }

    public function ajaxCevapDogruUpdate(Request $request){
        //eğer cevap doğru olarak işaretlenmişse o sorunun tüm cevapları dogru=0, sonra ilgili cevap dogru=1 yapılır
        if($request->dogru === 'checked'){
            $res = 'DOĞRU';
            Cevap::where('soru_id', $request->soru_id)->update([
                'dogru' => 0
            ]);

            Cevap::find($request->cevap_id)->update([
                'dogru' => 1
            ]);
        }else{ //eğer cevap yanlış olarak işaretlenirse o cevap dogru=0 yapılır
            $res = 'YANLIŞ';
            Cevap::find($request->cevap_id)->update([
                'dogru' => 0
            ]);
        }

        return ['title' => 'Başarılı', 'text'=> $request->cevap_id.' id no\'lu Cevap '.$res.' olarak güncellendi.. :)'];
    }
}
