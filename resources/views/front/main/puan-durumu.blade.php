<div class="container" >

    <div class="pricing-table row no-gutters mt-2 mb-2">
        <div class="col-lg-4">
            <div class="plan">
                <h3>SORU ÇÖZENLER<span><i class="fas fa-edit"></i></span></h3>

                <ul class="simple-post-list">

                    @foreach($soruCozenUyeler as $uye)
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="{{ userSlug($uye->kullanici, $uye->id) }}">
                                        <img src="{{ userPhoto($uye) }}" alt="{{ userTitle($uye->kullanici) }}" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="{{ userSlug($uye->kullanici, $uye->id) }}" style="float:left;" class="linka">{{ userTitle($uye->kullanici) }} &nbsp; <small>{{ userCity($uye->id) }}</small> </a></div><br>
                            <a href="{{ sinavUrl($uye->link) }}" class="text-primary linka" data-toggle="tooltip" title="{{ $uye->adi }}" data-original-title="{{ $uye->adi }}">
                                <div class="progress mb-2" style="">
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemax="100" aria-valuemin="20" aria-valuenow="{{ $uye->puan }}" style="width: {{ $uye->puan }}%;">
                                        {{ $uye->puan }}
                                    </div>

                                </div>
                            </a>


                        </li>
                        @endforeach

                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="plan">
                <h3>YENİ ÜYELER<span><i class="far fa-user"></i></span></h3>

                <ul class="simple-post-list" >

                    @foreach($yeniUyeler as $uye)
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="{{ userSlug($uye->kullanici, $uye->id) }}" >
                                        <img src="{{ userPhoto($uye) }}" alt="{{ userTitle($uye->kullanici) }}" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="{{ userSlug($uye->kullanici, $uye->id) }}" style="float: left;" class="linka">{{ userTitle($uye->kullanici) }} &nbsp; <small>{{ userCity($uye->id) }}</small></a></div><br>
                            <div style="float: left">
                                {{ $uye->sehir }}
                            </div>

                        </li>
                        @endforeach

                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="plan">
                <h3>Puan Durumu<span><i class="fas fa-list-ol"></i></span></h3>

                <ul class="simple-post-list" id="mimg">

                    @foreach($puanDurumu as $uye)
                        <li >
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="{{ userSlug($uye->kullanici, $uye->id) }}">
                                        <img src="{{ userPhoto($uye) }}" alt="{{ userTitle($uye->kullanici) }}" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="{{ userSlug($uye->kullanici, $uye->id) }}" style="float: left;" class="linka">{{ userTitle($uye->kullanici) }} &nbsp; <small>{{ userCity($uye->id) }}</small></a></div><br>
                            <div style="float: left">
                                {{ $uye->puan }}
                            </div>

                        </li>
                        @endforeach

                </ul>
            </div>
        </div>
    <div class="col-md-12">
		<!-- Admatic masthead 970x250 Ad Code START -->
		<ins data-publisher="adm-pub-190101685456" data-ad-type="masthead" class="adm-ads-area" data-ad-network="182901247802" data-ad-sid="501" data-ad-width="970" data-ad-height="250"></ins>
		<!-- Admatic masthead 970x250 Ad Code END -->
	</div>	
	</div>

</div>