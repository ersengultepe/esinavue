let vm;
vm = new Vue({
    el: '#sinav',
    name: 'Sinav',
    mounted() {
        setTimeout(() => {
            this.getSinavSuccessfulUsers();
            this.getSorular();
            // this.getNextPageSorular(1);
            this.getSinavInfos();
            //this.getKacKezCozdun();

        }, 2000)
    },
    data: {
        avatar: '',
        sinav_adi: '',
        sinav_link: '',
        sinav_id: 0,
        soru_sayisi: 0,
        ortalama_puan: 0,
        cozum_sayisi: 0,
        sorular: [],
        success_users: [],
        cevapCheck: false,
        abcde : ['A) ', 'B) ', 'C) ', 'D) ', 'E) '],
        sayac : 0,
        dogrular: [],
        dogrular2: [],
        yanlislar: [],
        isActive: 1,
        secilenSiklar: {},
        toplamPuan: 0,
        sure: '',
        preloadx: false,
        userId: document.getElementById('user-id').textContent,
        routeUrlParameter: document.getElementById('route-url-parameter').textContent,
        kac_kez_cozdun: 0,
        kullanici: '',
        yorum: '',
        email: '',
        yorumresult: null,
        yorumView: true,
        yorumlar: null,
        soruHata: null,
        soru_id: 0,
        hataliSoruMesaj: null,
        dsply: true,
        socialDesc: '',
        socialTitle: '',
        socialUrl: '',
        socialImage:'',
		answer: 0,
        cevapClass : ''
    },
    methods: {
        getSorular()
        {
            /**
            *  Sayfanın adresi window.location.href ile yakalanır
            *  replace ile değişiklik yapılarak rotaya uygun hale gelir
            *  Buradaki 1 sayfa no yu temsil eder.
            */
            axios.get(window.location.href.replace('/sinav/', '/sinav-getir/1/0/')).then(response => {
                this.sorular = response.data;
            });

        },

        getYorumlar(sinav_id)
        {
            axios.get(document.location.origin + '/get/yorumlar/' + sinav_id)
                .then(response => {
                    this.yorumlar = response.data;
                });
        },

        getSinavInfos()
        {
            axios.get(window.location.href.replace('/sinav/', '/sinav-infos/'))
                .then(response => {
                    this.soru_sayisi = response.data[0].soru_sayisi;
                    this.sinav_adi = response.data[0].adi;
                    this.sinav_link = response.data[0].link;
                    this.sinav_id = response.data[0].id;
                    this.ortalama_puan = response.data[0].ortalama_puan.replace(',', '.',);
                    this.cozum_sayisi = response.data[0].cozum_sayisi;
                    this.getYorumlar(response.data[0].id);
                });
        },

        getNextPageSorular(n)
        {
            let uId = this.userId === "" ? 0 : parseInt(this.userId) ;
            //http://localhost:8000/sinav/652-sayili-meb-teskilati-khk-deneme-sinavi-1 bunun yerine
            //http://localhost:8000/sinav-getir/2/652-sayili-meb-teskilati-khk-deneme-sinavi-1 bu şekilde çalışır.
            axios.get(window.location.href.replace('/sinav/', '/sinav-getir/'+ n +'/'+uId+'/'))
                .then(response =>
                {
                    this.sorular = [];
                    this.sorular = response.data;
                    this.isActive = n;
                    this.radioSelect = '';
                });
        },

        getSinavSuccessfulUsers() {
            axios.get(window.location.href.replace('/sinav/', '/sinav-success/')).then(response => {
                this.success_users = response.data;
            })
        },

        slugify(text) {
            var trMap = {
                'çÇ': 'c',
                'ğĞ': 'g',
                'şŞ': 's',
                'üÜ': 'u',
                'ıİ': 'i',
                'öÖ': 'o'
            };
            for (var key in trMap) {
                text = text.replace(new RegExp('[' + key + ']', 'g'), trMap[key]);
            }
            return text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
                .replace(/\s/gi, "-") // convert spaces to dashes
                .replace(/[-]+/gi, "-") // trim repeated dashes
                .toLowerCase();
        },

        userSlug(id, kullanici) {
            return document.location.origin + '/uye/' + id + '-' + this.slugify(kullanici);
        },

        userPhoto(par) {
            if (par.includes('http')) {
                par.replace('http:', 'https:');
                return par;
            }
            return document.location.origin+'/'+par;
			
        },

        getCevapCheck(cevap, token, soru_puani, user_id) {
            this.preloadx = true;

            if (cevap.dogru === 1) {
                //Soru doğru cevaplanmışsa sorunun doğru sayısı bir artırılacak
                //3. paremetre 1=>doğru, 2=>yanlış

                this.dogrular.push(cevap.id);
                this.dogrular2.push(cevap.id);
                this.dogruSayisiniBul(soru_puani);
                //soru doğru cevaplandığı için
                this.answer = 1;
            } else {
                //Soru doğru cevaplanmamışsa sorunun yanlış sayısı bir artırılacak
                //3. paremetre 1=>doğru, 2=>yanlış

                this.yanlislar.push(cevap.id);
                this.dogrular2.push(cevap.id);
            }

            if (this.secilenSiklar[cevap.soru_id] === undefined) {
                this.secilenSiklar[cevap.soru_id] = cevap.id;
            }

            if(this.toplamPuan >0){
                this.cevapSave(token, user_id, cevap); //cevap.dogru 1 veya 0 gönderecek
            }

            setTimeout( () => {
                this.preloadx = false;
            }, 800);

            if(this.answer === 1){
                toastr.success("Doğru cevapladınız...<br><strong>"+ (this.soru_sayisi - Object.keys(this.secilenSiklar).length)+"</strong> Soru kaldı...");
            }else{
                toastr.error("Yanlış Cevapladınız...<br><strong>"+ (this.soru_sayisi - Object.keys(this.secilenSiklar).length)+"</strong> Soru kaldı...");
            }

            this.answer = 0;

            if ((this.soru_sayisi-Object.keys(this.secilenSiklar).length) === 0){

                setTimeout(function () {

                    let span = document.createElement("span");
                    span.innerHTML = "<h4><strong>"+document.getElementById('sure').textContent+"</strong> sürede " +
                        "Toplam <span class='inverted inverted-tertiary'>"+ vm.toplamPuan+"</span> puan almayı başararak sınavı bitirdiniz.</h4>";
                    swal({
                        title: "Tebrikler",
                        content: span,
                        button: {
                            text: "Kapat", //Buton yazısı
                            value: true,
                            visible: true, //Görünsün mü? true, false
                            className: "", //class değiştirmek istersen
                            closeModal: true, //Modal kapatılsın mı, true, false
                        },
                        closeOnClickOutside: false, //Modal dışında tıklayınca kapansın mı true, false
                        closeOnEsc: false, //Modal ESC ile kapansın mı true, false
                        dangerMode: true, //Buton rengi kırmızıya döner true, false
                        //timer: 13000, //Belli bir süre sonra otomatik kapanır. (ms cinsinden)
                        icon: "success",
                    });

                    $('#sure').hide();
                }, 1000)
            }

        },

        cevapFind(id, dogru, soru_id) {
            if (this.secilenSiklar[soru_id] !== undefined) {
                if (dogru === 1) {
                    return 'inverted inverted-tertiary';
                }else if (this.secilenSiklar[soru_id] === id){
                    return 'inverted inverted-secondary';
                }
            }
        },

        dogruSayisiniBul(soru_puani) {
            //Eğer tüm sorular çözülmüşse ve hepsi doğru ise toplam puanı 100 olarak düzelt.
            if (this.soru_sayisi === Object.keys(this.secilenSiklar).length && this.dogrular.length === Object.keys(this.secilenSiklar).length) {
                this.toplamPuan = 100;
            } else {
                this.toplamPuan = (parseFloat(this.toplamPuan) + parseFloat(soru_puani)).toFixed(2);
            }

            //Eğer toplam puan 100 ü aşıyor ve kullanıcının yanlış cevap(lar) ı varsa
            if (this.toplamPuan > 100 && this.yanlislar.length > 0) {
                //toplam puandan soru ortalama puan değeri yanlış kadar çarpılarak düşülür
                this.toplamPuan -= (100 / this.soru_sayisi) * this.yanlislar.length;
            }
        },

        cevapSave(token, user_id, cevap) {
            //Tüm sorular doğru cevaplanmışsa
            if( (this.soru_sayisi === this.dogrular2.length) && (this.dogrular2.length === this.dogrular.length))
            {
                this.toplamPuan = 100;
            }

            axios.post('/sinav-cevap-save', {
                sinav_id: this.sinav_id,
                dogru: this.dogrular.length,
                yanlis: this.yanlislar.length,
                bos: this.soru_sayisi - (this.dogrular.length + this.yanlislar.length),
                puan: this.toplamPuan,
                sure: document.getElementById('sure').textContent,
                cevap_soru_ids: this.secilenSiklar,
                token: token,
                user_id: user_id,
                cevap_dogrumu : cevap.dogru,
                soru_id : cevap.soru_id
            });
        },

        isDisable(soru_id) {
            return this.secilenSiklar[soru_id];
        },

        setYorumGonder() {
            fr.preloademesaj = true;
            axios.post(document.location.origin+'/set/yorum-gonder', {
                kullanici: this.kullanici,
                user_id: this.userId,
                icerik: this.sinav_id,
                tur: 2, //2 sınav, 1 haber
                yorum: this.yorum,
                onay: 0,
                email: this.email
            }).then(response => {
                this.yorumresult = response.data;
                if (this.yorumresult.ok) {
                    this.yorumView = false;
                }
                setTimeout(() => {
                        fr.preloademesaj = false
                    }, 100
                )
            })
        },

        getSoruZorlukSeviyesi(soruPuan, soruSayisi) {

            let ortSoruPuan = 100 / parseInt(soruSayisi);  //5
            ortSoruPuan = parseFloat(ortSoruPuan).toFixed(2);

            let baremTolerans = 5;  // %5
            let soruPuanBaremi = parseInt(baremTolerans) / 100 * parseFloat(ortSoruPuan);  //0,25

            if ((parseFloat(ortSoruPuan) - parseFloat(soruPuanBaremi)).toFixed(2) >= parseFloat(soruPuan)) {
                return 'border-top-width: medium;border-top-style: solid;border-top-color:forestgreen;'; //4.75
            }

            if ((parseFloat(ortSoruPuan) - parseFloat(soruPuanBaremi) / 2).toFixed(2) >= parseFloat(soruPuan)) {
                return 'border-top-width: medium;border-top-style: solid;border-top-color:deepskyblue;'; //4.88
            }

            if (parseFloat(ortSoruPuan) >= parseFloat(soruPuan)) {
                return 'border-top-width: medium;border-top-style: solid;border-top-color:grey;'; //5
            }

            if ((parseFloat(ortSoruPuan) + parseFloat(soruPuanBaremi) / 2).toFixed(2) >= parseFloat(soruPuan)) {
                return 'border-top-width: medium;border-top-style: solid;border-top-color:coral;'; //5.13
            }

            if ((parseFloat(ortSoruPuan) + parseFloat(soruPuanBaremi)).toFixed(2) >= parseFloat(soruPuan)) {
                return 'border-top-width: medium;border-top-style: solid;border-top-color:#dc3545;'; // 5.25
            }

            return 'border-top-width: medium;border-top-style: solid;border-top-color:grey';
        },

        setHataliSoruBildir() {
            axios.post(document.location.origin + '/set/hatali-soru-bildir', {
                user_id: parseInt(this.userId),
                sinav_id: this.sinav_id,
                soru_id: this.soru_id,
                hata: this.soruHata
            }).then(response => {
                this.hataliSoruMesaj = response.data;
                if (this.hataliSoruMesaj.ok) {
                    this.dsply = false;
                    this.soruHata = null;
                }
            })
        },

        setSoruId(soru_id) {
            this.soru_id = soru_id;
        },
    }
});