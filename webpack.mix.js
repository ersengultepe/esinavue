let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
.browserSync('http://localhost:8000/');
   /* .combine([
        'public/js/app.js',
        'public/js/account/account-page.js',
        'public/js/account/edit-profile.js',
        'public/js/account/sidebar-left.js',
        'public/js/auth/login-register.js',
        'public/js/profile/user-profile.js',
        'public/js/sinav_sayfasi/sinav_values.js',
        // 'public/pa/HTML/assets/vendor/bootstrap/js/bootstrap.js',
        // 'public/pa/HTML/assets/vendor/jquery/jquery.js',
        // 'public/pa/HTML/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js',
        // 'public/pa/HTML/assets/vendor/bootstrap/js/bootstrap.js',
        /!*'public/pa/HTML/assets/vendor/nanoscroller/nanoscroller.js',
        'public/pa/HTML/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'public/pa/HTML/assets/vendor/magnific-popup/magnific-popup.js',
        'public/pa/HTML/assets/vendor/jquery-placeholder/jquery.placeholder.js',
        'public/pa/HTML/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js',
        'public/pa/HTML/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js',
        'public/pa/HTML/assets/vendor/jquery-appear/jquery.appear.js',
        'public/pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js',
        'public/pa/HTML/assets/vendor/flot/jquery.flot.js',
        'public/pa/HTML/assets/vendor/flot-tooltip/jquery.flot.tooltip.js',
        'public/pa/HTML/assets/vendor/flot/jquery.flot.pie.js',
        'public/pa/HTML/assets/vendor/flot/jquery.flot.categories.js',
        'public/pa/HTML/assets/vendor/flot/jquery.flot.resize.js',
        'public/pa/HTML/assets/vendor/jquery-sparkline/jquery.sparkline.js',
        'public/pa/HTML/assets/vendor/raphael/raphael.js',
        'public/pa/HTML/assets/vendor/morris/morris.js',
        'public/pa/HTML/assets/vendor/gauge/gauge.js',
        'public/pa/HTML/assets/vendor/snap-svg/snap.svg.js',
        'public/pa/HTML/assets/vendor/liquid-meter/liquid.meter.js',
        'public/pa/HTML/assets/vendor/ios7-switch/ios7-switch.js',
        '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
        'public/pa/HTML/assets/vendor/select2/select2.js',
        'public/pa/HTML/assets/vendor/jquery-maskedinput/jquery.maskedinput.js',
        'public/pa/HTML/assets/vendor/jquery-autosize/jquery.autosize.js',
        'public/pa/HTML/assets/javascripts/dashboard/examples.dashboard.js',
        'view.contact.js',
        'view.home.js',
        'custom.js',
        'theme.init.js',
        'theme.js',*!/
    ], 'public/merged.js');*/


