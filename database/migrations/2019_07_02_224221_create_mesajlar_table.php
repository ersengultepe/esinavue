<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMesajlarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mesajlar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from')->unsigned()->index();
            $table->integer('to')->unsigned()->index();
            $table->string('name',100);
            $table->string('body',300);
            $table->string('email',300);
            $table->ipAddress('ip');
            $table->enum('read', [1,0])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mesajlar');
    }
}
