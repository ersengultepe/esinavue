<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
 
    'facebook' => [
        'client_id' => '242614719760687',
        'client_secret' => '4eb6c6f9b02e4467fff007450dc16a5a',
        'redirect' => 'https://esinavsalonu.com/auth/callback/facebook',
    ],

    'twitter' => [
        'client_id' => '8w5Vh1MUnl6ub79VWgIhTSUzF',
        'client_secret' => 'SW7m3kP0lIB64p8jwGggorpZ2cehfqYkvTA1MPJUwT267Em9hq',
        'redirect' => 'https://esinavsalonu.com/auth/callback/twitter',
    ],

    'google' => [
        'client_id' => '102008307083-12en3e040b739emdj9ejnjucupn79f5o.apps.googleusercontent.com',
        'client_secret' => 'xf0wbGRCyOJ3Qy-wj52VgxKJ',
        'redirect' => 'https://esinavsalonu.com/auth/callback/google',
    ],

    'linkedin' => [
        'client_id' => 'xxxx',
        'client_secret' => 'xxx',
        'redirect' => 'https://esinavsalonu.com/auth/callback/linkedin',
    ],

    'github' => [
        'client_id' => 'xxxx',
        'client_secret' => 'xxx',
        'redirect' => 'https://esinavsalonu.com/auth/callback/github',
    ],

    'bitbucket' => [
        'client_id' => 'xxxx',
        'client_secret' => 'xxx',
        'redirect' => 'https://esinavsalonu.com/auth/callback/bitbucket',
    ],
];
