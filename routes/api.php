<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorizations, X-Auth-Token');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('uye/{link}', 'UserController@getUserProfilePage');
Route::get('hesap-yonetimi/{link}', 'UserController@getUserAccountManagement');

Route::post('set/ebulten', 'UserController@ebultenCheck');
Route::post('set/mesajGonder', 'UserController@mesajGonder');

/*Route::get('/sinav-infos/{url}', 'SinavController@getSinavGeneralInfos');
//Route::post('kac_kez_cozdun', 'SinavController@getKacKezCozdun');
Route::get('/sinav-success/{url}', 'SinavController@getSinavSuccessfulUsers');
Route::post('/sinav-cevap-save/', 'SinavController@setCevapSave');
Route::post('/sinav-cevap/', 'SinavController@getCevapCheck');*/
//Route::post('set/soru-dogru-yanlis-sayisi/', 'SinavController@setSoruDogruYanlisSayisi')->name('setSoruDogruYanlisSayisi');

/*Route::get('get/sehirler', 'UserController@sehirler')->name('sehirler');
Route::get('get/ilceler/{sehir_id}', 'UserController@ilceler')->name('ilceler');
Route::get('get/meslekler', 'UserController@meslekler')->name('meslekler');
Route::get('get/takimlar', 'UserController@takimlar')->name('takimlar');
Route::get('get/sinavlar', 'UserController@sinavlar')->name('sinavlar');
Route::get('get/yorumlar/{id}', 'SinavController@getYorumlar')->name('yorumlar');*/

/*
Route::post('set/yorum-gonder', 'SinavController@setYorumGonder');
Route::post('set/hatali-soru-bildir', 'SoruController@setHataliSoruBildir');*/
