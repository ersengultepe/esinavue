<div class="row mt-5">
    <div class="col">
        <div class="heading heading-border heading-bottom-border">
            <h2 class="mb-2 word-rotator-title">
                Tüm
                <strong class="inverted">
                    <span class="word-rotator active" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                        <span class="word-rotator-items" style="width: 142.95px; transform: translate3d(0px, -126px, 0px); transition: transform 300ms ease 0s, width 300ms ease 0s;">
                            <span>KPSS</span>
                            <span>YDS</span>
                            <span>Özel Güvenlik</span>
                            <span>Ehliyet</span>
                            <span>İş Sağlığı ve Güvenliği</span>
                            <span>Asli Öğretmenliğe Geçiş</span>
                        </span>
                    </span>
                </strong>
                Sınavları.
            </h2>
        </div>
        <div class="content-grid mt-4 mb-4">
            <div class="row content-grid-row">
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/ozel-guvenlik.png') }}" alt="">
                </div>
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/ehliyet.png') }}" alt="">
                </div>
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/is-sagligi.png') }}" alt="">
                </div>
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/asli-ogretmenlik.png') }}" alt="">
                </div>
            </div>
            <div class="row content-grid-row">
                <div class="content-grid-item col-lg-3 text-center">

                </div>
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/kpss.png') }}" alt="">
                </div>
                <div class="content-grid-item col-lg-3 text-center">
                    <img class="img-fluid siyah-beyaz" src="{{ asset('img/logos/yds.png') }}" alt="">
                </div>
                <div class="content-grid-item col-lg-3 text-center">

                </div>

            </div>
        </div>

        <hr class="tall">

    </div>
</div>