@extends('admin.template')

@section('content')
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-danger">
                    <a href="{{ route('getSoruEkle') }}" style="color: white;text-decoration-line: none">  <i class="fa fa-hand-o-left"></i>
                    Geri Dön </a>
                </button>
                Ayıklanan Soruları İncele
            </h2>
        </header>
        <div class="panel-body">
            @if(count($sorular) > 0)
            <form class="form-horizontal form-bordered" method="post" action="{{ route('insertSoruEkle') }}">
                @csrf
                <input type="hidden" name="sinav_mi" class="type" value="{{ $sinav_mi }}">
                <input type="hidden" name="soru_kategori" class="type" value="{{ $soru_kategori}}">
                <input type="hidden" name="soru_kaynak_id" class="type" value="{{ $soru_kaynak_id }}">
                <br>
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-lg btn-block">
                    <i class="fa fa-spinner"></i>
                    Soruları Kaydet
                </button>
                <?php $i = 1; $abcde = ['A', 'B', 'C', 'D', 'E']; $j = 0; ?>

                @for($f=0; $f<count($sorular); $f++)
                    <div class="col-md-6">
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary"> Soru {{  ' '.$i }}</button>
                        <textarea name="soru[]" class="ckeditor" id="soru_{{ $f }}" rows="2">{!! htmlspecialchars_decode($sorular[$f]) !!}</textarea>
                            @for($t=0; $t<count($cevaplar[$f]); $t++)
                            <div class="form-group {{ dogruCevapAddClass(htmlspecialchars_decode($cevaplar[$f][$t])) }}">
                                    <label class="col-md-1" for="cevap{{ $f.$j }}">{{ $abcde[$j].') ' }}</label>
                                <div class="col-md-11">
                                    <input name="cevap[{{$f}}][]" class="form-control" type="text" size="90" id="cevap{{ $f.$j }}"
                                           value="{!! htmlspecialchars_decode($cevaplar[$f][$t]) !!}" >
                                </div>
                            </div>
                                <?php $j++; ?>
                            @endfor
                                <?php $j = 0; ?>
                    </div>
                    @if($i%2==0)
                            <div class="col-md-12">
                                <hr style="font-size: 3em">
                            </div>
                    @endif
                        <?php $i++; ?>
                @endfor
                <br>
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary btn-lg btn-block">
                    <i class="fa fa-spinner"></i>
                    Soruları Kaydet
                </button>
            </form>
            @else
                <div class="col-md-12">
                    <h4>Listelenecek Soru Bulunamadı..!</h4>
                </div>
            @endif
        </div>
    </section>
@stop

