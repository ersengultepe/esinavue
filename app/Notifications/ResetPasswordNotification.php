<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class ResetPasswordNotification extends Notification
{
    use Queueable;

    protected $token;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Merhaba')
            ->subject(Lang::getFromJson('Şifre Sıfırlama Bağlantısı | EsinavSalonu.com'))
            ->line(Lang::getFromJson('Hesabınız için şifre sıfırlama isteğinde bulunduğunuz için bu e-postayı alıyorsunuz.'))
            ->action(Lang::getFromJson('Şifre Sıfırlama'), url(config('app.url').route('password.reset', $this->token, false)))
            ->line(Lang::getFromJson('Şifre sıfırlama isteğinde bulunmadıysanız, başka bir işlem yapmanız gerekmez.'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
