<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SinavTip extends Model
{
    protected $table = 'sinav_tips';
    protected $fillable = [
        'sinav_tip',
        'link',
        'logo',
        'soru_kategori_ids',
        'sinav_kategori_ids',
        'durum',
        'ekleyen',
        'ip'
    ];
}
