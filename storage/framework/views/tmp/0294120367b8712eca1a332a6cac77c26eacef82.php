<div class="container" >

    <div class="pricing-table row no-gutters mt-2 mb-2">
        <div class="col-lg-4">
            <div class="plan">
                <h3>SORU ÇÖZENLER<span><i class="fas fa-edit"></i></span></h3>

                <ul class="simple-post-list">

                    <?php $__currentLoopData = $soruCozenUyeler; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uye): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>">
                                        <img src="<?php echo e(userPhoto($uye->foto)); ?>" alt="<?php echo e(userTitle($uye->kullanici)); ?>" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>" style="float:left;" class="linka"><?php echo e(userTitle($uye->kullanici)); ?></a></div><br>
                            <a href="<?php echo e(sinavUrl($uye->link)); ?>" class="text-primary linka" data-toggle="tooltip" title="<?php echo e($uye->adi); ?>" data-original-title="<?php echo e($uye->adi); ?>">
                                <div class="progress mb-2" style="">
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemax="100" aria-valuemin="20" aria-valuenow="<?php echo e($uye->puan); ?>" style="width: <?php echo e($uye->puan); ?>%;">
                                        <?php echo e($uye->puan); ?>

                                    </div>
                                </div>
                            </a>


                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="plan">
                <h3>YENİ ÜYELER<span><i class="far fa-user"></i></span></h3>

                <ul class="simple-post-list" >

                    <?php $__currentLoopData = $yeniUyeler; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uye): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>" >
                                        <img src="<?php echo e(userPhoto($uye->foto)); ?>" alt="<?php echo e(userTitle($uye->kullanici)); ?>" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>" style="float: left;" class="linka"><?php echo e(userTitle($uye->kullanici)); ?></a></div><br>
                            <div style="float: left">
                                <?php echo e($uye->sehir); ?>

                            </div>

                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="plan">
                <h3>Puan Durumu<span><i class="fas fa-list-ol"></i></span></h3>

                <ul class="simple-post-list" id="mimg">

                    <?php $__currentLoopData = $puanDurumu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $uye): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li >
                            <div class="post-image">
                                <div class="img-thumbnail d-block">
                                    <a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>">
                                        <img src="<?php echo e(userPhoto($uye->foto)); ?>" alt="<?php echo e(userTitle($uye->kullanici)); ?>" style="height: 50px;width: 50px">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info"><a href="<?php echo e(userSlug($uye->kullanici, $uye->id)); ?>" style="float: left;" class="linka"><?php echo e(userTitle($uye->kullanici)); ?></a></div><br>
                            <div style="float: left">
                                <?php echo e($uye->puan); ?>

                            </div>

                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </ul>
            </div>
        </div>
    </div>

</div>