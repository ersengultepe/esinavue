<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EbultenRegistered extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Tebrikler')
            ->subject('EsinavSalonu.com | Ebülten Aboneliğiniz Onaylandı..!')
            ->line("Bu epostayı Esinavsalonu.com sitesinin e-bültenlerine aboneliğinizi onayladığınız için almaktasınız.")
            ->line("KPSS, ÖSYM, Ehliyet vb. Sınavlar hakkında bilgiler alacak, yeni eklenen sınavlarımızdan habersiz kalmayacaksınız.")
            ->action('Sınav Ekranı',  route('ekran'))
            ->line('Tüm soru ve önerileriniz için [Sık Sorulan Sorular](http://www.esinavsalonu.com/sss) sayfamızdan destek alabilirsiniz.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
