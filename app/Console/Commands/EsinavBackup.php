<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class EsinavBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'esinav:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'esinavsalonu.com veritabanı ve dosyalarının yedeğini alır';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Esinavsalonu.com yedeği hazırlanıyor...');

        try{
                //veritabanı yedeğini drive a yükleyelim
                $filename = 'dbackup_esinavsalonu_'.date('d.m.Y').'.sql.gz';
                $filePath = base_path('storage').'/'.$filename;
                Storage::cloud()->put($filename, fopen($filePath, 'r+'));
                Log::info('Veritabanı Yedeğini Buluta Yükleme', ['Durum'=> 'Yüklendi']);
                $this->info('Veritabanı Yedeğini Buluta Yükleme işlemi tamamlandı...');

                //site dosyalarının yedeğini drive a yükleyelim
                $filename = 'files_backup_'.date('d.m.Y').'.tar.gz';
                $filePath = base_path('storage').'/'.$filename;
                Storage::cloud()->put($filename, fopen($filePath, 'r+'));
                Log::info('Site Dosyalarının Yedeğini Buluta Yükleme', ['Durum'=> 'Yüklendi']);
                $this->info('Site Dosyalarının Yedeğini Buluta Yükleme İşlemi Tamamlandı...');

                //Buluttan 3 gün önceki yedekleri silelim
                cloudFileDeleteFromName('dbackup_esinavsalonu_'.date('d.m.Y', strtotime('-3 days')).'.sql.gz');
                cloudFileDeleteFromName('files_backup_'.date('d.m.Y', strtotime('-3 days')).'.tar.gz');
                Log::info('Buluttan Eski Yedekleri Silme', ['Durum'=> 'Silindi']);
                $this->info('Buluttan Eski Yedekleri Silme İşlemi Tamamlandı...');

                //Buluta gönderilen yedek dosyaları sunucudan sil
                unlink(base_path('storage').'/files_backup_'.date('d.m.Y').'.tar.gz');
                unlink(base_path('storage').'/dbackup_esinavsalonu_'.date('d.m.Y').'.sql.gz');
                Log::info('Sunucudan Eski Yedekleri Kaldırma', ['Durum'=> 'Kaldırıldı']);
                $this->info('Sunucudan Eski Yedekleri Kaldırma İşlemi Tamamlandı...');

        }catch(\Exception $e){
            Log::error('Yedekleme Hatası', ['Yedekler Google Drive\'a atılamadı'=> 'Mesaj..:'.$e->getMessage().' Satır..:'.$e->getLine()]);
        }

        $this->info('Esinavsalonu.com başarıyla yedeklendi...');

        return $this;
    }
}
