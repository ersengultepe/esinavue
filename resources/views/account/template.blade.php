<!doctype html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>User Profile | Okler Themes | Porto-Admin</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('css/app.css') }}"></script>
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/magnific-popup/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}" />
    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/morris/morris.css') }}" />
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/select2/select2.css') }}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/theme.css') }}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/skins/default.css') }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/stylesheets/theme-custom.css') }}">

    <!-- Head Libs -->
    <script src="{{ asset('pa/HTML/assets/vendor/modernizr/modernizr.js') }}"></script>

    <!-- Toaster -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <!-- VueJS -->

</head>
<body>
<section class="body" >

    <!-- start: header -->
    <header class="header">
        <!-- start: Logo -->
    @include('account.logo')
    <!-- end: Logo -->

        <!-- start: search & user box -->
        @includeWhen(Auth::check(), 'account.header')
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div id="account-page" class="inner-wrapper" >

        @auth()
            @include('account.sidebar')
        @endauth

        @guest()
            @include('profile.sidebar-reklam')
        @endguest

        <section role="main" class="content-body">

            <!-- start: breadcrumbs -->
            @include('account.breadcrumbs')
            <!-- end: breadcrumbs -->

            <!-- start: page -->
            @yield('content')
            <!-- end: page -->
        </section>
    </div>
    <!-- start: sidebar-right -->
    @include('account.sidebar-right')
    <!-- end: sidebar-right -->
</section>


<!-- Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
<!-- Specific Page Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.categories.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/raphael/raphael.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/morris/morris.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/gauge/gauge.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/snap-svg/snap.svg.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/ios7-switch/ios7-switch.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('pa/HTML/assets/vendor/select2/select2.js') }}"></script>
<script src="{{ asset('pa/HTML/assets/vendor/jquery-maskedinput/jquery.maskedinput.js') }}"></script>

<!-- Specific Page Vendor -->
<script src="{{ asset('pa/HTML/assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>

<!-- Theme Base, Components and Settings -->
<script src="{{ asset('pa/HTML/assets/javascripts/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('pa/HTML/assets/javascripts/theme.custom.js') }}"></script>
<!-- Theme Initialization Files -->
<script src="{{ asset('pa/HTML/assets/javascripts/theme.init.js') }}"></script>
<!-- Examples -->
<script src="{{ asset('pa/HTML/assets/javascripts/dashboard/examples.dashboard.js') }}"></script>
@yield('js')
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false
    };

            @if(Session::has('message'))
    let type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif

    @if(Session::has('swal'))
    swal ( "Oops" ,  "Something went wrong!" ,  "error" );
    @endif
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40136905-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-40136905-8');
</script>

</body>
</html>