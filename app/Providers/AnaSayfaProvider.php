<?php

namespace App\Providers;

use App\Http\Controllers\SinavController;
use App\Http\Controllers\SoruController;
use App\Http\Controllers\UserController;
use App\Models\Sinav;
use App\Models\SinavTip;
use Illuminate\Support\ServiceProvider;

class AnaSayfaProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('front.index', function($view){
                $sinav = new SinavController();
                $enCokCozulenSinavlar = $sinav->getEnCokCozulenSinavlar();
                $yeniEklenenSinavlar = $sinav->getYeniEklenenSinavlar();
                $sinavSayisi = $sinav->getSinavSayisi();

                $soru = new SoruController();
                $soruSayisi = $soru->getSoruSayisi();
                $cozulenSoruSayisi = $soru->getCozulenSoruSayisi();

                $uye = new UserController();
                $uyeSayisi = $uye->getUyeSayisi();
                $soruCozenUyeler = $uye->getSoruCozenUyeler();
                $yeniUyeler = $uye->getYeniUyeler();
                $puanDurumu = collect($uye->getGenerateGenelBasariSirasi())->take(10);
                $enCokSinavCozenUye = $uye->getEnCokSinavCozenUye();
                $enCokSoruCozenUye = $uye->getEnCokSoruCozenUye($enCokSinavCozenUye->id);
                $enCokPuanAlanUye = $uye->getEnCokPuanAlanUye($enCokSoruCozenUye->id, $enCokSinavCozenUye->id);
                $enYuksekOrtalamaPuanliUye = $uye->getEnYuksekOrtalamaPuanliUye($enCokSoruCozenUye->id, $enCokSinavCozenUye->id,$enCokPuanAlanUye->id);

                $sinavTipleri = SinavTip::where('durum', 1)->orderBy('sinav_tip')->get();

                $view->with([
                    'enCokCozulenSinavlar' => $enCokCozulenSinavlar,
                    'yeniEklenenSinavlar' => $yeniEklenenSinavlar,
                    'soruSayisi' => $soruSayisi,
                    'cozulenSoruSayisi' => $cozulenSoruSayisi,
                    'sinavSayisi' => $sinavSayisi,
                    'uyeSayisi' => $uyeSayisi,
                    'soruCozenUyeler' => $soruCozenUyeler,
                    'yeniUyeler' => $yeniUyeler,
                    'puanDurumu' => $puanDurumu,
                    'enCokSinavCozenUye' => $enCokSinavCozenUye,
                    'enCokSoruCozenUye' => $enCokSoruCozenUye,
                    'enCokPuanAlanUye' => $enCokPuanAlanUye,
                    'enYuksekOrtalamaPuanliUye' => $enYuksekOrtalamaPuanliUye,
                    'sinavTipleri' => $sinavTipleri,
                ]);
            });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
