<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meslek extends Model
{
    protected $table = 'mesleks';

    protected $fillable = [
        'meslek'
    ];
}
