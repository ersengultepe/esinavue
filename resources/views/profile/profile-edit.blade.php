<div id="edit" class="tab-pane">

    <form class="form-horizontal" method="get">
        <h4 class="mb-xlg">Kişisel Bilgiler</h4>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileFirstName">Adınız</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileFirstName">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileLastName">Soyadınız</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileLastName">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileAddress">Doğum Tarihiniz</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileAddress">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileCompany">Cinsiyetiniz</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileCompany">
                </div>
            </div>
        </fieldset>
        <hr class="dotted tall">
        <h4 class="mb-xlg">Senin Hakkında</h4>
        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileBio">Kendini Biraz Anlat</label>
                <div class="col-md-8">
                    <textarea class="form-control" rows="3" id="profileBio"></textarea>
                </div>
            </div>
            <div class="switch switch-sm switch-dark">
                <div class="ios-switch on"><div class="on-background background-fill"></div><div class="state-background background-fill"></div><div class="handle"></div></div><input name="switch" data-plugin-ios-switch="" checked="checked" style="display: none;" type="checkbox">
            </div>
        </fieldset>
        <hr class="dotted tall">
        <h4 class="mb-xlg">Change Password</h4>
        <fieldset class="mb-xl">
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileNewPassword">New Password</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileNewPassword">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="profileNewPasswordRepeat">Repeat New Password</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="profileNewPasswordRepeat">
                </div>
            </div>
        </fieldset>
        <div class="panel-footer">
            <div class="row">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </div>

    </form>

</div>