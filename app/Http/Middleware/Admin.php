<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Ziyaretçi ise hata sayfasına yönlendirilecek
        if(Auth::guest()){
            return abort(404);
        }

        //Site üyesi ve yetkisi 1 ise admin sayfasına
        if(Auth::user()->yetki == 1){
            return $next($request);
        }

        //değilse hata sayfasına
        return abort(404);

    }
}
