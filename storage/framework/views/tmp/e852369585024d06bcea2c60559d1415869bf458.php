<div class="col-lg-12">
    <div class="toggle toggle-secondary" data-plugin-toggle="">
        <section class="toggle">
            <label><?php echo e($kategori->kategori_adi); ?></label>
            <div class="toggle-content " style="display: none">

                <div class="col-lg-12">

                    <ol class="list list-ordened list-ordened-style-2">
                        <?php $__currentLoopData = $sinavlar->where('kategori_id', $kategori->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sinav): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a href="<?php echo e(route('sinav',[$sinav->link])); ?>"
                                   data-toggle="tooltip" data-placement="top" title=""
                                   data-original-title="">
                                    <?php if($sinav->created_at <> null and \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($sinav->created_at)) < 10): ?>
                                        <span class="badge badge-primary">Yeni </span>
                                    <?php endif; ?> &nbsp;<?php echo e($sinav->adi); ?>

                                </a><span style=""><?php echo e($sinav->cozum_sayisi); ?>

                                    kez çözüldü</span>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ol>

                </div>

            </div>
        </section>
    </div>
</div>