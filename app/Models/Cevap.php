<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cevap extends Model
{
    protected $table    =   'cevaps';

    protected $fillable = [
        'soru_id', 'cevap','dogru','durum','ekleyen'
    ];

    public function soru()
    {
        return $this->belongsTo(Soru::class);
    }
}
