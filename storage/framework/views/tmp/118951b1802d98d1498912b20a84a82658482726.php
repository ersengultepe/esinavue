<div class="logo-container">
    <a href="../" class="logo">
        <img src="<?php echo e(asset('img/assets/logo.png')); ?>" height="35" alt="EsinavSalonu.Com" />
    </a>
    <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
</div>