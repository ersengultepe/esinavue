<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoruKaynaklariTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soru_kaynaklari', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('ad', 500);
            $table->enum('telif', [0,1])->default(1)->index();
            $table->date('tarih')->default('2000-01-01');
            $table->string('duzenleyen_kurum', 500)->nullable();
            $table->string('duzenlenen_kurum', 500)->nullable();
            $table->mediumInteger('user_id')->default(1)->index();
            $table->unsignedSmallInteger('soru_sayisi')->nullable()->default(100);
            $table->ipAddress('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soru_kaynaklari');
    }
}
