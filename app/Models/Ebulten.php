<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ebulten extends Model
{
    protected $table = 'ebultens';

    protected $fillable = [
        'eposta',
        'durum'
    ];


}
