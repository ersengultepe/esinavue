<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Sinav extends Model
{
    protected $table    =   'sinavs';

    protected $fillable = [
        'kategori_adi',
        'link',
        'kategori_id',
        'ekleyen',
        'tarih',
        'ip',
        'durum',
    ];

    protected $hidden = [
        'ekleyen',
        'ip',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'ekleyen', 'id');
    }

    public function kategoriler()
    {
        return $this->hasMany(SinavKategori::class, 'id', 'kategori_id');
    }
}
