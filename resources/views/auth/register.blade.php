@extends('front.template')

@section('title')
    <title>{{ config('app.name').' | '.config('app.slogan') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
@stop

@section('css')
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme-blog.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme-shop.css') }}">
@stop

@section('content')
    @include('front.part.header')
    <div role="main" class="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active">Sınav Ekranı </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Sınav Ekranı</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col">

                    <div class="featured-boxes">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="featured-box featured-box-primary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">ÜYE GİRİŞİ</h4>
                                        <form action="{{ route('login') }}" id="frmSignIn" method="POST" role="form">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email Adresi</label>
                                                    <input type="text" value="" class="form-control form-control-lg" name="email" id="email">
                                                    <label for="email" class="badge badge-danger text-light badge-md">{{ $errors->first('email') }}</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <a class="float-right" href="#">(Şifrenizi Unuttunuz mu?)</a>
                                                    <label>Şifre</label>
                                                    <input type="password" value="" class="form-control form-control-lg" name="password" id="password">
                                                    <label for="password" class="badge badge-danger text-light badge-md">{{ $errors->first('password') }}</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" id="rememberme" name="rememberme"> Beni Hatırla
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <input type="submit" value="Giriş" class="btn btn-primary float-right mb-5" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="featured-box featured-box-full-secondary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">KAYIT OL</h4>
                                        <form action="{{ route('register') }}" id="frmSignUp" method="post">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>Ad</label>
                                                    <input id="ad" type="text" value="" class="form-control form-control-lg" name="ad" maxlength="25">
                                                    <label for="ad" class="badge badge-danger text-light badge-md">{{ $errors->first('ad') }}</label>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Soyad</label>
                                                    <input id="soyad" type="text" value="" class="form-control form-control-lg" name="soyad" maxlength="40">
                                                    <label for="soyad" class="badge badge-danger text-light badge-md">{{ $errors->first('soyad') }}</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email Adresi</label>
                                                    <input id="email" type="email" value="" class="form-control form-control-lg" name="email" maxlength="80">
                                                    <label for="email" class="badge badge-danger text-light badge-md">{{ $errors->first('email') }}</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>Şifre</label>
                                                    <input id="password" type="password" value="" class="form-control form-control-lg" name="password" maxlength="12" minlength="6">
                                                    <label for="password" class="badge badge-danger text-light badge-md">{{ $errors->first('password') }}</label>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Tekrar Şifre</label>
                                                    <input id="password_confirm" type="password" value="" class="form-control form-control-lg" name="password_confirmation" maxlength="12" minlength="6">
                                                    <label for="password_confirm" class="badge badge-danger text-light badge-md">{{ $errors->first('password_confirmation') }}</label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="submit" value="Kayıt Ol" class="btn btn-primary float-right mb-5" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')

    <!-- Vendor -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.appear/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-cookie/jquery-cookie.min.js') }}"></script>
    <script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/common/common.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.validation/jquery.validation.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('vendor/vide/vide.min.js') }}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('js/theme.js') }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset('js/custom.js') }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset('js/theme.init.js') }}"></script>

@stop