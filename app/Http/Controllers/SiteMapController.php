<?php

namespace App\Http\Controllers;

use App\Models\Sinav;
use App\Models\SinavTip;
use App\User;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use Illuminate\Support\Facades\Log;

class SiteMapController extends Controller
{
    public function sitemap()
    {
        //sitemap ana sayfa
        $sitemap = Sitemap::create();
        for($i=1; $i<5; $i++) $sitemap->add(Url::create("/sitemap".$i.".xml"));
        $sitemap->writeToFile(public_path('sitemap.xml'));
        Log::info('sitemap', ['Sitemap Ana Sayfa' => 'Sitemap Ana Sayfa başarıyla oluşturuldu.']);
    }

    public function sitemap1()
    {
        //sitemap statik sayfalar
        $sitemap = Sitemap::create()
        ->add(Url::create("/"))
        ->add(Url::create("/logout"))
        ->add(Url::create("/cikis"))
        ->add(Url::create("/login"))
        ->add(Url::create("/giris"))
        ->add(Url::create("/register"))
        ->add(Url::create("/kayit"))
        ->add(Url::create("/sik-sorulan-sorular"))
        ->add(Url::create("/sinav-ekrani"));
        $sitemap->writeToFile(public_path('sitemap1.xml'));
        Log::info('sitemap1', ['Anasayfa' => 'Statik sayfalar için site haritası başarıyla oluşturuldu.']);
    }

    public function sitemap2()
    {
        //Sınav tipleri
        $sitemap = Sitemap::create();
        SinavTip::where('durum',1)->each(function (SinavTip $sinavTip) use ($sitemap) {
            $sitemap->add(Url::create("/sinavlar/{$sinavTip->link}"));
        });
        $sitemap->writeToFile(public_path('sitemap2.xml'));
        Log::info('sitemap2', ['Sınav tipleri' => 'Sınav tipleri için site haritası başarıyla oluşturuldu.']);
    }

    public function sitemap3()
    {
        //Sınavlar
        $sitemap = Sitemap::create();
        Sinav::where('durum',1)->each(function (Sinav $sinav) use ($sitemap) {
            $sitemap->add(Url::create("/sinav/{$sinav->link}"));
        });
        $sitemap->writeToFile(public_path('sitemap3.xml'));
        Log::info('sitemap3', ['Sınavlar' => 'Sınavlar için site haritası başarıyla oluşturuldu.']);
    }

    public function sitemap4()
    {
        //üyeler
        $sitemap = Sitemap::create();
        User::where('durum',1)->where('id', '>', 1)->select('kullanici','id')->orderBy('created_at', 'ASC')->take(50000)->each(function (User $user) use ($sitemap) {
            $sitemap->add(Url::create("/uye/".$user->id.'-'.str_slug($user->kullanici)));
        });
        $sitemap->writeToFile(public_path('sitemap4.xml'));
        Log::info('sitemap4', ['Üyeler' => 'Üyeler için site haritası başarıyla oluşturuldu.']);
    }

}
