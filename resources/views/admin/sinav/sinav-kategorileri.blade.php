@extends('admin.template')

@section('css')
    <link rel="stylesheet" href="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }}" />
    @stop

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Kayıtlı Tüm Sınav Kategorilerimiz</h2>
                <small>Toplam {{ $kategoriler->count() }} Sınav</small>
                <br>

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Başarılı</strong> {{ Session::get('success') }}.
                    </div>
                    @endif

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Üzgünüm..:( </strong> {{ Session::get('error') }}.
                    </div>
                    @endif

            </header>
            <div class="panel-body">
                <a href="{{ url()->previous() }}"><button class="btn btn-success" >Geri Dön</button></a>
                <a href="{{ route('getKatSinavInsert') }}"><button class="btn btn-danger">Yeni Ekle</button></a>
                <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                    <thead>
                    <tr>
                        <th>Sıra No</th>
                        <th>ID</th>
                        <th>Sınav Kategori Adı</th>
                        <th>Mevcut Sınav Sayısı</th>
                        <th>Eklenme Tarihi</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($kategoriler as $kat)
                    <tr class="gradeX">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $kat->id }}</td>
                        <td><a href="{{ route('getKatSinavlar', ['id'=>$kat->id]) }}">
                                {{ str_limit($kat->kategori_adi) }}
                            </a>
                        </td>
                        <td>{{ $kat->sinavlar->count() }}</td>
                        <td>{{ $kat->created_at }}</td>
                        <td class="actions">
                            <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
                            <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>
                            <a href="{{ route('getKatSinavEdit', ['id'=>$kat->id]) }}" class="on-default edit-row"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>
                            <a href="{{ route('insertSinavs', ['id' => $kat->id]) }}" class="on-default insert-sinav"><i class="fa fa-spin fa-cog"></i></a>
                        </td>
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
        </section>
    </div>
@stop

@section('js')
    <script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('pa/HTML/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/tables/sinav.kat.list.datatables.editable.js') }}"></script>
    <script src="{{ asset('/pa/HTML/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    @stop