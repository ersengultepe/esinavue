<?php

namespace App\Http\Controllers;

use App\Models\Yorum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class YorumController extends Controller
{
    public function index()
    {
        //Onaylı Yorumlar
        $yorumlar = Yorum::where('onay', 1)->where('user_id', '!=', 1)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Onaylanan Yorumlar');
        return view('admin.yorum.index', compact('yorumlar'));
    }

    public function onaysizYorumlar()
    {
        //Onaysız Yorumlar
        $yorumlar = Yorum::where('onay', 0)->where('user_id', '!=', 1)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Onay Bekleyen Yorumlar');
        return view('admin.yorum.index', compact('yorumlar'));
    }

    public function silinenYorumlar()
    {
        //Silinen Yorumlar
        $yorumlar = Yorum::where('onay', 2)->where('user_id', '!=', 1)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Silinen Yorumlar');
        return view('admin.yorum.index', compact('yorumlar'));
    }

    public function ajaxYorumOnayUpdate(Request $request)
    {
        try{
            Yorum::findOrFail($request->yorum_id)
                ->update([
                   'onay' => $request->onay
                ]);

            switch ($request->onay){
                case 1 : $text = 'Onaylandı'; break;
                case 0 : $text = 'Bekleyenlere alındı'; break;
                case 2 : $text = 'Silindi'; break;
                default : $text = 'Onaylandı';
            }

            return ['title'=> 'Başarılı', 'text'=> 'Yorum Başarıyla '.$text ];

        }catch (\Exception $e){
            return false;
        }
    }
}
