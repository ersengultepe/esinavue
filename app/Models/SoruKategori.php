<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoruKategori extends Model
{
    protected $table = 'soru_kategoris';

    protected $fillable = [
        'kategori_adi',
        'ekleyen',
        'ip'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'ekleyen', 'id');
    }
}
