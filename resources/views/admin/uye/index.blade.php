@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">{{ Session::get('breadcrumb') }}</h2>
                <small>Toplam {{ Session::get('breadcrumb') }}: {{ $uyeler->total() }}</small>
                @if(request()->segment(3) === 'onaylananlar')
                    <span class="sayfa" id="sp_onaylananlar"></span>
                @endif
                @if(request()->segment(3) === 'onay-bekleyenler')
                    <span class="sayfa" id="sp_onay_bekleyenler"></span>
                @endif
                @if(request()->segment(3) === 'silinenler')
                    <span class="sayfa" id="sp_silinenler"></span>
                @endif
            </header>
            <div class="panel-body">
                {{ $uyeler->links() }}
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                        <tr >
                            <th>Sıra</th>
                            <th>ID</th>
                            <th>Fotoğraf</th>
                            <th>Cinsiyet</th>
                            <th>Üye Adı, Soyadı</th>
                            <th>eposta</th>
                            <th>Şehir</th>
                            <th>E-bülten</th>
                            <th>İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($uyeler as $uye)
                            <tr id="tr_{{ $uye->id }}">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $uye->id }}</td>
                                <td><img class="img-thumbnail" src="{{ asset($uye->foto) }}" style="width: 60px;height: 60px"></td>
                                <td><i style="font-size: 2em" class="fa {{ $uye->cinsiyet == 1 ? 'fa-male' : 'fa-female' }}"></i></td>
                                <td style="font-size: 1.3em">{{ $uye->kullanici }}</td>
                                <td>{{ $uye->email }}</td>
                                <td>{{ optional($uye->city)->sehir }}</td>
                                <td>{{ $uye->ebulten }}</td>
                                <td class="actions-hover actions-fade" width="200">
                                    <select class="form-control input-sm mb-md uye-onay" id="{{ $uye->id }}">
                                        <option value="1" @if($uye->durum === 1) selected @endif >Onayla</option>
                                        <option value="0" @if($uye->durum === 0) selected @endif >Beklet</option>
                                        <option value="3" @if($uye->durum === 3) selected @endif >Sil</option>
                                    </select>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            {{ $uyeler->links() }}
        </section>
    </div>
@stop


@section('js')
    <script>
        $('.uye-onay').change(function () {
            let durum = $(this).val();
            let user_id = this.id;

            $.ajax({
                type: "POST",
                url: document.location.origin + '/ajax/user/onay-update',
                data: {
                    'user_id': user_id,
                    'durum': durum
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });

                if($(".sayfa").attr('id') === 'sp_onaylananlar')
                {
                    $("#tr_"+user_id).fadeOut(2300);
                }
                if($(".sayfa").attr('id') === 'sp_onay_bekleyenler')
                {
                    $("#tr_"+user_id).fadeOut(2300);
                }
                if($(".sayfa").attr('id') === 'sp_silinenler')
                {
                    $("#tr_"+user_id).fadeOut(2300);
                }

            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Uye onayı güncellenemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });
    </script>
@stop