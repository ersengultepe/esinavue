<section class="section section-text-dark section-background section-center section-overlay-opacity section-overlay-opacity-light section-overlay-opacity-light-scale-9"
		 style="background-image: url({{ asset('img/custom-header-bg.jpg') }});">
	<div class="container">
			<div class="row mt-5 mb-5">
				<div class="col-sm-12">
					<div class="heading heading-border heading-bottom-border">
						<h3><strong>Son Zamanların </strong>En'leri</h3>
					</div>
				</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
									<span class="thumb-info thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper">
											<a href="{{ userSlug($enCokPuanAlanUye->kullanici, $enCokPuanAlanUye->id) }}">
												<img src="{{ userPhoto($enCokPuanAlanUye) }}" class="img-fluid haftanin-enleri" alt="{{ userTitle($enCokPuanAlanUye->kullanici) }}">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">{{ userTitle($enCokPuanAlanUye->kullanici) }}</span>
													<span class="thumb-info-type">{{ $enCokPuanAlanUye->meslek }}</span>
												</span>
											</a>
										</span>
										<span class="thumb-info-caption">
											<span class="thumb-info-caption-text">Son Zamanlarda En Çok Puan Alan Üyemiz</span>
											<span class="thumb-info-social-icons">
												<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
												<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
												<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
											</span>
										</span>
									</span>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-lg-0">
									<span class="thumb-info thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper">
											<a href="{{ userSlug($enCokSoruCozenUye->kullanici, $enCokSoruCozenUye->id) }}">
												<img src="{{ userPhoto($enCokSoruCozenUye) }}" class="img-fluid haftanin-enleri" alt="{{ userTitle($enCokSoruCozenUye->kullanici) }}">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">{{ userTitle($enCokSoruCozenUye->kullanici) }}</span>
													<span class="thumb-info-type">{{ $enCokSoruCozenUye->meslek }}</span>
												</span>
											</a>
										</span>
										<span class="thumb-info-caption">
											<span class="thumb-info-caption-text">Son Zamanlarda En Çok Soru Çözen Üyemiz</span>
											<span class="thumb-info-social-icons">
												<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
												<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
												<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
											</span>
										</span>
									</span>
					</div>
					<div class="col-sm-6 col-lg-3 mb-4 mb-sm-0">
									<span class="thumb-info thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper">
											<a href="{{ userSlug($enCokSinavCozenUye->kullanici, $enCokSinavCozenUye->id) }}">
												<img src="{{ userPhoto($enCokSinavCozenUye) }}" class="img-fluid haftanin-enleri" alt="{{ userTitle($enCokSinavCozenUye->kullanici) }}">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">{{ userTitle($enCokSinavCozenUye->kullanici) }}</span>
													<span class="thumb-info-type">{{ $enCokSinavCozenUye->meslek }}</span>
												</span>
											</a>
										</span>
										<span class="thumb-info-caption">
											<span class="thumb-info-caption-text">Son Zamanlarda En Çok Sınav Çözen Üyemiz</span>
											<span class="thumb-info-social-icons">
												<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
												<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
												<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
											</span>
										</span>
									</span>
					</div>
					<div class="col-sm-6 col-lg-3">
									<span class="thumb-info thumb-info-hide-wrapper-bg">
										<span class="thumb-info-wrapper">
											<a href="{{ userSlug($enYuksekOrtalamaPuanliUye->kullanici, $enYuksekOrtalamaPuanliUye->id) }}">
												<img src="{{ userPhoto($enYuksekOrtalamaPuanliUye) }}" class="img-fluid haftanin-enleri" alt="{{ userTitle($enYuksekOrtalamaPuanliUye->kullanici) }}">
												<span class="thumb-info-title">
													<span class="thumb-info-inner">{{ userTitle($enYuksekOrtalamaPuanliUye->kullanici) }}</span>
													<span class="thumb-info-type">{{ $enYuksekOrtalamaPuanliUye->meslek }}</span>
												</span>
											</a>
										</span>
										<span class="thumb-info-caption">
											<span class="thumb-info-caption-text">Son Zamanlarda En Yüksek Ortalama Puan Alan Üyemiz</span>
											<span class="thumb-info-social-icons">
												<a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
												<a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
												<a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
											</span>
										</span>
									</span>
					</div>
			</div>
	</div>
</section>
