<li class="dropdown"><a class="dropdown-item dropdown-toggle" href="#">
        Pages </a>
    <ul class="dropdown-menu">
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Contact
                Us</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item"
                       href="contact-us-advanced.php">Contact Us -
                        Advanced</a></li>
                <li><a class="dropdown-item" href="contact-us.html">Contact
                        Us - Basic</a></li>
                <li><a class="dropdown-item"
                       href="contact-us-recaptcha.html">Contact Us -
                        Recaptcha</a></li>
            </ul>
        </li>
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">About
                Us</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item"
                       href="about-us-advanced.html">About Us -
                        Advanced</a></li>
                <li><a class="dropdown-item" href="about-us.html">About
                        Us - Basic</a></li>
                <li><a class="dropdown-item" href="about-me.html">About
                        Me</a></li>
            </ul>
        </li>
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Layouts</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item"
                       href="page-full-width.html">Full Width</a></li>
                <li><a class="dropdown-item"
                       href="page-left-sidebar.html">Left Sidebar</a>
                </li>
                <li><a class="dropdown-item"
                       href="page-right-sidebar.html">Right Sidebar</a>
                </li>
                <li><a class="dropdown-item"
                       href="page-left-and-right-sidebars.html">Left and
                        Right Sidebars</a></li>
                <li><a class="dropdown-item"
                       href="page-sticky-sidebar.html">Sticky
                        Sidebar</a></li>
                <li><a class="dropdown-item"
                       href="page-secondary-navbar.html">Secondary
                        Navbar</a></li>
            </ul>
        </li>
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Extra</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="page-404.html">404
                        Error</a></li>
                <li><a class="dropdown-item" href="page-500.html">500
                        Error</a></li>
                <li><a class="dropdown-item"
                       href="page-coming-soon.html">Coming Soon</a></li>
                <li><a class="dropdown-item"
                       href="page-maintenance-mode.html">Maintenance
                        Mode</a></li>
                <li><a class="dropdown-item"
                       href="page-search-results.html">Search
                        Results</a></li>
                <li><a class="dropdown-item"
                       href="sitemap.html">Sitemap</a></li>
            </ul>
        </li>
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Team</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item"
                       href="page-team-advanced.html">Team -
                        Advanced</a></li>
                <li><a class="dropdown-item" href="page-team.html">Team
                        - Basic</a></li>
            </ul>
        </li>
        <li class="dropdown-submenu"><a class="dropdown-item" href="#">Services</a>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="page-services.html">Services
                        - Version 1</a></li>
                <li><a class="dropdown-item"
                       href="page-services-2.html">Services - Version
                        2</a></li>
                <li><a class="dropdown-item"
                       href="page-services-3.html">Services - Version
                        3</a></li>
            </ul>
        </li>
        <li><a class="dropdown-item" href="page-custom-header.html">Custom
                Header</a></li>
        <li><a class="dropdown-item"
               href="page-careers.html">Careers</a></li>
        <li><a class="dropdown-item" href="page-faq.html">FAQ</a></li>
        <li><a class="dropdown-item" href="page-login.html">Login /
                Register</a></li>
        <li><a class="dropdown-item" href="page-user-profile.html">User
                Profile</a></li>
    </ul>
</li>