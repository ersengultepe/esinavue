<?php $__env->startSection('content'); ?>
    <div class="row" id="profile">

        <div class="col-md-4 col-lg-3">

            <section class="panel">
                <div class="panel-body">
                    <div class="thumb-info mb-md">
                        <img :src="userPhoto(pVal.user.foto)" class="rounded img-responsive" :alt="pVal.user.kullanici" style="width: 100%">
                        <div class="thumb-info-title">
                            <span class="thumb-info-inner">{{pVal.user.kullanici }}</span>
                            <span class="thumb-info-type">{{ pVal.user.meslekY }}</span>
                        </div>
                    </div>

                    <div class="widget-toggle-expand mb-md">
                        <div class="widget-header">
                            <h6>Profil Tamamlama</h6>
                            <div class="widget-toggle">+</div>
                        </div>
                        <div class="widget-content-collapsed">
                            <div class="progress progress-xs light">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    60%
                                </div>
                            </div>
                        </div>
                        <div class="widget-content-expanded">
                            <ul class="simple-todo-list">
                                <li class="completed">Profil Resmi Güncellemesi</li>
                                <li class="completed">Kişisel Bilgiler Güncellemesi</li>
                                <li>Sosyal Medya Güncellemesi</li>
                                <li>Arkadaş Takibi</li>
                            </ul>
                        </div>
                    </div>

                    <hr class="dotted short">

                    <h6 class="text-muted">Hakkında</h6>
                    <p v-show="kendiHakkindaOzet">{{ pVal.user.kendi_hakkinda !== null ? pVal.user.kendi_hakkinda.substr(0,120)+'...' : null }}</p>
                    <p v-show="kendiHakkinda">{{ pVal.user.kendi_hakkinda }}</p>

                    <div class="clearfix">
                        <a class="text-uppercase text-muted pull-right" href="#" @click="kendiHakkindaClick()">{{ gosterGizle }}</a>
                    </div>

                    <hr class="dotted short">

                    <div class="social-icons-list">
                        <a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                        <a rel="tooltip" data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                        <a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
                    </div>

                </div>
            </section>


            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title">
                        <span class="label label-primary label-sm text-normal va-middle mr-sm">198</span>
                        <span class="va-middle">Arkadaşlar</span>
                    </h2>
                </header>
                <div class="panel-body">
                    <div class="content">
                        <ul class="simple-user-list">
                            <li>
                                <figure class="image rounded">
                                    <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Doe Junior" class="img-circle" height="35" width="35">
                                </figure>
                                <span class="title">Joseph Doe Junior</span>
                                <span class="message truncate">Lorem ipsum dolor sit.</span>
                            </li>
                            <li>
                                <figure class="image rounded">
                                    <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Junior" class="img-circle">
                                </figure>
                                <span class="title">Joseph Junior</span>
                                <span class="message truncate">Lorem ipsum dolor sit.</span>
                            </li>
                            <li>
                                <figure class="image rounded">
                                    <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joe Junior" class="img-circle">
                                </figure>
                                <span class="title">Joe Junior</span>
                                <span class="message truncate">Lorem ipsum dolor sit.</span>
                            </li>
                            <li>
                                <figure class="image rounded">
                                    <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Doe Junior" class="img-circle">
                                </figure>
                                <span class="title">Joseph Doe Junior</span>
                                <span class="message truncate">Lorem ipsum dolor sit.</span>
                            </li>
                        </ul>
                        <hr class="dotted short">
                        <div class="text-right">
                            <a class="text-uppercase text-muted" href="#">(View All)</a>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="input-group input-search">
                        <input type="text" class="form-control" name="q" id="q" placeholder="Search...">
                        <span class="input-group-btn">
											<button class="btn btn-default" type="submit"><i class="fa fa-search"></i>
											</button>
										</span>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>

                    <h2 class="panel-title">Popular Posts</h2>
                </header>
                <div class="panel-body">
                    <ul class="simple-post-list">
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail">
                                    <a href="#">
                                        <img src="<?php echo e(asset('pa/HTML/assets/images/post-thumb-1.jpg')); ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="#">Nullam Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2013
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail">
                                    <a href="#">
                                        <img src="<?php echo e(asset('pa/HTML/assets/images/post-thumb-2.jpg')); ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="#">Vitae Nibh Un Odiosters</a>
                                <div class="post-meta">
                                    Jan 10, 2013
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="post-image">
                                <div class="img-thumbnail">
                                    <a href="#">
                                        <img src="<?php echo e(asset('pa/HTML/assets/images/post-thumb-3.jpg')); ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="#">Odiosters Nullam Vitae</a>
                                <div class="post-meta">
                                    Jan 10, 2013
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

        </div>
        <div class="col-md-8 col-lg-6">
            <div class="tabs">
                <ul class="nav nav-tabs tabs-primary">
                    <li class="active">
                        <a href="#overview" data-toggle="tab">Genel Bakış</a>
                    </li>
                    <li>
                        <a href="#edit" data-toggle="tab">Hesap Bilgileri</a>
                    </li>
                </ul>
                <div class="tab-content">

                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-3">

            <h4 class="mb-md">Sınav İstatistikleri</h4>
            <ul class="simple-card-list mb-xlg">
                <li v-if="pVal.info.toplamPuan === '-' " class="danger">
                    <h3>Hiç Sınav Çözmedi</h3>
                </li>
                <li v-else class="primary">
                    <h3>{{ pVal.info.basariSirasi }}</h3>
                    <p>Başarı Sırası</p>
                </li>
                <li class="primary">
                    <h3>{{ pVal.info.toplamPuan }}</h3>
                    <p>Toplam Puan</p>
                </li>
                <li class="primary">
                    <h3>{{ pVal.info.ortalamaPuan }}</h3>
                    <p>Son 10 Sınav Ortalaması</p>
                </li>
                <li class="primary">
                    <h3>{{ pVal.info.cozulensinav }}</h3>
                    <p>Çözülen Sınav Sayısı</p>
                </li>
            </ul>

            <h4 class="mb-md">Projects</h4>
            <ul class="simple-bullet-list mb-xlg">
                <li class="red">
                    <span class="title">Porto Template</span>
                    <span class="description truncate">Lorem ipsom dolor sit.</span>
                </li>
                <li class="green">
                    <span class="title">Tucson HTML5 Template</span>
                    <span class="description truncate">Lorem ipsom dolor sit amet</span>
                </li>
                <li class="blue">
                    <span class="title">Porto HTML5 Template</span>
                    <span class="description truncate">Lorem ipsom dolor sit.</span>
                </li>
                <li class="orange">
                    <span class="title">Tucson Template</span>
                    <span class="description truncate">Lorem ipsom dolor sit.</span>
                </li>
            </ul>

            <h4 class="mb-md">Messages</h4>
            <ul class="simple-user-list mb-xlg">
                <li>
                    <figure class="image rounded">
                        <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Doe Junior" class="img-circle">
                    </figure>
                    <span class="title">Joseph Doe Junior</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                </li>
                <li>
                    <figure class="image rounded">
                        <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Junior" class="img-circle">
                    </figure>
                    <span class="title">Joseph Junior</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                </li>
                <li>
                    <figure class="image rounded">
                        <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joe Junior" class="img-circle">
                    </figure>
                    <span class="title">Joe Junior</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                </li>
                <li>
                    <figure class="image rounded">
                        <img src="<?php echo e(asset('pa/HTML/assets/images/!sample-user.jpg')); ?>" alt="Joseph Doe Junior" class="img-circle">
                    </figure>
                    <span class="title">Joseph Doe Junior</span>
                    <span class="message">Lorem ipsum dolor sit.</span>
                </li>
            </ul>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="<?php echo e(asset('js/profile/user-profile.js')); ?>"></script>
    <script src="<?php echo e(asset('js/account/sidebar-left.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('profile.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>