<?php $__env->startSection('title'); ?>
    <title>404 Sayfa Bulunamadı <?php echo e(config('app.name')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="404 Sayfa Bulunamadı">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-elements.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/theme-blog.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
     <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col">
								<ul class="breadcrumb">
									<li><a href="<?php echo e(url('/')); ?>">Anasayfa</a></li>
									<li class="active">404-Sayfa Bulunamadı</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<h1>404 - Sayfa Bulunamadı</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<section class="page-not-found">
						<div class="row justify-content-center">
							<div class="col-lg-7 text-center">
								<div class="page-not-found-main">
									<h2>404 <i class="fas fa-file"></i></h2>
									<p>Yanlış bir link yazdınız, böyle bir sayfa yok.</p>
								</div>
							</div>
							<div class="col-lg-4">
								<h4 class="heading-primary">Bunlar ilgilinizi çekebilir</h4>
								<ul class="nav nav-list flex-column">
									<li class="nav-item"><a class="nav-link" href="#">Anasayfa</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Sınav Ekranı</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Sık Sorulan Sorular</a></li>
									<li class="nav-item"><a class="nav-link" href="#">Site Haritası</a></li>
									<li class="nav-item"><a class="nav-link" href="#">İletişim</a></li>
								</ul>
							</div>
						</div>
					</section>

				</div>

			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>