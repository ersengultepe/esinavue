@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Kayıtlı Tüm Sınavlarımız</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sınav Adı</th>
                            <th>Eklenme Tarihi</th>
                            <th>İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($sinavlar as $sinav)
                            <tr id="tr_{{ $sinav->id }}">
                                <td>{{ $loop->iteration }}</td>
                                <td><a href="{{ route('getSinavSorulari', ['id'=>$sinav->id]) }}" >
                                        {{ str_limit($sinav->adi) }}
                                    </a>
                                </td>
                                <td>{{ $sinav->created_at }}</td>
                                <td class="actions-hover actions-fade">
                                    <a class="row-sinav" href="{{ route('getUpdate', ['id'=>$sinav->id]) }}" ><i class="fa fa-pencil"></i></a>
                                    <a href="#modalFullColorDanger{{ $sinav->id }}" class="delete-row modal-basic"><i class="fa fa-trash-o"></i></a>

                                    <div id="modalFullColorDanger{{ $sinav->id }}" class="modal-block modal-full-color modal-block-danger mfp-hide">
                                        <section class="panel">
                                            <header class="panel-heading">
                                                <h2 class="panel-title">Dikkat...!</h2>
                                            </header>
                                            <div class="panel-body">
                                                <div class="modal-wrapper">
                                                    <div class="modal-icon">
                                                        <i class="fa fa-times-circle"></i>
                                                    </div>
                                                    <div class="modal-text">
                                                        <h3>{!! $sinav->adi !!}</h3><br>
                                                        <h4>Bu sınav silinecektir, Onaylıyor musunuz ?</h4>
                                                        <small>Bu işlemin geri dönüşü olmayacaktır..!</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <footer class="panel-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <button class="btn btn-danger modal-confirm" id="{{ $sinav->id }}">Sil</button>
                                                        <button class="btn btn-default modal-dismiss">İptal</button>
                                                    </div>
                                                </div>
                                            </footer>
                                        </section>
                                    </div>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            {{ $sinavlar->links() }}
        </section>

    </div>
@stop

@section('js')
    <!-- Examples -->
    <script src="{{ asset('pa/HTML/assets/javascripts/ui-elements/examples.modals.js') }}"></script>
    <script>
        $(document).on('click', '.modal-confirm', function (e) {
        	e.preventDefault();
        	$.magnificPopup.close();

            let sinav_id = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: document.location.origin+'/ajax/sinav/delete',
                data:{
                    'sinav_id': sinav_id
                }
            }).done(function (data) {
                new PNotify({
                    title: data.title,
                    text: data.text,
                    type: 'success',
                    shadow: true
                });

                //Silinen kaydı gridden kaldır
                $('#tr_'+sinav_id).fadeOut(1500);

            }).fail(function () {
                new PNotify({
                    title: 'Başarısız',
                    text: 'Sınav Silinemedi... :(',
                    type: 'error',
                    shadow: true
                })
            });
        });
    </script>
    @stop