<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="<?php echo csrf_token(); ?>">
    <?php echo $__env->yieldContent('title'); ?>
    <?php echo $__env->yieldContent('meta'); ?>
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <meta name="author" content="softexts.xyz">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo e(asset('/img/favicon.ico')); ?>" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo e(asset('/img/apple-touch-icon.png')); ?>">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/css/skins/default.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/font-awesome/css/fontawesome-all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/vendor/animate/animate.min.css')); ?>">

    <?php echo $__env->yieldContent('css'); ?>
    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/skins/skin-corporate-5.css')); ?>">
    <!-- Head Libs -->
    <script async src="<?php echo e(asset('/vendor/modernizr/modernizr.min.js')); ?>"></script>
    <script>
        // Tooltips
        $(function () {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        });
    </script>
</head>

<body>

<div id="sinav">
    <?php echo $__env->yieldContent('content'); ?>
</div>

<?php echo $__env->make('front.part.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->yieldContent('js'); ?>
<script async src="<?php echo e(asset('js/front/front.js')); ?>"></script>
<script async src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true
    };

   <?php if(Session::has('message')): ?>
    var type = "<?php echo e(Session::get('alert-type', 'info')); ?>";
    switch(type){
        case 'info':
            toastr.info("<?php echo e(Session::get('message')); ?>");
            break;

        case 'warning':
            toastr.warning("<?php echo e(Session::get('message')); ?>");
            break;

        case 'success':
            toastr.success("<?php echo e(Session::get('message')); ?>");
            break;

        case 'error':
            toastr.error("<?php echo e(Session::get('message')); ?>");
            break;
    }
    <?php endif; ?>

    //swal ( "Oops" ,  "Something went wrong!" ,  "error" );

</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40136905-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-40136905-8');
</script>

</body>
</html>
