<div class="row mt-5 col-md-9">	
    <div class="col">
        <div class="heading heading-border heading-bottom-border">
            <h2 class="mb-2 word-rotator-title">

                <strong class="inverted">
                    <span class="word-rotator active" data-plugin-options="{'delay': 2000, 'animDelay': 300}">
                        <span class="word-rotator-items" style="width: 142.95px; transform: translate3d(0px, -126px, 0px); transition: transform 300ms ease 0s, width 300ms ease 0s;">
                            @foreach( $sinavTipleri as $tip)
                            <span>{!! $tip->sinav_tip !!}</span>
                            @endforeach
                        </span>
                    </span>
                </strong>

            </h2>
        </div>
        <div class="content-grid mt-4 mb-4">
            @foreach( $sinavTipleri as $tip)
                @if($loop->first)
                <div class="row content-grid-row">
                @endif
                    <div class="content-grid-item col-lg-3 text-center">
                        <a href="{{ url('sinavlar/'.$tip->link) }}" target="_blank"><img class="img-fluid siyah-beyaz" src="{{ asset($tip->logo2) }}" alt="{{ $tip->sinav_tip }}"></a>
                    </div>
                @if($loop->iteration%4 === 0)
                </div>
                <div class="row content-grid-row">
                @endif
            @endforeach
                </div>
        </div>

        <hr class="tall">

        <div class="col-lg-12">

            <!-- 336x280 Büyük Dikdörtgen -->
            <ins class="adsbygoogle"
                 style="display:inline-block;width:336px;height:280px"
                 data-ad-client="ca-pub-2211050103220921"
                 data-ad-slot="7261074082"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>

    </div>
</div>

<div class="col-md-3">

<!-- esinavsalonu yan alan -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-2211050103220921"
     data-ad-slot="9358695495"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>