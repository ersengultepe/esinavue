<div class="post-block post-leave-comment">
    <h3 class="heading-primary">Yorumunuzu Gönderin</h3>

    <div v-if="yorumresult !== null">
        <yorum-error v-if="yorumresult.hata !== undefined" :yorumResult="yorumresult"></yorum-error>
        <yorum-success v-if="yorumresult.ok !== undefined" :yorumResult="yorumresult"></yorum-success>
    </div>

    <div v-if="yorumView">
        <div class="form-row">
            <?php if(auth()->guard()->guest()): ?>
                <div class="form-group col-lg-6">
                    <label for="name">İsminiz <span class="text-color-secondary">*</span></label>
                    <input type="text" maxlength="80" value="" class="form-control" v-model="kullanici" name="kullanici"
                           id="name">
                </div>
                <div class="form-group col-lg-6">
                    <label for="email">Epostanız <span class="text-color-secondary">*</span></label>
                    <input type="email" maxlength="80" value="" class="form-control" v-model="email" name="email"
                           id="email">
                </div>
            <?php endif; ?>
            <?php if(auth()->guard()->check()): ?>
                <div class="form-group col-lg-12">
                    <div class="post-block post-comments clearfix">
                        <ul class="comments">
                            <li>
                                <div class="comment">
                                    <div class="img-thumbnail d-none d-sm-block">
                                        <img class="avatar appear-animation animated tada appear-animation-visible" alt="<?php echo e(userTitle(Auth::user()->kullanici)); ?>"
                                             src="<?php echo e(userPhoto(Auth::user()->foto)); ?>"
                                             data-appear-animation="tada" data-appear-animation-delay="0" data-appear-animation-duration="1s" >
                                    </div>
                                    <div class="comment-block">
                                        <div class="comment-arrow"></div>
                                        <span class="comment-by">
                                                    <strong><?php echo e(userTitle(Auth::user()->kullanici)); ?></strong>
                                                </span>
                                        <p><?php echo Auth::user()->motto; ?></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="comment">Yorumunuz <span class="text-color-danger">*</span></label>
                    <textarea rows="6" class="form-control" v-model="yorum" name="yorum" id="comment"
                    data-toggle="tooltip" data-original-title="Lütfen soru ile ilgili bir hata varsa üye girişi yapıp <br><i class='fa fa-bug'></i> simgesine tıklayarak bildiriniz..!" title="" data-html="true"></textarea>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <input type="button" value="Gönder" @click="setYorumGonder" class="btn btn-success btn-lg right"
                           data-loading-text="Loading...">
                </div>
            </div>
        </div>
    </div>
</div>