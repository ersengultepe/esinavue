<li class="dropdown"><a class="dropdown-item dropdown-toggle"
                        href="index.html"> YKS - TYT - AYT </a>
    <ul class="dropdown-menu">
        @foreach($dersler->pluck('sinif')->unique() as $sinif)
            <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ $sinif }}</a>
                <ul class="dropdown-menu">
                    @foreach($dersler->where('sinif', $sinif)->unique()->pluck('ders') as $ders)
                        <li class="dropdown-submenu"><a class="dropdown-item" href="#">{{ $ders }}</a>
                            <ul class="dropdown-menu" style="max-height: 800px;overflow-y: auto;">
                                @foreach($yks as $sinav)
                                    @if(str_contains($sinav->adi, $sinif) and str_contains($sinav->adi, $ders))
                                        <li><a class="dropdown-item" href="{{ route('sinav', ['link'=> $sinav->link]) }}" target="_blank"
                                               data-thumb-preview="img/previews/preview-corporate.jpg">
                                                {{ str_replace('YKS - '.$sinif.' - '.$ders.' - ', '', $sinav->adi) }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>

                    @endforeach
                </ul>
            </li>
        @endforeach

    </ul>
</li>