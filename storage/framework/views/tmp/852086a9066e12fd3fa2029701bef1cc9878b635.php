<?php $__env->startSection('title'); ?>
    <title><?php echo e(config('app.name').' | '.config('app.slogan')); ?></title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('meta'); ?>
    <meta name="keywords" content="online deneme sınavı, görevde yükselme,unvan değişikliği,kpss,yds"/>
    <meta name="description" content="Online Deneme Sınavları">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('css/theme.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/theme-elements.css')); ?>">


<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('front.part.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div role="main" class="main" id="auth">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="breadcrumb">
                            <li><a href="#">Ana Sayfa</a></li>
                            <li class="active">Sınav Ekranı </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h1>Sınav Ekranı</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row" >
                <div class="col">

                    <div class="featured-boxes">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="featured-box featured-box-primary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">ÜYE GİRİŞİ</h4>
                                        <form action="<?php echo e(route('login')); ?>" id="frmSignIn" method="POST" role="form">
                                            <?php echo csrf_field(); ?>

                                            <div class="form-row">
                                                <div class="form-group col-lg-4">
                                                    <a href="<?php echo e(url('auth/redirect/facebook')); ?>">
                                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary" style="background-color: #4267b2;border-color: #4267b2;color: #eee;">
                                                            <i class="fab fa-facebook-f"></i> | Facebook Giriş
                                                        </button>
                                                    </a>
                                                </div>
                                                <div class="form-group col-lg-4">
                                                    <a href="<?php echo e(url('auth/redirect/google')); ?>">
                                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-danger">
                                                            <i class="fab fa-google"></i> | Google Giriş
                                                        </button>
                                                    </a>
                                                </div>
                                                <div class="form-group col-lg-4">
                                                    <a href="<?php echo e(url('auth/redirect/twitter')); ?>">
                                                        <button type="button" class="mb-1 mt-1 mr-1 btn btn-tertiary" style="background-color: #55ACEE;border-color: #55ACEE;color: #eee;">
                                                            <i class="fab fa-twitter"></i> | Twitter Giriş
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email Adresi</label>
                                                    <input type="text" value="<?php echo e(old('email')); ?>" class="form-control form-control-lg" name="email" id="email" maxlength="80" required autofocus>
                                                    <?php if(\Illuminate\Support\Facades\Session::has('flashLogin')): ?>
                                                    <label for="email" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('email')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <a class="float-right" href="<?php echo e(route('password.request')); ?>">(Şifrenizi Unuttunuz mu?)</a>
                                                    <label>Şifre</label>
                                                    <input type="password" class="form-control form-control-lg" name="password" id="password" required autofocus>
                                                    <?php if(\Illuminate\Support\Facades\Session::has('flashLogin')): ?>
                                                    <label for="password" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('password')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" id="rememberme" name="rememberme"> Beni Hatırla
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <input type="submit" value="Giriş" class="btn btn-danger float-right mb-5" data-loading-text="Loading..." :onclick="clickButton(login)">
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="featured-box featured-box-full-secondary text-left mt-5">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-3">KAYIT OL</h4>
                                        <form action="<?php echo e(route('register')); ?>" id="frmSignUp" method="post">
                                            <?php echo csrf_field(); ?>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>Ad</label>
                                                    <input id="ad" type="text" value="<?php echo e(old('ad')); ?>" class="form-control form-control-lg" name="ad" maxlength="25" required autofocus>
                                                        <label for="ad" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('ad')); ?></label>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Soyad</label>
                                                    <input id="soyad" type="text" value="<?php echo e(old('soyad')); ?>" class="form-control form-control-lg" name="soyad" maxlength="40" required autofocus>
                                                    <label for="soyad" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('soyad')); ?></label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <label>Email Adresi</label>
                                                    <input id="email2" type="email" value="<?php echo e(old('email')); ?>" class="form-control form-control-lg" name="email" maxlength="80" required autofocus>
                                                    <?php if(\Illuminate\Support\Facades\Session::has('flashLogin') === false): ?>
                                                    <label for="email2" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('email')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-lg-6">
                                                    <label>Şifre</label>
                                                    <input id="password2" type="password" value="" class="form-control form-control-lg" name="password" maxlength="12" minlength="6" required autofocus>
                                                    <?php if(\Illuminate\Support\Facades\Session::has('flashLogin') === false): ?>
                                                    <label for="password2" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('password')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Tekrar Şifre</label>
                                                    <input id="password_confirm" type="password" value="" class="form-control form-control-lg" name="password_confirmation" maxlength="12" minlength="6" required autofocus>
                                                    <label for="password_confirm" class="badge badge-danger text-light badge-md" ><?php echo e($errors->first('password_confirmation')); ?></label>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col">
                                                    <input type="submit" value="Kayıt Ol" class="btn btn-danger float-right mb-5" data-loading-text="Loading..." :onclick="clickButton(register)">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <!-- Vuejs auth -->
    
    <!-- Vendor -->
    <script src="<?php echo e(asset('vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.appear/jquery.appear.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery-cookie/jquery-cookie.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/popper/umd/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/common/common.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.validation/jquery.validation.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.gmap/jquery.gmap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/jquery.lazyload/jquery.lazyload.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/isotope/jquery.isotope.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/owl.carousel/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>
    <script src="<?php echo e(asset('vendor/vide/vide.min.js')); ?>"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo e(asset('js/theme.js')); ?>"></script>

    <!-- Theme Custom -->
    <script src="<?php echo e(asset('js/custom.js')); ?>"></script>

    <!-- Theme Initialization Files -->
    <script src="<?php echo e(asset('js/theme.init.js')); ?>"></script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('front.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>