<header id="header" class="header-no-min-height" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 0, 'stickySetTop': '0'}">
    <div class="header-body">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <a href="<?php echo e(url('/')); ?>">
                            <img alt="Porto" width="150" height="48" src="<?php echo e(asset('img/assets/logo.png')); ?>">
                        </a>
                            <span class="alternative-font "><em>EsinavSalonu.Com</em></span>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-stripe">
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="dropdown-item <?php if(Route::currentRouteName() === 'anasayfa'): ?> active <?php endif; ?>" href="<?php echo e(url('/')); ?>">
                                                Anasayfa
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item <?php if(Route::currentRouteName() === 'ekran'): ?> active <?php endif; ?>" href="<?php echo e(route('ekran')); ?>">
                                                Sınav Ekranı
                                            </a>
                                        </li>
                                        <?php if(auth()->guard()->guest()): ?>
                                            
                                            <li class="<?php if(Route::currentRouteName() === 'login'): ?> active <?php endif; ?> dropdown ">
                                                <a class="dropdown-item dropdown-toggle" href="#">
                                                    Giriş
                                                </a>

                                                <ul class="dropdown-menu">
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="<?php echo e(route('login')); ?>"><i class="fas fa-sign-in-alt"></i> Site Girişi</a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="<?php echo e(url('auth/redirect/facebook')); ?>"><i class="fab fa-facebook-f"></i> Facebook Giriş </a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="<?php echo e(url('auth/redirect/google')); ?>"><i class="fab fa-google"></i> Google Giriş</a>
                                                    </li>
                                                    <li class="dropdown-submenu">
                                                        <a class="dropdown-item" href="<?php echo e(url('auth/redirect/twitter')); ?>"><i class="fab fa-twitter"></i> Twitter Giriş</a>
                                                    </li>
                                                </ul>
                                            </li>

                                        <?php endif; ?>
                                    </ul>
                                </nav>
                            </div>

                            <?php if(auth()->guard()->check()): ?>

                                <div class="testimonial testimonial-primary">
                                    <div class="testimonial-author">
                                        <div class="testimonial-author-thumbnail img-thumbnail">
                                            <a href="<?php echo e(route('account.management', ['link'=>Auth::id().'-'.str_slug(Auth::user()->kullanici)])); ?>">
                                                <img data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo e(Auth::user()->kullanici); ?>"
                                                     src="<?php echo e(userPhoto(Auth::user()->foto)); ?>" alt="<?php echo e(Auth::user()->kullanici); ?>" style="max-height: 50px">
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                                <a href="<?php echo e(route('logout')); ?>"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <img style="height: 50px;margin-left: 8px;margin-top: -10px" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Çıkış Yap"
                                         src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUzIDUzIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MyA1MzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiM1NTYwODA7IiBwb2ludHM9IjAuNSwwIDAuNSw0NiAyMi41LDQ2IDM0LjUsNDYgMzQuNSwyOSAzNC41LDE3IDM0LjUsMCAiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzczODNCRjsiIHBvaW50cz0iMjIuNSw3IDAuNSwwIDAuNSw0NiAyMi41LDUzICIvPgo8bGluZSBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojRUZDRTRBO3N0cm9rZS13aWR0aDoyO3N0cm9rZS1saW5lY2FwOnJvdW5kO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoxMDsiIHgxPSIzOS41IiB5MT0iMzUiIHgyPSI1MS41IiB5Mj0iMjMiLz4KPGxpbmUgc3R5bGU9ImZpbGw6bm9uZTtzdHJva2U6I0VGQ0U0QTtzdHJva2Utd2lkdGg6MjtzdHJva2UtbGluZWNhcDpyb3VuZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MTA7IiB4MT0iMzkuNSIgeTE9IjExIiB4Mj0iNTEuNSIgeTI9IjIzIi8+CjxsaW5lIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiNFRkNFNEE7c3Ryb2tlLXdpZHRoOjI7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwOyIgeDE9IjUxLjUiIHkxPSIyMyIgeDI9IjI3LjUiIHkyPSIyMyIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" />
                                </a>
                            <?php endif; ?>

                            <ul class="header-social-icons social-icons d-none d-sm-block">
                                <li class="social-icons-facebook"><a href="https://www.facebook.com/esinavsalonu/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="social-icons-twitter"><a href="https://twitter.com/esinavsalonu" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
