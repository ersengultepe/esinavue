<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col">
                <ul class="breadcrumb">
                    <li><a href="#">Ana Sayfa</a></li>
                    <li class="active"><a href="{{ route('ekran') }}">Sınav Ekranı</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h1>@{{ sinav_adi }}</h1>
            </div>
        </div>
    </div>
</section>