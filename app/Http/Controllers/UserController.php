<?php

namespace App\Http\Controllers;

use App\Models\Ebulten;
use App\Models\Mesaj;
use App\Models\Meslek;
use App\Notifications\EbultenConfirmMessage;
use App\Notifications\EbultenRegistered;
use App\Models\Sehir;
use App\Models\Sinav;
use App\Models\Takim;
use App\User;
use App\Models\UyelerDetay;
use App\Models\Ilce;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //TODO Üye ekleme modülünde Cache i temizlemeyi Unutma..!
    public function getUyeSayisi()
    {
        return Cache::rememberForever('uyeSayisi', function () {
            return collect(DB::select('select count(id) sayi from users where durum=1'))->first();
        });
    }

    public function getSoruCozenUyeler()
    {
        return Cache::remember('soruCozenUyeler', 15, function () {
            return DB::select('SELECT u.*,
                                           s.adi,
                                           s.link,
                                           d.*
                                      FROM (SELECT id,
                                                   email,
                                                   kullanici,
                                                   foto,
                                                   gp_id,
                                                   fb_id,
                                                   tw_id
                                              FROM users
                                             WHERE durum = 1) u
                                           INNER JOIN (SELECT id as detay_id,  
                                                             sinav_id,
                                                             puan,
                                                             user_id,
                                                             created_at
                                                        FROM uyeler_detays
                                                       WHERE user_id > 1 AND durum = 1 AND puan > 10
                                                      ORDER BY id DESC) d
                                              ON d.user_id = u.id
                                           INNER JOIN (SELECT id, adi, link
                                                         FROM sinavs
                                                        WHERE durum = 1) s
                                              ON s.id = d.sinav_id
                                              ORDER BY detay_id desc
                                            LIMIT 10');
        });
    }

    //TODO üye kayıt esnasında cache i temizle..!
    public function getYeniUyeler(){
        return Cache::remember('yeniuyeler', 120, function (){
            return DB::select('SELECT coalesce(s.id, \'\') plaka,
                                           coalesce(s.sehir, \'\') sehir,
                                           u.*
                                      FROM sehirs s
                                           RIGHT JOIN (SELECT id, email, kullanici, foto, gp_id, fb_id, tw_id, sehir
                                                         FROM users
                                                        WHERE durum = 1 AND id > 1 AND yetki = 2
                                                       ORDER BY id DESC
                                                        LIMIT 10) u
                                              ON s.id = u.sehir');
                                    });
}

    /*public function getPuanDurumu(){
        return Cache::remember('puanDurumu', 180, function (){
            return DB::select('SELECT dt.user_id id,
                                           SUM(dt.puan) AS puan,
                                           u.kullanici,
                                           u.foto,
                                           u.email, 
                                           u.gp_id, 
                                           u.fb_id, 
                                           u.tw_id
                                      FROM (SELECT d.user_id, d.sinav_id, MAX(d.puan) AS puan
                                              FROM uyeler_detays d
                                             WHERE d.user_id > 1 AND d.durum = 1
                                            GROUP BY d.sinav_id, d.user_id
                                            ORDER BY d.user_id) AS dt
                                           LEFT JOIN users u ON u.id = dt.user_id
                                     WHERE u.durum = 1
                                    GROUP BY dt.user_id, u.kullanici, u.foto, u.email, u.gp_id, u.fb_id, u.tw_id
                                    ORDER BY puan DESC
                                     LIMIT 10');
        });
    }*/

    public function getEnCokSinavCozenUye(){
        return Cache::remember('enCokSinavCozenUye', 180, function () {
            return collect(
                DB::select('SELECT u.id,
                                       u.kullanici,
                                       u.foto,
                                       u.email,
                                       u.fb_id,
                                       u.gp_id,
                                       u.tw_id,
                                       coalesce(m.meslek, \' \') meslek,
                                       count(sinav_id) AS sinav_sayisi
                                  FROM (SELECT *               
                                          FROM uyeler_detays d               
                                         WHERE d.user_id > 1 AND d.durum = 1
                                        ORDER BY id DESC
                                         LIMIT 5000) dt
                                         INNER JOIN users u ON u.id = dt.user_id AND u.durum = 1
                                         LEFT JOIN mesleks m ON u.meslek = m.id
                                GROUP BY user_id
                                ORDER BY sinav_sayisi DESC
                                 LIMIT 1')
            )->first();

        });
    }

    public function getEnCokSoruCozenUye($userId){
        return Cache::remember('enCoksoruCozenUye', 180, function () use($userId){
            return collect(
                DB::select('SELECT user_id,
                                       sum(dogru) + sum(yanlis) AS cozulen_soru_sayisi,
                                       u.kullanici,
                                       u.foto,
                                       coalesce(m.meslek, \' \') meslek,
                                       u.id,
                                       u.email,
                                       u.fb_id,
                                       u.gp_id,
                                       u.tw_id
                                  FROM (SELECT *
                                          FROM uyeler_detays
                                         WHERE user_id > 1 AND durum = 1 AND user_id != ?
                                        ORDER BY id DESC
                                         LIMIT 5000) d
                                       INNER JOIN users u
                                          ON u.id = d.user_id and u.durum=1
                                       LEFT JOIN mesleks m ON u.meslek = m.id
                                GROUP BY user_id
                                ORDER BY cozulen_soru_sayisi DESC
                                 LIMIT 1', [$userId])
            )->first();
        });
    }

    public function getEnCokPuanAlanUye($userId, $userId2){
        return Cache::remember('enCokPuanAlanUye', 180, function () use ($userId, $userId2){
            return collect(
                DB::select('SELECT u.id, user_id, u.kullanici, sum(max_puan) AS toplam_puan, u.foto, coalesce(m.meslek, \' \') meslek,
                                               u.email,
                                               u.fb_id,
                                               u.gp_id,
                                               u.tw_id
                                  FROM (SELECT d.user_id, d.sinav_id, max(d.puan) max_puan
                                          FROM (SELECT *
                                                  FROM uyeler_detays
                                                 WHERE durum = 1
                                                ORDER BY id DESC
                                                 LIMIT 5000) d
                                         WHERE user_id > 1 and user_id not IN (?, ?)
                                        GROUP BY sinav_id
                                        ORDER BY user_id DESC, sinav_id ASC) dt
                                        INNER JOIN users u ON u.id = dt.user_id and u.durum=1
                                        LEFT JOIN mesleks m ON u.meslek = m.id
                                GROUP BY user_id
                                ORDER BY toplam_puan DESC
                                 LIMIT 1', [$userId, $userId2])
            )->first();
        });
    }

    public function getEnYuksekOrtalamaPuanliUye($userId, $userId2, $userId3)
    {
        return Cache::remember('enYuksekOrtalamaPuanliUye', 180, function () use($userId, $userId2, $userId3){
            return collect(
                DB::select('SELECT *
                                  FROM (SELECT u.id,
                                               user_id,
                                               u.kullanici,
                                               avg(max_puan) AS ortalama_puan,
                                               COUNT(dt.sinav_id) sinav_sayisi,
                                               u.foto,
                                               u.email,
                                               u.fb_id,
                                               u.gp_id,
                                               u.tw_id,
                                               coalesce(m.meslek, \' \') meslek
                                          FROM (SELECT d.user_id, d.sinav_id, max(d.puan) max_puan
                                                  FROM (SELECT *
                                                          FROM uyeler_detays
                                                         WHERE durum = 1
                                                        ORDER BY id DESC
                                                         LIMIT 5000) d
                                                 WHERE user_id > 1 AND user_id NOT IN (?, ?, ?)
                                                GROUP BY sinav_id
                                                ORDER BY user_id DESC, sinav_id ASC) dt
                                               INNER JOIN users u ON u.id = dt.user_id AND u.durum = 1
                                               LEFT JOIN mesleks m ON u.meslek = m.id
                                        GROUP BY user_id
                                        ORDER BY ortalama_puan DESC) tt
                                 WHERE tt.sinav_sayisi > 4
                                ORDER BY ortalama_puan DESC
                                 LIMIT 1', [$userId, $userId2, $userId3])
            )->first();
        });
    }

//    Route::get('uye/{link}', 'UserController@getUserProfilePage')->name('profilePage');
    public function getUserProfilePage($link)
    {
        $user = $this->getUserSlugCheck($link);

        if($user === false){
            return abort(404);
        }

        $sinavlar = self::getUserCozulenTumSinavlar($user); //ortalama puanlarla birlikte sınav bilgileri geldi
        $sinavAdlari =  collect($sinavlar)->take(10)->pluck('adi');

        $info = $this->getUserSinavIstatistikleri($user->id, $sinavlar);

        return view('profile.user-profile', compact('user', 'info', 'sinavlar', 'sinavAdlari'));
    }

    private function getUserCozulenTumSinavlar($user)
    {
        return Cache::remember('userCozulenTumSinavlar_'.$user->id, 120, function () use($user){
            $sinavlar = DB::select('SELECT FTURKCE(adi) as adi,
                               link,
                               kategori_id,
                               d.*
                          FROM sinavs s
                               INNER JOIN (SELECT *
                                             FROM uyeler_detays
                                            WHERE durum = 1 AND sinav_id > 0) d
                                  ON d.sinav_id = s.id
                         WHERE user_id = ?
                        ORDER BY id DESC', [$user->id]);

            return self::getSeciliSinavlarinOrtalamalari($sinavlar);//burada sinavların içine ort puan da ekleniyor. $sinavlar döndürür
        });
    }

    public function getAllSinavOrtalamalari() : array
    {
        try {
            return $sinavOrtalamalari = Cache::remember('sinavOrtalamalari', 1440, function () {

                $sinavOrtalamalariListesi = [];

                $ort = DB::select('select d.sinav_id, ROUND(avg(d.puan),2) as ort 
                            from uyeler_detays d, sinavs s
                            where (dogru+yanlis)*100 / (dogru+yanlis+bos) > 15 
                            and d.durum = 1
                            and s.durum = 1
                            and d.sinav_id = s.id
                            GROUP BY d.sinav_id');

                $sinavOrtalamalari = (array)$ort;
                foreach ($sinavOrtalamalari as $sinavOrt) {
                    $sinavOrtalamalariListesi[$sinavOrt->sinav_id] = $sinavOrt->ort;
                }

                return $sinavOrtalamalariListesi;

            });

        } catch (\Exception $e)
        {
            Log::error('Sınav Ortalamaları Hatası', ['Hata'=> 'Ortalamalar Oluşturulamadı '.$e->getMessage().' '.$e->getCode().' '.$e->getFile().' '.$e->getLine()]);
            return ['response'=>'error'];
        }
    }

    public function getSeciliSinavlarinOrtalamalari($sinavlar)
    {
        $sinavOrtalamalari = self::getAllSinavOrtalamalari();
        foreach ($sinavlar as $sinav) {
            if (isset($sinavOrtalamalari[$sinav->sinav_id])) {
                $sinav->ort = $sinavOrtalamalari[$sinav->sinav_id];
            }
        }
        return $sinavlar;
    }

    public function getUserIdCheck($slug)
    {
        try{
            // Eğer adres satırında tire "-" yoksa geldiğin yere geri git
            if (str_contains($slug, '-') !== true) {
                Log::error('Üye Profili Görüntülenmek İsterken Hata Oluştu, gelen likte - bulunmuyordu Link:' . $slug);
                return false;
            }

            // "-" tire işaretinden bölüyor linki
            $slug = explode('-', strip_tags(replaceSpace(trim($slug))));

            // uyeLink[0] da id var.
            $userId = filter_var($slug[0], FILTER_SANITIZE_NUMBER_INT);

            if (!is_numeric($userId) or $userId === 0) {
                //id integer değilse şutla.
                Log::error('Üye Profili Görüntülenmek İsterken Hata Oluştu, user_id rakam değildi..! id:' . $userId . ' gelen link: ' . $slug);
                return false;
            }

        }catch (\Exception $e){
            Log::error('Üye Id Hatalı : ' .$e->getMessage().'...'.$e->getFile().'...'.$e->getLine().'...'.$e->getCode());
            return false;
        }
        return $userId;
    }

    public function getUserNameCheck(Int $userId, String $slug, User $user) : bool
    {
        try{
            // $userName yakala
            $userName = str_replace($userId . '-', '', strip_tags(replaceSpace(trim($slug))));

            if ($userName === null)
            {
                return false;
            }

            //Linki temizledik.
            $userName = filter_var($userName, FILTER_SANITIZE_STRING);

            if (str_slug($user->kullanici) !== $userName) {
                Log::error('Üye Profili Görüntülenmek İsterken Hata Oluştu, ziyaretçi hatalı link tıkladı Link:' . $slug);
                return false;
            }

            return true;

        }catch (\Exception $e)
        {
            Log::error('Üye Profili Görüntülenmek İsterken Hata Oluştu.'. $e->getMessage().'...'.$e->getFile().'...'.$e->getLine().'...'.$e->getCode());
            return false;
        }
    }

    public function getUserSlugCheck($slug) : User
    {
        try {
            $userId = $this->getUserIdCheck($slug);
            if ($userId === false) {
                return abort(404);
            }

            $user = User::findOrFail($userId);

            $userName = $this->getUserNameCheck($userId, $slug, $user);
            if ($userName === false) {
                return abort(404);
            }

            return $user;

        } catch (\Exception $e) {
            Log::error('Üye Profili görüntülenirken hata oluştu  Message:' .$e->getMessage().'...'.$e->getFile().'...'.$e->getLine().'...'.$e->getCode());
            return abort(404);
        }

    }

    public function getGenerateGenelBasariSirasi()
    {
        try {
            return $genelBasariSirasi = Cache::remember('genelBasariSirasi', 60, function () {
                return DB::select('select l.*, u.* from users u
                                            LEFT JOIN (SELECT @fakeId := @fakeId + 1 AS sira, d.user_id, round(puan, 2) puan
                                            FROM (SELECT @fakeId := 0) f,
                                                 (SELECT dt.user_id, sum(dt.puan) AS puan
                                                    FROM (SELECT ud.user_id, ud.sinav_id, ud.puan AS puan
                                                           FROM (select * from uyeler_detays where ((dogru+yanlis+bos)*bos/100) < 85) ud, users us
                                                           WHERE     us.id = ud.user_id
                                                                 AND ud.user_id > 1
                                                                 AND us.durum = 1
                                                                 AND ud.durum = 1
                                                          GROUP BY sinav_id, user_id
                                                          ORDER BY user_id) AS dt
                                                  GROUP BY user_id
                                                  ORDER BY puan DESC) AS d) l
                                             ON l.user_id=u.id 
                                             where l.user_id is not null
                                             and u.durum=1 
                                             order by  sira asc');
            });

        } catch (\Exception $e) {
            Log::error('Genel Başarı Listesi Oluşturulurken hata oluştu. Message:' . $e->getMessage() . ' File:' . $e->getFile() . ' Code:' . $e->getCode() . ' Line:' . $e->getLine());
            return false;
        }
    }

    public function getUserToplamPuanVeBasariSirasi($userId){
        $basariListesi = self::getGenerateGenelBasariSirasi();

        $user = collect($basariListesi)->where('user_id', $userId)->first();

        return $user;
    }

    public function getUserSinavIstatistikleri(Int $userId, $sinavlar)
    {
        return Cache::remember($userId.'-getUserSinavIstatistikleri', 600, function () use($userId, $sinavlar){
            $toplamPuanVeBasariSirasi = $this->getUserToplamPuanVeBasariSirasi($userId);

            if($toplamPuanVeBasariSirasi === null){
                $user['toplamPuan']     = '-';
                $user['basariSirasi']   = '-';
                $user['ortalamaPuan']   = '-';
                $user['cozulensinav']   = '-';
            }else{
                $user['toplamPuan']     = number_format(round($toplamPuanVeBasariSirasi->puan, 2), 2, ',', '.');
                $user['basariSirasi']   = $toplamPuanVeBasariSirasi->sira;
                $user['ortalamaPuan']   = number_format(round(collect($sinavlar)->avg('ort'), 2), 2, ',', '.');
                $user['cozulensinav']   = collect($sinavlar)->count();
            }

            return (object)$user;
        });
    }

    public function getYorumlar($limit)
    {
        //TODO bir yorum onaylandığında bu cache i temizle..!
        return Cache::rememberForever('sonYorumlar', function () use($limit){
           return DB::select('SELECT y.id,
                                       CASE
                                          WHEN y.user_id > 0
                                          THEN
                                             (SELECT kullanici
                                                FROM users
                                               WHERE id = y.user_id)
                                          ELSE
                                             y.kullanici
                                       END
                                          kullanici_adi,
                                       /*CASE
                                          WHEN y.user_id > 0
                                          THEN
                                             (SELECT CASE
                                                        WHEN foto = \'resim/user.png\'
                                                        THEN
                                                           \'/resim/uyeler/user.png\'
                                                        ELSE
                                                           foto
                                                     END
                                                        foto
                                                FROM users
                                               WHERE id = y.user_id)
                                          ELSE
                                             \'/resim/uyeler/user.png\'
                                       END
                                          user_foto,*/
                                       FTURKCE(y.yorum) yorum,
                                       s.link,
                                       y.user_id,
                                       y.created_at AS tarih
                                  FROM yorums y LEFT JOIN sinavs s ON s.id = y.icerik
                                 WHERE y.tur = 2 AND y.onay = 1 and y.user_id != 1
                                ORDER BY y.created_at DESC
                                limit ?', [$limit]);
        });
    }

    public function getUserAccountManagement($link)
    {

        $user = $this->getUser($link);

        $profileInfo = $this->getUserSinavIstatistikleri(explode('-',$link)[0]);

        $user = array_merge((array)$user, (array)$profileInfo);

        return Response::json($user);
    }

    public function sehirler()
    {
        return Sehir::all();
    }

    public function ilceler($sehirId)
    {
        return Ilce::where('sehir_id', $sehirId)->orderBy('ilce')->get();
    }

    public function meslekler()
    {
        return Meslek::all();
    }

    public function takimlar()
    {
        return Takim::all();
    }

    public function sinavlar()
    {
        return Sinav::all();
    }

    public function getOnayliUyeler()
    {
        $uyeler = User::where('durum', 1)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Üyeler');
        return view('admin.uye.index', compact('uyeler'));
    }

    public function getOnayBekleyenUyeler()
    {
        $uyeler = User::where('durum', 0)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Onay Bekleyen Üyeler');
        return view('admin.uye.index', compact('uyeler'));
    }

    public function getSilinenUyeler()
    {
        $uyeler = User::where('durum', 3)->orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Silinen Üyeler');
        return view('admin.uye.index', compact('uyeler'));
    }

    public function ajaxUserDurumUpdate(Request $request)
    {
        try{
            User::findOrFail($request->user_id)
                ->update([
                    'durum' => $request->durum
                ]);

            switch ($request->durum){
                case 1 : $text = 'Onaylandı'; break;
                case 0 : $text = 'Bekleyenlere alındı'; break;
                case 3 : $text = 'Silindi'; break;
                default : $text = 'Onaylandı';
            }

            return ['title'=> 'Başarılı', 'text'=> 'Üye Başarıyla '.$text ];

        }catch (\Exception $e){
            return false;
        }
    }

    //for admin
    public function ebulten()
    {
        $uyeler = Ebulten::orderBy('id', 'DESC')->paginate(100);
        Session::put('breadcrumb', 'Ebülten Aboneleri');
        return view('admin.ebulten.index', compact('uyeler'));
    }

    public function ebultenValidate($email)
    {
        $kurallar = array(
            'email' => 'required|email|max:100|unique:ebultens,eposta',
        );

        $hataMesajlari = array(
            'email.required' => 'E-posta adresinizi belirtmelisiniz..!',
            'email.email' => 'Lütfen geçerli bir E-posta adresi yazınız..!',
            'email.unique' => 'Zaten E-bültene abonesiniz..!',
            'email.max' => 'En fazla 100 karakter yazabilirsiniz..!'
        );

        $denetim = Validator::make(['email'=> $email], $kurallar, $hataMesajlari);

        if ($denetim->fails())
        {
            return ['hata' => $denetim->errors()->first()];
        }

        return true;
    }

    public function ebultenConfirmLinkCheck($link)
    {
        //gelen linkin 12 karakter baştan sil
        $link = str_replace(substr($link, 0, 12), '',$link);
        $link = substr_replace($link, '',-10);
        $email = base64_decode($link);

        //Linkten elde edilen emaik doğrulanıyor
        if($this->ebultenValidate($email) === true)
        {
            return $email;
        }
        return false;
    }

    public function ebultenInsert($link)
    {
        $email = $this->ebultenConfirmLinkCheck($link);
        if ($email === false){
            return view('errors.error')
                ->with([
                    'errorMessage' => 'E-bülten Onay Linki Hatalı',
                    'errorExplain' => 'E-bülten Onay Linki veritabanımızdaki kayıtlarla uyuşmamaktadır.'
                ]);
        }

           $ins = new Ebulten();
           $ins->eposta = $email;
           $ins->save();

            //Ebülten aboneliğinin gerçekleştiğini bildir.
            (new User)->forceFill([
                'email' => $email,
            ])->notify(new EbultenRegistered());

        return view('success')->with([
            'successMessage' => 'E-bülten Aboneliğiniz Gerçekleştirildi.<br>E-bültenlerimize başarıyla abone oldunuz. Umarız beğenirsiniz.',
            'successTitle' => 'Tebrikler'
        ]);
    }

    public function ebultenCheck(Request $request)
    {
        $valid = $this->ebultenValidate($request->email);
        if($valid === true)
        {
            $resUser = User::where('email', $request->email)
                ->where('durum', 1)
                ->first();

            if($resUser !== null)
            {
                return Response::json(['hata'=>'Zaten kayıtlı üyesiniz. Ebülten\'e ayrıca kaydolmanıza gerek yok']);
            }else{
                //Geçerli bir email adresi ise ve Üye değilse,
                (new User)->forceFill([
                    'email' => $request->email,
                ])->notify(new EbultenConfirmMessage());
                return Response::json(['ok' => 'Eposta adresinize onay epostası gönderdik, Lütfen onaylayın.']);
            }
        }else{
            return $valid;
        }
    }

    public function mesajValidate($request)
    {
        $gelenVeriler = [
            'email'=> $request->email,
            'ad_soyad' => $request->ad_soyad,
            'mesaj' => $request->mesaj
        ];

        $kurallar = array(
            'email' => 'required|email|max:100',
            'ad_soyad' => 'required|string|max:100',
            'mesaj' => 'required|string|max:500'
        );

        $hataMesajlari = array(
            'email.required' => 'E-posta adresinizi belirtmelisiniz..!',
            'email.email' => 'Lütfen geçerli bir E-posta adresi yazınız..!',
            'email.max' => 'En fazla 100 karakter yazabilirsiniz..!',
            'ad_soyad.required' => 'Lütfen ad soyadınızı boş bırakmayınız..!',
            'ad_soyad.string' => 'Hatalı karakter girişi yaptınız..!',
            'ad_soyad.max' => 'Ad soyadınız en fazla 100 karakter olmalıdır..!',
            'mesaj.required' => 'Lütfen mesajınızı boş bırakmayınız..!',
            'mesaj.string' => 'Hatalı karakter girişi yaptınız..!',
            'mesaj.max' => 'Mesajınız en fazla 100 karakter olmalıdır..!'
        );

        $denetim = Validator::make($gelenVeriler, $kurallar, $hataMesajlari);

        if($denetim->fails())
        {
            return $denetim->errors()->first();
        }

        return true;
    }

    public function mesajInsert(Request $request)
    {
        try{
            $ins = new Mesaj();
            $ins->name = $request->ad_soyad;
            $ins->email = $request->email;
            $ins->from = Auth::check() ? Auth::id() : 0;
            $ins->to = 1;
            $ins->body = $request->mesaj;
            $ins->ip = request()->ip();
            $ins->read = 0;
            $ins->saveOrFail();

            return true;
        }catch (\Exception $e){
            Log::error('ana-sayfa-mesaj-gonderme-hatası', ['Hata', $e->getCode().'---'.$e->getFile().'---'.$e->getMessage().'---'.$e->getLine()]);
            return false;
        }
    }

    public function mesajGonder(Request $request)
    {
        $res = $this->mesajValidate($request);

        if($res !== true)
        {
            return Response::json(['hata' => $res]);
        }

        if($this->mesajInsert($request)){
            return Response::json(['ok' => 'Mesajınız alınmıştır...']);
        }else{
            return false;
        }
    }

    public function hesapGoruntule(){
        if (Auth::id() == 1 and Session::has('admin-self-account-profile')==false ){
            return redirect()->route('admin');
        }
        return view('account.index');
    }
}
