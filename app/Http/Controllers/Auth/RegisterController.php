<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\UserConfirm;
use App\Notifications\UserRegister;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     * Giriş yaptıktan sonra profil sayfasına yönlendirilecek.
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'ad' => 'required|string|max:25',
            'soyad' => 'required|string|max:40',
            'email' => 'required|string|email|max:80|unique:users',
            'password' => 'required|string|min:6|max:12|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'ad' => ucWordsTr($data['ad']),
            'soyad' => ucwordsTr($data['soyad']),
            'kullanici' => ucWordsTr($data['ad'].' '.$data['soyad']),
            'foto' => '/resim/uyeler/user.png',
            'ebulten' => 'E',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'md5_password' => md5($data['password']),
            'orginal_password' => $data['password'],
            'durum' => 0,
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.login-register');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        Auth::logout();

        $user->notify(new UserRegister());

        $notification = [
            'message' => 'Sitemize Kayıt Oldunuz..! Şimdi gönderdiğimiz maili onaylayınız.',
            'alert-type' => 'success'
        ];

        Log::info($user->id.' id no\'lu '.$user->kullanici.' isimli kişi siteye kayıt oldu.');

        return redirect()->route('ekran')->with($notification);
    }

    /**
     * Gelen url içinde - tire olup olmadığı kontrol ediliyor.
     *
     * @param $url
     * @return bool
     */
    protected function confirmLinkCheck($url){
        return str_contains($url, '-') ? true : false;
    }

    /**
     * error.blade.php içine parametre göndererek hata sayfası gösteriliyor.
     *
     * @return $this
     */
    protected function confirmLinkErrorPage(){
        $link = route('confirm.page');
        return view('errors.error')
            ->with([
                'errorMessage' => 'Onay Link Hatası',
                'errorExplain' => "Tıkladığınız link geçersiz, tekrar onay maili almak için lütfen <a href='{$link}'>Onay Maili Gönder</a> linkini tıklayınız"
            ]);
    }

    /**
     * Onay maili kontrollerden geçiriliyor, hata varsa hata mesajı gösteriliyor,
     * Hata yoksa kullanıcı durum=1 yapılarak üyeliği aktifleştiriliyor.
     *
     * @param $url
     * @return RegisterController|\Illuminate\Http\RedirectResponse
     */
    protected function confirmAccount($url){
        if($this->confirmLinkCheck($url) == false){
            return $this->confirmLinkErrorPage();
        }

        $url = explode('-',$url);
        $user = User::where('md5_password', $url[0])
            ->where('id', $url[1])
            ->where('durum', 0)
            ->first();

        if($user === null){
            return $this->confirmLinkErrorPage();
        }

        $user->durum = 1;
        $user->save();

        $user->notify(new UserConfirm());
        return redirect()->route('ekran');
    }

    protected function confirmAccountSendCode(Request $request){
        //Gelen mail kontrol edildi, veritabanından var mı yok mu bakıldı
        $this->validatorEmail($request);

        $user = User::where('email', $request->email)->first();

        $user->notify(new UserRegister());

        $notification = [
            'message' => 'Sitemize Daha Önce Kayıt Olmuştunuz. Size yeniden bir onay kodu gönderdik. Lütfen üyeliğinizi etkinleştiriniz.',
            'alert-type' => 'success'
        ];

        Log::info($user->id.' id no\'lu '.$user->kullanici.' isimli kişiye tekrar onay kodu gönderildi.');

        return redirect()->route('ekran')->with($notification);
    }

    protected function validatorEmail(Request $request)
    {
        return $this->validate($request, [
            'email' => 'required|string|email|max:80|exists:users',
        ]);
    }
}
