<?php

namespace App\Console\Commands;

use App\Http\Controllers\SiteMapController;
use Illuminate\Console\Command;

class SiteMapCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'esinav:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Site Haritaları Oluşturmak İçin Kullanılmaktadır.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = new SiteMapController();
        $sitemap->sitemap();
        $this->info('Anasayfa Sitemap Oluşturuldu...');
        $sitemap->sitemap1();
        $this->info('Sayfalar için Sitemap Oluşturuldu...');
        $sitemap->sitemap2();
        $this->info('Sınav Tipleri için Sitemap Oluşturuldu...');
        $sitemap->sitemap3();
        $this->info('Sınavlar için Sitemap Oluşturuldu...');
        $sitemap->sitemap4();
        $this->info('Üyeler için Sitemap oluşturuldu.');
        $this->info('Tüm Sitemapler Oluşturuldu...');
        return $this;
    }

}
