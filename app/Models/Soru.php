<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Soru extends Model
{
    protected $table = 'sorus';

    protected $fillable = [
        'soru_kaynak_id',
        'soru_kategori_id',
        'dogru',
        'yanlis',
        'bos',
        'ekleyen',
        'ip',
        'durum'
    ];

    public function cevaplar()
    {
        return $this->hasMany(Cevap::class);
    }

}
