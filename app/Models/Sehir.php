<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sehir extends Model
{
   protected $table = 'sehirs';

   protected $fillable = [
    'sehir'
   ];

   public function ilce(){
       return $this->hasMany('App\Ilce');
   }

}
