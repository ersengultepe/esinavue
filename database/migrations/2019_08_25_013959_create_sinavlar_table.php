<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSinavlarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sinavlar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adi');
            $table->string('link')->unique();
            $table->unsignedInteger('kategori_id')->index();
            $table->unsignedTinyInteger('tarzi')->index()->comment('1=>Konu Sınavı (Kümeler, 657 SK gibi), 2=> Deneme Sınavı (GYS Denemesi içinde her kanundan sorular var gibi), 3=> Yapılan Sınavlar');
            $table->unsignedInteger('kapsam')->index()->comment('gys, uds, kpss, yds');
            $table->unsignedInteger('duzenleyen')->nullable()->default(0)->index()->comment('bu sınavı düzenleyen kurum yani todaie sgk için sınav sorusu hazırlamış');
            $table->unsignedInteger('kurum')->nullable()->default(0)->index()->comment('sınavın gerçekleştiği kurum yani sgk sınav yaptırmış');
            $table->string('kapsam2')->nullable()->default('')->comment('sgk, meb');
            $table->string('kapsam3')->nullable()->default('')->comment('11. Sınıf');
            $table->string('kapsam4')->nullable()->default('')->comment('Matematik, Fizik');
            $table->unsignedSmallInteger('soru_sayisi')->nullable()->default(0)->index()->comment('gerekirse kullanılabilir');
            $table->date('tarih')->nullable()->comment('Varsa Sınavın tarihi');
            $table->enum('menu_gorunum', [1,0])->default(1)->comment('menüde görünsün, görünmesin //Eskiyen sınavlar DB den silinmeyecek ama listelenmeyecek');
            $table->enum('durum', [1,0])->default(1)->comment('1 ise aktif 0 ise pasif');
            $table->unsignedInteger('user_id')->nullable()->default(1)->comment('ekleyen kullanıcı id'); //
            $table->ipAddress('ip')->nullable()->default('')->comment('hangi ip den eklendi'); //
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sinavlar');
    }
}
