<div class="home-intro" id="home-intro">
    <div class="container">

        <div class="row align-items-center">
            <div class="col-lg-8">
                <p>
                    Online Sınavları çözmek herkes için artık gerçekten de çok <em>Keyifli</em>
                    <span>Tamamen ve daima <strong>ücretsiz</strong></span>
                </p>
            </div>
            <div class="col-lg-4">
                <div class="get-started text-left text-lg-right">
                    <a href="<?php echo e(route('ekran')); ?>" class="btn btn-lg btn-primary">Sınav Ekranını Aç!</a>
                    <!-- TODO sık sorulan sorular sayfası yap-->
                    <div class="learn-more">veya <a href="<?php echo e(route('sss')); ?>">SSS sayfasına geç.</a></div>
                </div>
            </div>
        </div>

    </div>
</div>