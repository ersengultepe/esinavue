@extends('admin.template')

@section('content')
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">{{ Session::get('breadcrumb') }}</h2>
                <small>Toplam {{ Session::get('breadcrumb') }}: {{ $uyeler->total() }}</small>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Email Adresi</th>
                            <th>Eklenme Tarihi</th>
                            <th>Durum</th>
                            <th>İşlemler</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($uyeler as $uye)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td style="font-size: 1.3em">{{ $uye->eposta }}</td>
                                <td>{{ $uye->created_at }}</td>
                                <td>{{ $uye->durum == 1 ? 'Evet' : 'Hayır' }}</td>
                                <td class="actions-hover actions-fade">
                                    <a href=""><i class="fa fa-pencil"></i></a>
                                    <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            {{ $uyeler->links() }}
        </section>
    </div>
@stop